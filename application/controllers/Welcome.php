<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_controller', 'welcome');

	}

	public function index() {
		
		$clients = new $this->Clients_model;
		$clients->set_order('id', 'DESC');
		$this->template_data->set('clients', $clients->populate());

		$this->template_data->set('current_client', false);

		$this->load->view('welcome/welcome', $this->template_data->get_data());
	}

	public function set_current_client($client_id) {
		$this->session->set_userdata('current_client', $client_id);
		redirect("layout/index/{$client_id}");
	}

}
