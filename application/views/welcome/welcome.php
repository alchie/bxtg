<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group list-group-icons icons2">
                <?php if( $clients ) foreach($clients as $client) { ?>
                  <a class="list-group-item" href="<?php echo site_url("welcome/set_current_client/{$client->id}"); ?>">
                  <span class="glyphicon glyphicon-globe"></span>
                  <?php echo $client->name; ?>
                  <br>
                     <small>(<?php echo $client->domain; ?>)</small>
                  </a>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('footer'); ?>