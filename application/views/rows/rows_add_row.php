<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Row</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

    
                <div class="form-group">
                    <label>Row Title</label>
                    <input name="row_title" type="text" class="form-control" value="">
                </div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label>Row Columns</label>
                <select name="columns" class="form-control" title="- - Number of Columns - -">
                <?php 
                $columns = 1;
                for($i=1;$i<=12;$i++) { ?>
                    ?>
                  <option value="<?php echo $i; ?>" <?php echo ($columns==$i) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                <?php } ?>
                </select>
              </div>
    </div>
    <div class="col-md-6">
                <div class="form-group">
                    <label>Row Class</label>
                    <input name="row_class" type="text" class="form-control" value="">
                </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
                <div class="form-group">
                    <label class="pull-right"><input name="container" type="checkbox" value="1"> Add Container</label>
                    <label>Container Class</label>
                    <input name="container_class" type="text" class="form-control" value="">
                </div>
    </div>
    <div class="col-md-6">
                 <div class="form-group">
                    <label>Wrapper Class</label>
                    <input name="wrapper_class" type="text" class="form-control" value="">
                </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label>Group</label>
                <?php  $selected = $this->input->get('group'); ?>
                <select name="group_id" class="form-control" title="- - Row Group - -">
                     <option value="" <?php echo ($selected==false) ? 'SELECTED' : ''; ?>>- - None - -</option>
                <?php 
                foreach($groups as $group) { ?>
                    ?>
                  <option value="<?php echo $group->group_id; ?>" <?php echo ($selected==$group->group_id) ? 'SELECTED' : ''; ?>><?php echo $group->group_name; ?></option>
                <?php } ?>
                </select>
              </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("rows/index/{$client_id}"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>