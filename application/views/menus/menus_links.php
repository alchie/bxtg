<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ):  ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	<form method="post">

<ul class="nav nav-tabs">
<?php foreach($menus as $menu2) { ?>
  <li role="presentation" class="<?php echo ($menu->id==$menu2->id) ? 'active' : ''; ?>"><a href="<?php echo site_url("menus/links/{$menu2->id}"); ?>"><?php echo $menu2->name; ?></a></li>
<?php } ?>
  <li role="presentation" class="pull-right"><a data-title="Add Menu" href="<?php echo site_url("menus/add/{$current_client->id}/ajax"); ?>" class="ajax-modal"><span class="glyphicon glyphicon-plus"></span> Add Menu</a></li>
</ul>

	    <div class="panel panel-default">
	    	<div class="panel-heading">

	    	<a data-title="Add Link" href="<?php echo site_url("menus/add_link/{$menu->id}/ajax") . "?next=" . uri_string(); ?>" class="btn btn-success btn-xs pull-right ajax-modal"><span class="glyphicon glyphicon-plus"></span> Add Link</a>

	    	<button class="btn btn-primary btn-xs pull-right" type="submit" style="margin-right:5px;">Save</button>

	    		<h3 class="panel-title"><?php echo $menu->name; ?>
	    			
	    			<a data-title="Edit Menu" href="<?php echo site_url("menus/edit/{$menu->id}/ajax") . "?next=" . uri_string(); ?>" class="ajax-modal">
	    				<span class="glyphicon glyphicon-pencil"></span>
	    			</a>

	    		</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php 

function display_links($links, $pre="") {
if( $links ) {
	echo '<div class="list-group sortable" style="margin-top:10px;">';
	foreach($links as $link) { ?>
	<div class="list-group-item">
	<?php echo $pre; ?><?php echo $link->text; ?><input type="hidden" name="order[]" value="<?php echo $link->id; ?>">
		<div class="btn-group pull-right">
		  <a data-title="Edit Link" class="btn btn-warning btn-xs ajax-modal" href="<?php echo site_url("menus/edit_link/{$link->id}/ajax") . "?next=" . uri_string(); ?>">Edit</a>
		  <?php if($link->active==0) { ?>
		  <a class="btn btn-danger btn-xs confirm" href="<?php echo site_url("menus/delete_link/{$link->id}") . "?next=" . uri_string(); ?>">Delete</a>
		  <?php } ?>
		</div>

<?php 
if( $link->children ) {
	display_links($link->children, $pre);
}
?>

	</div>
	<?php 
	 	}
	 echo '</div>'; 
	}
}
display_links($links, "");

?>
</tbody>
</table>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
	    </form>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>