<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Upload</h3>
            </div>

            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="thumbnail">
      <img src="<?php echo base_url("uploads/{$current_image->file_name}"); ?>" />
</div>


<form method="post">

<div class="form-group">
    <div class="form-control input-sm"><?php echo base_url("uploads/{$current_image->file_name}"); ?></div>
</div>
<div class="form-group">
      <input name="title" value="<?php echo $current_image->title; ?>" type="text" class="form-control input-sm" placeholder="Title">
</div>
<div class="form-group">
      <input name="upload_url" value="<?php echo $current_image->upload_url; ?>" type="text" class="form-control  input-sm" placeholder="Upload URL">
</div>

<div class="form-group">
        <button class="btn btn-success" type="type">Save</button>
</div>

</form>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>

        </div>
    </div>

</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>