<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rows_columns_widgets_model Class
 *
 * Manipulates `rows_columns_widgets` table on database

CREATE TABLE `rows_columns_widgets` (
  `widget_id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `column` int(2) NOT NULL,
  `type` varchar(200) NOT NULL,
  `options` text NOT NULL,
  `order` int(2) DEFAULT 0,
  PRIMARY KEY (`widget_id`),
  KEY `row_id` (`row_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

ALTER TABLE  `rows_columns_widgets` ADD  `widget_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `rows_columns_widgets` ADD  `row_id` int(20) NOT NULL   ;
ALTER TABLE  `rows_columns_widgets` ADD  `column` int(2) NOT NULL   ;
ALTER TABLE  `rows_columns_widgets` ADD  `type` varchar(200) NOT NULL   ;
ALTER TABLE  `rows_columns_widgets` ADD  `options` text NOT NULL   ;
ALTER TABLE  `rows_columns_widgets` ADD  `order` int(2) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Rows_columns_widgets_model extends MY_Model {

	protected $widget_id;
	protected $row_id;
	protected $column;
	protected $type;
	protected $options;
	protected $order;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'rows_columns_widgets';
		$this->_short_name = 'rows_columns_widgets';
		$this->_fields = array("widget_id","row_id","column","type","options","order");
		$this->_required = array("row_id","column","type","options");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: widget_id -------------------------------------- 

	/** 
	* Sets a value to `widget_id` variable
	* @access public
	*/

	public function setWidgetId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('widget_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_widget_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('widget_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `widget_id` variable
	* @access public
	*/

	public function getWidgetId() {
		return $this->widget_id;
	}

	public function get_widget_id() {
		return $this->widget_id;
	}

	
// ------------------------------ End Field: widget_id --------------------------------------


// ---------------------------- Start Field: row_id -------------------------------------- 

	/** 
	* Sets a value to `row_id` variable
	* @access public
	*/

	public function setRowId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_row_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `row_id` variable
	* @access public
	*/

	public function getRowId() {
		return $this->row_id;
	}

	public function get_row_id() {
		return $this->row_id;
	}

	
// ------------------------------ End Field: row_id --------------------------------------


// ---------------------------- Start Field: column -------------------------------------- 

	/** 
	* Sets a value to `column` variable
	* @access public
	*/

	public function setColumn($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_column($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `column` variable
	* @access public
	*/

	public function getColumn() {
		return $this->column;
	}

	public function get_column() {
		return $this->column;
	}

	
// ------------------------------ End Field: column --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_type($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}

	public function get_type() {
		return $this->type;
	}

	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: options -------------------------------------- 

	/** 
	* Sets a value to `options` variable
	* @access public
	*/

	public function setOptions($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('options', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_options($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('options', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `options` variable
	* @access public
	*/

	public function getOptions() {
		return $this->options;
	}

	public function get_options() {
		return $this->options;
	}

	
// ------------------------------ End Field: options --------------------------------------


// ---------------------------- Start Field: order -------------------------------------- 

	/** 
	* Sets a value to `order` variable
	* @access public
	*/

	public function setOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_order($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `order` variable
	* @access public
	*/

	public function getOrder() {
		return $this->order;
	}

	public function get_order() {
		return $this->order;
	}

	
// ------------------------------ End Field: order --------------------------------------



	
	public function get_table_options() {
		return array(
			'widget_id' => (object) array(
										'Field'=>'widget_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'row_id' => (object) array(
										'Field'=>'row_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'column' => (object) array(
										'Field'=>'column',
										'Type'=>'int(2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'options' => (object) array(
										'Field'=>'options',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'order' => (object) array(
										'Field'=>'order',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'widget_id' => "ALTER TABLE  `rows_columns_widgets` ADD  `widget_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'row_id' => "ALTER TABLE  `rows_columns_widgets` ADD  `row_id` int(20) NOT NULL   ;",
			'column' => "ALTER TABLE  `rows_columns_widgets` ADD  `column` int(2) NOT NULL   ;",
			'type' => "ALTER TABLE  `rows_columns_widgets` ADD  `type` varchar(200) NOT NULL   ;",
			'options' => "ALTER TABLE  `rows_columns_widgets` ADD  `options` text NOT NULL   ;",
			'order' => "ALTER TABLE  `rows_columns_widgets` ADD  `order` int(2) NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setWidgetId() - widget_id
//setRowId() - row_id
//setColumn() - column
//setType() - type
//setOptions() - options
//setOrder() - order

--------------------------------------

//set_widget_id() - widget_id
//set_row_id() - row_id
//set_column() - column
//set_type() - type
//set_options() - options
//set_order() - order

*/
/* End of file Rows_columns_widgets_model.php */
/* Location: ./application/models/Rows_columns_widgets_model.php */
