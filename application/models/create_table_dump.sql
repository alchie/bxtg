CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `clients` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` text DEFAULT NULL,
  `phone_number` text DEFAULT NULL,
  `domain` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

CREATE TABLE `clients_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `images` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_ext` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `image_width` int(10) NOT NULL,
  `image_height` int(10) NOT NULL,
  `image_type` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  `upload_url` text DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin;

-- Table structure for table `images` 

CREATE TABLE `layouts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `location` varchar(100) NOT NULL,
  `layout_key` text NOT NULL,
  `layout_value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin;

CREATE TABLE `menus` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin;

CREATE TABLE `menus_links` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `menu_id` int(20) NOT NULL,
  `text` text NOT NULL,
  `url` text NOT NULL,
  `icon` text DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `inner_page` int(20) DEFAULT 0,
  `order` int(2) DEFAULT 0,
  `target` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin;

CREATE TABLE `menus_links_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `link_id` int(20) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `menus_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `menu_id` int(20) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin;

CREATE TABLE `pages` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `page_id` varchar(250) NOT NULL,
  `page_title` varchar(250) NOT NULL,
  `page_slug` varchar(250) NOT NULL,
  `order` int(2) DEFAULT 0,
  `active` int(1) NOT NULL DEFAULT 1,
  `parent_page` int(20) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin;

CREATE TABLE `pages_containers` (
  `client_id` int(20) NOT NULL,
  `id` int(20) NOT NULL DEFAULT 0,
  `page_container` int(10) NOT NULL,
  `opt_key` varchar(100) NOT NULL,
  `opt_value` text NOT NULL,
  KEY `id` (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `pages_options` (
  `client_id` int(20) NOT NULL,
  `id` int(20) NOT NULL DEFAULT 0,
  `opt_key` varchar(250) NOT NULL,
  `opt_value` text NOT NULL,
  KEY `id` (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `pages_rows` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `page_id` int(20) NOT NULL,
  `page_containers` int(2) NOT NULL DEFAULT 1,
  `row_id` int(20) NOT NULL,
  `order` int(2) DEFAULT 0,
  `active` int(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin;

CREATE TABLE `rows` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `columns` int(2) NOT NULL DEFAULT 12,
  `row_class` varchar(200) DEFAULT NULL,
  `container` int(1) DEFAULT 0,
  `container_class` varchar(200) DEFAULT NULL,
  `wrapper_class` varchar(200) DEFAULT NULL,
  `display_rows` int(1) DEFAULT 1,
  `parent_row` int(20) DEFAULT 0,
  `more_settings` text DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `group_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_row` (`parent_row`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin;

CREATE TABLE `rows_columns` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `column` int(2) DEFAULT 1,
  `class` varchar(200) DEFAULT NULL,
  `container` varchar(200) DEFAULT NULL,
  `wrapper` varchar(200) DEFAULT NULL,
  `more_settings` text DEFAULT NULL,
  `column_order` int(2) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `page_id` (`row_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin;

CREATE TABLE `rows_columns_widgets` (
  `widget_id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `column` int(2) NOT NULL,
  `type` varchar(200) NOT NULL,
  `options` text NOT NULL,
  `order` int(2) DEFAULT 0,
  PRIMARY KEY (`widget_id`),
  KEY `row_id` (`row_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin;

CREATE TABLE `rows_groups` (
  `group_id` int(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `client_id` int(20) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `rows_settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `setting_key` varchar(100) NOT NULL,
  `setting_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `user_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

CREATE TABLE `user_accounts_options` (
  `uid` int(20) NOT NULL,
  `department` varchar(200) NOT NULL,
  `section` varchar(200) NOT NULL,
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

CREATE TABLE `user_accounts_restrictions` (
  `uid` int(20) NOT NULL,
  `department` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `view` int(1) NOT NULL DEFAULT 0,
  `add` int(1) NOT NULL DEFAULT 0,
  `edit` int(1) NOT NULL DEFAULT 0,
  `delete` int(1) NOT NULL DEFAULT 0,
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

