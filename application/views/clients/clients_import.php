<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Import Client</h3>
            </div>
            <form action="<?php echo site_url("clients/import"); ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="form-group">
    <input type="file" name="json_file" id="json_file" />
    <input type="hidden" name="action" value="import"  />
</div>

<?php if( isset($output) && ($output=='ajax') ) : ?>
                <button type="submit" class="btn btn-success">Import Now</button>
<?php endif; ?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("system_users"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>