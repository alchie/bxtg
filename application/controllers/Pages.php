<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Pages');
		$this->template_data->set('current_uri', 'pages');

		$this->_isAuth('clients', 'clients', 'view');
		$this->_isAuth('clients', 'clients', 'edit');

		$this->check_current_client();

	}

	public function index($client_id, $start=0) {

		$allpages = new $this->Pages_model('p1');
		$allpages->setClientId($this->session->userdata('current_client'), true);
		$allpages->setParentPage(0, true);
		$allpages->set_select("*");
		$allpages->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");
		$allpages->set_select("(SELECT p3.page_title FROM pages p3 WHERE p3.id=p1.parent_page) as parent_page_title");
		$allpages->set_order('p1.page_title','ASC');
		$allpages->set_limit(10);
		$allpages->set_start($start);

		if( $this->input->get('q') ) {
			$allpages->set_where('(p1.page_title LIKE "%'.$this->input->get('q').'%")');
		}

		$this->template_data->set('allpages', $allpages->populate());
		$this->template_data->set('pages_count', $allpages->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/pages/index/" . $this->session->userdata('current_client')),
			'total_rows' => $allpages->count_all_results(),
			'per_page' => $allpages->get_limit()
		)));

		$this->load->view('pages/pages_list', $this->template_data->get_data());

	}

	public function add($parent_id, $output='') {

		$this->template_data->set('parent_id', $parent_id);

		if( $this->input->post() ) {

			$this->form_validation->set_rules('page_title', 'Page Title', 'trim|required');
			
			if( $this->form_validation->run() ) {
				$page_id = url_title($this->input->post('page_title'), "-", true);
				$page = new $this->Pages_model;
				$page->setClientId($this->session->userdata('current_client'));
				$page->setPageTitle($this->input->post('page_title'));
				$page->setPageId($page_id);
				$page->setPageSlug($page_id);
				$page->setParentPage(($this->input->post('parent_page'))?$this->input->post('parent_page'):0);
				$page->setActive(1);
				$page->insert();
				if( $this->input->get('next')=='subpages' ) {
					$parent_page = $this->input->post('parent_page');
					redirect("pages/subpages/{$parent_page}");
				} else {
					redirect("pages/options/{$this->session->userdata('current_client')}/{$page->getId()}");
				}
			}
		}

		$parent_pages = new $this->Pages_model;
		$parent_pages->setClientId($this->session->userdata('current_client'),true);
		$parent_pages->setParentPage(0,true);
		$parent_pages->set_limit(0);
		$parent_pages->set_order('page_title', 'ASC');
		$this->template_data->set('parent_pages', $parent_pages->populate());

		$this->template_data->set('output', $output);
		$this->load->view('pages/pages_add', $this->template_data->get_data());
	}

	public function edit($id, $output='') {

		$this->template_data->set('location', 'edit');
		$this->template_data->set('page_id', $id);


		$page = new $this->Pages_model('p1');
		$page->setId($id, true,false);
		$page->set_select("*");
		$page->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");

		if( $this->input->post() ) {

			$this->form_validation->set_rules('page_id', 'Page ID', 'trim|required');
			$this->form_validation->set_rules('page_title', 'Page Title', 'trim|required');
			$this->form_validation->set_rules('page_slug', 'Page Slug', 'trim|required');

			if( $this->form_validation->run() ) {

				$page_id = url_title($this->input->post('page_id'), "-", true);
				$page_slug = url_title($this->input->post('page_slug'), "-", true);

				$page->setPageTitle($this->input->post('page_title'),false,true);
				$page->setPageId($page_id,false,true);
				$page->setPageSlug($page_slug,false,true);
				$page->setActive( (($this->input->post('active')) ? 1 : 0),false,true );
				$page->setParentPage( $this->input->post('parent_page'),false,true );
				$page->update();
			}
			$this->postNext();
		}

		$page_data = $page->get();
		$this->template_data->set('page', $page_data);

		$parent_pages = new $this->Pages_model;
		$parent_pages->setClientId($page_data->client_id,true);
		$parent_pages->setParentPage(0,true);
		$parent_pages->set_limit(0);
		$parent_pages->set_order('page_title', 'ASC');
		$this->template_data->set('parent_pages', $parent_pages->populate());

		$this->template_data->set('output', $output);
		$this->load->view('pages/pages_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		$page = new $this->Pages_model;
		$page->setId($id, true);
		$page->setActive(0, true);
		$page_data = $page->get();
		if( $page_data ) {

			$options = new $this->Pages_options_model;
			$options->setClientId($page_data->client_id, true);
			$options->setId($page_data->id, true);
			$options->delete();

			$row = new $this->Pages_rows_model;
			$row->setClientId($page_data->client_id,true);
			$row->setPageId($page_data->id,true);
			$row->delete();

			$page->delete();
		}
		redirect("layout/index/{$page_data->client_id}");
	}

	public function subpages($page_id, $start=0) {

		$this->template_data->set('location', 'subpages');
		$this->template_data->set('page_id', $page_id);

		$page = new $this->Pages_model('p1');
		$page->setId($page_id, true);
		$page->set_select("*");
		$page->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");
		$page_data = $page->get();
		$this->template_data->set('page', $page_data);

		$subpages = new $this->Pages_model('p1');
		$subpages->setClientId($page_data->client_id, true,false);
		$subpages->setParentPage($page_id,true);
		$subpages->set_select("*");
		$subpages->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");
		$subpages->set_start($start);
		$subpages->set_order('page_title', 'ASC');
		$this->template_data->set('subpages', $subpages->populate());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/pages/subpages/" . $page_id),
			'total_rows' => $subpages->count_all_results(),
			'per_page' => $subpages->get_limit()
		)));

		$this->load->view('pages/pages_subpages', $this->template_data->get_data());
	}

	public function options($client_id, $page_id, $output='') {

		$this->template_data->set('location', 'options');
		$this->template_data->set('client_id', $client_id);
		$this->template_data->set('page_id', $page_id);

		if( $this->input->post() ) {
			foreach( $this->input->post('option') as $key=>$value) {
				$options = new $this->Pages_options_model;
				$options->setClientId($client_id, true);
				$options->setId($page_id, true);
				$options->setOptKey($key,true);
				$options->setOptValue($value);
				$value = trim($value);
					if( $options->nonEmpty() ) {
						if( ($value == '') || (empty($value)) ) {
							$options->delete();
						} else {
							$options->set_exclude('id');
							$options->update();
						}
					} else {
						if( ($value != '') || (!empty($value)) ) {
							$options->insert();
						}
					}
			}
		}

		$page = new $this->Pages_model('p1');
		$page->setId($page_id, true);
		$page->set_select("*");
		$page->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");
		$this->template_data->set('page', $page->get());

		$options = new $this->Pages_options_model;
		$options->setClientId($client_id, true);
		$options->setId($page_id, true);
		$this->template_data->set('options', $options->populate());

		$this->template_data->set('output', $output);
		$this->load->view('pages/pages_options', $this->template_data->get_data());
	}


	public function rows($client_id, $page_id, $output='') {

		$this->template_data->set('title', 'Widgets');
		$this->template_data->set('location', 'widgets');
		$this->template_data->set('client_id', $client_id);
		$this->template_data->set('page_id', $page_id);

		if( $this->input->post('order') ) {
			foreach($this->input->post('order') as $order=>$row_id) {
				$row_order = new $this->Pages_rows_model;
				$row_order->setClientId($client_id, true);
				$row_order->setPageId($page_id, true);
				$row_order->setRowId($row_id, true);
				$row_order->setOrder($order,false,true);
				$row_order->update();
			}
		}

		if( $this->input->post('container') ) {
				foreach( $this->input->post('container') as $page_container_n => $container_options) {
								foreach( $container_options as $option_key => $option_value ) {
										$containers_model = new $this->Pages_containers_model();
										$containers_model->setClientId($client_id,true,true);
										$containers_model->setId($page_id,true,true);
										$containers_model->setPageContainer($page_container_n,true,true);
										$containers_model->setOptKey($option_key,true,true);
										$containers_model->setOptValue($option_value,false,true);
										if( $containers_model->nonEmpty() ) {
											$containers_model->update();
										} else {
											$containers_model->insert();
										}
								}					
				}
					
		}

		$page = new $this->Pages_model('p1');
		$page->setId($page_id, true);
		$page->set_select("*");
		$page->set_select("(SELECT COUNT(*) FROM pages p2 WHERE p2.parent_page=p1.id) as subpages");
		$this->template_data->set('page', $page->get());

		$options = new $this->Pages_options_model;
		$options->setClientId($client_id, true);
		$options->setId($page_id, true);
		$this->template_data->set('options', $options->populate());

		$row = new $this->Pages_rows_model('pr');
		$row->set_join('rows r', 'r.id=pr.row_id');
		$row->set_select("*");
		$row->set_select("pr.id as pr_id");
		$row->setClientId($client_id,true);
		$row->setPageId($page_id,true);
		$row->_set_order('order', 'ASC');
		$this->template_data->set('rows', $row->recursive('r.parent_row', 0));

		$page_containers = new $this->Pages_containers_model;
		$page_containers->setClientId($client_id, true);
		$page_containers->setId($page_id, true);
		$this->template_data->set('page_containers', $page_containers->populate());

		$this->template_data->set('output', $output);
		$this->load->view('pages/pages_rows_list', $this->template_data->get_data());
				
	}

	public function add_row($client_id, $page_id, $output='') {

		$this->template_data->set('client_id', $client_id);
		$this->template_data->set('page_id', $page_id);

		if( $this->input->post('row') ) {
			foreach( $this->input->post('row') as $row_id) {
				$row = new $this->Pages_rows_model;
				$row->setClientId($client_id, true);
				$row->setPageId($page_id,true);
				$row->setRowId($row_id,true);
				$row->setPageContainers( ($this->input->get('container'))?$this->input->get('container'):1 );
				$row->setActive(1);
				if( $row->nonEmpty() ) {
					$row->update();
				} else {
					$row->insert();
				}
			}
			redirect("pages/rows/{$client_id}/{$page_id}");
		}

		$page = new $this->Pages_model;
		$page->setId($page_id, true);
		$this->template_data->set('page', $page->get());

		$options = new $this->Pages_options_model;
		$options->setClientId($client_id, true);
		$options->setId($page_id, true);
		$this->template_data->set('options', $options->populate());

		$rows = new $this->Rows_model;
		$rows->setClientId($client_id, true);
		$rows->setActive(1, true);
		$rows->set_where("((SELECT COUNT(*) FROM pages_rows pr WHERE pr.client_id={$client_id} AND pr.page_id={$page_id} AND pr.row_id=rows.id) = 0)");
		$rows->set_limit(0);
		$rows->set_order('title', 'ASC');
		$this->template_data->set('rows', $rows->populate());
		
		$this->template_data->set('output', $output);
		$this->load->view('pages/pages_add_row', $this->template_data->get_data());
	}

	public function delete_row($client_id, $page_id, $row_id, $output='') {
		$row = new $this->Pages_rows_model;
		$row->setClientId($client_id, true);
		$row->setPageId($page_id,true);
		$row->setRowId($row_id,true);
		$row->delete();

		redirect("pages/rows/{$client_id}/{$page_id}");
	}

	public function move_to_container($page_row_id, $container=1) {
		
		$row = new $this->Pages_rows_model;
		$row->setId($page_row_id,true,false);
		$row->setPageContainers($container,false,true);
		$row->update();
		
		redirect($this->input->get('back'));
	}

}
