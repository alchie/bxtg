    <div class="banner carousel slide" id="recommended-item-carousel" data-ride="carousel">
      <div class="slides carousel-inner">

<?php 
$item_n = 0;
if( $widget_options ) foreach($widget_options->items as $item) { ?>
        <div class="item <?php echo ($item_n==0) ? 'active' : ''; ?>">
<?php if( $item->image_link != '') { ?>
<a href="<?php echo $item->image_link; ?>">
<?php } ?>
          <img src="<?php echo $item->image_url; ?>"  />
<?php if( $item->image_link != '') { ?>
</a>
<?php } ?>
<?php if($item->show_caption==1) { ?>
          <div class="banner_caption">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <div class="caption_inner animated fadeInUp">
                    <?php if($item->show_title==1) { ?><h1><?php echo $item->image_title; ?></h1><?php } ?>
                    <?php if($item->show_desc==1) { ?><p><?php echo $item->image_desc; ?></p><?php } ?>
                  </div><!--end caption_inner-->
                </div>
              </div><!--end row-->
            </div><!--end container-->
          </div><!--end banner_caption-->
<?php } ?>
        </div>
<?php 
$item_n++;
} ?>
      </div>
<?php 
if( $template_output == 'demo' ) {
  $prev = base_url('assets/templates/royal_college/images/prev.png');
  $next = base_url('assets/templates/royal_college/images/next.png');
} else {
  $prev = 'https://1.bp.blogspot.com/-ENmR_q2MCLQ/WCqZvPKL2TI/AAAAAAAALwY/B4pReByQBnI0st3xVpooIJcCkdT2mbDogCLcB/s1600/prev.png';
  $next = 'https://1.bp.blogspot.com/-_SkVT4iJzOE/WCqZuYM1TII/AAAAAAAALwI/o0G0lu4aBeYnEdCHUnbRJBVgzD2fcaQHACLcB/s1600/next.png';
}
?>
      <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">

        <img src="<?php echo $prev; ?>"  />
        </a>
      <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
        <img src="<?php echo $next; ?>"  />
      </a>    
    </div><!--end banner-->
