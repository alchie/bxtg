<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
	<form method="post">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<a data-title="Add Menu" href="<?php echo site_url("menus/add/{$current_client->id}/ajax"); ?>" class="btn btn-success btn-xs pull-right ajax-modal"><span class="glyphicon glyphicon-plus"></span> Add Menu</a>
	    		<h3 class="panel-title">Menus</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<ul class="list-group">
<?php foreach($menus as $menu) { ?>
  <a href="<?php echo site_url("menus/links/{$menu->id}"); ?>" class="list-group-item">
  	<span class="badge"><?php echo $menu->links; ?></span>
  	<?php echo $menu->name; ?>
  </a>
<?php } ?>
</ul>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
	    </form>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>