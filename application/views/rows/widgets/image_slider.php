<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$items = false;
$i=0;
if( isset($widget) && ($widget) ) {
    $options = json_decode( $widget->options );
    $items = $options->items;
    foreach($items as $item1) {
        $i++;
    }
}
?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<script>
    <!--
        var image_slider_count = <?php echo $i; ?>;
    -->
</script>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
<button type="button" class="btn btn-warning btn-xs pull-right" id="widget_add_image_slider_button" data-action="add"><span class="glyphicon glyphicon-plus"></span> Add Slider Item</button>

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">Image Slider
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>
                </h3>
            </div>
          
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="list-group sortable" id="image_slider_group">
<?php 
$key = 0;
if( $items ) foreach( $items as $item ) { ?>
    <div class="list-group-item" id="image_slider_item<?php echo $key; ?>">

<?php foreach($item as $item_key => $item_value) { ?>
     <input type="hidden" id="<?php echo $item_key; ?><?php echo $key; ?>" name="items[<?php echo $key; ?>][<?php echo $item_key; ?>]" value="<?php echo (isset($item_value)) ? $item_value : ''; ?>">
<?php } ?>
  
    <span id="image_slider_item_title<?php echo $key; ?>"><?php echo $item->image_title; ?></span>

 <a href="javascript:void(0);" class="image_slider_item_duplicate btn btn-warning btn-xs pull-right" data-id="<?php echo $key; ?>" style="margin-left:5px;">Duplicate
    </a>

    <a href="javascript:void(0);" class="image_slider_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;" data-id="<?php echo $key; ?>">Delete
    </a> 

    <a href="javascript:void(0);" class="image_slider_item_edit btn btn-success btn-xs pull-right" data-id="<?php echo $key; ?>">Edit
    </a>
    </div>
<?php $key++; } ?>
</div>


<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="imageSliderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Image</h4>
      </div>
      <div class="modal-body">
        
    <div class="form-group">
        <label for="image_id">Image</label>
          <select class="form-control" id="image_id">
              <option value="">-- No Image --</option>
              <option id="image_id_item_sample_1" value="sample_1">- Sample Image 1 -</option>
              <option id="image_id_item_sample_2" value="sample_2">- Sample Image 2 -</option>
              <option id="image_id_item_sample_3" value="sample_3">- Sample Image 3 -</option>
              <option id="image_id_item_sample_4" value="sample_4">- Sample Image 4 -</option>
              <?php foreach($images as $image) { ?>
                  <option id="image_id_item_<?php echo $image->id; ?>" value="<?php echo $image->id; ?>"><?php echo $image->title; ?> - <?php echo $image->file_name; ?></option>
              <?php } ?>
          </select>
    </div>

    <div class="form-group">
        <label for="image_title">Image Title</label>
        <input type="text" class="form-control" placeholder="Image Title" id="image_title">
    </div>

    <div class="form-group">
        <label for="image_desc">Image Description</label>
        <textarea class="form-control" placeholder="Image Description" id="image_desc"></textarea>
    </div>

    <div class="form-group">
        <label for="image_link">Image Link</label>
        <input type="text" class="form-control" placeholder="Image Link" id="image_link">
    </div>

    <div class="form-group">
        <label for="image_link_target">Image Link Target</label>
        <input type="text" class="form-control" placeholder="Image Link" id="image_link_target">
    </div>

    <div class="form-group">
        <label for="image_page">Inner Page</label>
        <select class="form-control" id="image_page">
            <option value="0">No Inner Page</option>
            <?php 
            foreach($pages as $page) { ?>
                <option id="image_page_item_<?php echo $page->id; ?>" value="<?php echo $page->id; ?>"><?php echo $page->page_title; ?> (<?php echo $page->page_slug; ?>)</option>
            <?php } ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="show_caption"><input type="checkbox" id="show_caption" value="1"> Show Caption</label><br>
        <label for="show_title"><input type="checkbox" id="show_title" value="1"> Show Title</label><br>
        <label for="show_desc"><input type="checkbox" id="show_desc" value="1"> Show Description</label>
    </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="widget_image_slider_submit">Submit</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>