.section, .widget {
    margin: 0;
}
.ftco-navbar-light {
    height: inherit!important;
    padding: 8px 16px!important;
    margin: inherit!important;
}
.hero-wrap {
  filter: gray; 
  -webkit-filter: grayscale(1); 
  filter: grayscale(1); 
}

.gray-hover-colored {
  filter: gray;
  -webkit-filter: grayscale(1); 
  filter: grayscale(1); 
  z-index:1;
}

.gray-hover-colored:hover {
  filter: none;
  -webkit-filter: grayscale(0); 
  filter: grayscale(0); 
  z-index:1;
  position:relative;
}
.cause-entry .text,
.blog-entry .text {
	position:relative;
	z-index:99;
}
.ftco-intro .block-18.color-1 {
    background: #972a47;
}
.ftco-intro .block-18.color-2 {
    background: #1d807d;
}
.ftco-intro .block-18.color-3 {
    background: #28b3b0;
}
.ftco-navbar-light.scrolled {
    box-shadow: 0 0 10px 0 rgba(0,0,0,.5);
    z-index:999;
}