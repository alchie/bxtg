<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rows_groups_model Class
 *
 * Manipulates `rows_groups` table on database

CREATE TABLE `rows_groups` (
  `group_id` int(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `client_id` int(20) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `rows_groups` ADD  `group_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `rows_groups` ADD  `group_name` varchar(100) NOT NULL   ;
ALTER TABLE  `rows_groups` ADD  `client_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Rows_groups_model extends MY_Model {

	protected $group_id;
	protected $group_name;
	protected $client_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'rows_groups';
		$this->_short_name = 'rows_groups';
		$this->_fields = array("group_id","group_name","client_id");
		$this->_required = array("group_name","client_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: group_id -------------------------------------- 

	/** 
	* Sets a value to `group_id` variable
	* @access public
	*/

	public function setGroupId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_group_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_id` variable
	* @access public
	*/

	public function getGroupId() {
		return $this->group_id;
	}

	public function get_group_id() {
		return $this->group_id;
	}

	
// ------------------------------ End Field: group_id --------------------------------------


// ---------------------------- Start Field: group_name -------------------------------------- 

	/** 
	* Sets a value to `group_name` variable
	* @access public
	*/

	public function setGroupName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_group_name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_name` variable
	* @access public
	*/

	public function getGroupName() {
		return $this->group_name;
	}

	public function get_group_name() {
		return $this->group_name;
	}

	
// ------------------------------ End Field: group_name --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'group_id' => (object) array(
										'Field'=>'group_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'group_name' => (object) array(
										'Field'=>'group_name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'group_id' => "ALTER TABLE  `rows_groups` ADD  `group_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'group_name' => "ALTER TABLE  `rows_groups` ADD  `group_name` varchar(100) NOT NULL   ;",
			'client_id' => "ALTER TABLE  `rows_groups` ADD  `client_id` int(20) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setGroupId() - group_id
//setGroupName() - group_name
//setClientId() - client_id

--------------------------------------

//set_group_id() - group_id
//set_group_name() - group_name
//set_client_id() - client_id

*/
/* End of file Rows_groups_model.php */
/* Location: ./application/models/Rows_groups_model.php */
