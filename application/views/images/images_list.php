<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	<form method="post">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("images/upload"); ?>" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Upload</a>	
	    		<h3 class="panel-title">Images</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<div class="row">
<?php foreach($images as $image) { ?>
  <div class="col-xs-4 col-md-2">
  <div class="thumbnail">
      <img src="<?php echo base_url("uploads/{$image->file_name}"); ?>" />
      <div class="caption text-center">
        <a href="<?php echo site_url("images/edit_image/{$image->id}") . "?back=" . uri_string(); ?>" class="btn btn-success btn-xs" role="button">Edit</a>
        <a href="<?php echo site_url("images/delete_image/{$image->id}") . "?back=" . uri_string(); ?>" class="confirm btn btn-danger btn-xs" role="button">Delete</a>
      </div>
    </div>
  </div>

<?php } ?>
</div>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
	    </form>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>