<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('pages/pages_header_nav'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	<form method="post">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
        <a data-title="Add Page" href="<?php echo site_url("pages/add/{$page->id}/ajax"); ?>?next=subpages" class="btn btn-success btn-xs pull-right ajax-modal"><span class="glyphicon glyphicon-plus"></span> Add a Page</a>
	    		<h3 class="panel-title">Pages</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<div class="list-group sortable">
  <?php if( $subpages ) foreach($subpages as $page) { ?>
      <div class="list-group-item <?php echo ($page->active==1) ? '' : 'danger'; ?>">
      <input type="hidden" name="page_order[]" value="<?php echo $page->id; ?>" />
        <div class="btn-group pull-right">
          <a type="button" class="btn btn-success btn-xs" href="<?php echo site_url("pages/edit/{$page->id}"); ?>">Edit Page</a>
          <a type="button" class="btn btn-primary btn-xs" href="<?php echo site_url("pages/options/{$current_client->id}/{$page->id}"); ?>">Options</a>
          <a type="button" class="btn btn-info btn-xs" href="<?php echo site_url("pages/rows/{$current_client->id}/{$page->id}"); ?>">Widgets</a>
        </div>
        <strong><?php echo $page->page_title; ?></strong>
      </div>
  <?php } ?>
</div>

<?php echo $pagination; ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
	    </form>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>