<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pages_rows_model Class
 *
 * Manipulates `pages_rows` table on database

CREATE TABLE `pages_rows` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `page_id` int(20) NOT NULL,
  `page_containers` int(2) NOT NULL DEFAULT 1,
  `row_id` int(20) NOT NULL,
  `order` int(2) DEFAULT 0,
  `active` int(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin;

ALTER TABLE  `pages_rows` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `pages_rows` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `pages_rows` ADD  `page_id` int(20) NOT NULL   ;
ALTER TABLE  `pages_rows` ADD  `page_containers` int(2) NOT NULL   DEFAULT '1';
ALTER TABLE  `pages_rows` ADD  `row_id` int(20) NOT NULL   ;
ALTER TABLE  `pages_rows` ADD  `order` int(2) NULL   DEFAULT '0';
ALTER TABLE  `pages_rows` ADD  `active` int(1) NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Pages_rows_model extends MY_Model {

	protected $id;
	protected $client_id;
	protected $page_id;
	protected $page_containers;
	protected $row_id;
	protected $order;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'pages_rows';
		$this->_short_name = 'pages_rows';
		$this->_fields = array("id","client_id","page_id","page_containers","row_id","order","active");
		$this->_required = array("client_id","page_id","page_containers","row_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: page_id -------------------------------------- 

	/** 
	* Sets a value to `page_id` variable
	* @access public
	*/

	public function setPageId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_id` variable
	* @access public
	*/

	public function getPageId() {
		return $this->page_id;
	}

	public function get_page_id() {
		return $this->page_id;
	}

	
// ------------------------------ End Field: page_id --------------------------------------


// ---------------------------- Start Field: page_containers -------------------------------------- 

	/** 
	* Sets a value to `page_containers` variable
	* @access public
	*/

	public function setPageContainers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_containers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_containers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_containers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_containers` variable
	* @access public
	*/

	public function getPageContainers() {
		return $this->page_containers;
	}

	public function get_page_containers() {
		return $this->page_containers;
	}

	
// ------------------------------ End Field: page_containers --------------------------------------


// ---------------------------- Start Field: row_id -------------------------------------- 

	/** 
	* Sets a value to `row_id` variable
	* @access public
	*/

	public function setRowId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_row_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `row_id` variable
	* @access public
	*/

	public function getRowId() {
		return $this->row_id;
	}

	public function get_row_id() {
		return $this->row_id;
	}

	
// ------------------------------ End Field: row_id --------------------------------------


// ---------------------------- Start Field: order -------------------------------------- 

	/** 
	* Sets a value to `order` variable
	* @access public
	*/

	public function setOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_order($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `order` variable
	* @access public
	*/

	public function getOrder() {
		return $this->order;
	}

	public function get_order() {
		return $this->order;
	}

	
// ------------------------------ End Field: order --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'page_id' => (object) array(
										'Field'=>'page_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'page_containers' => (object) array(
										'Field'=>'page_containers',
										'Type'=>'int(2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'row_id' => (object) array(
										'Field'=>'row_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'order' => (object) array(
										'Field'=>'order',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `pages_rows` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'client_id' => "ALTER TABLE  `pages_rows` ADD  `client_id` int(20) NOT NULL   ;",
			'page_id' => "ALTER TABLE  `pages_rows` ADD  `page_id` int(20) NOT NULL   ;",
			'page_containers' => "ALTER TABLE  `pages_rows` ADD  `page_containers` int(2) NOT NULL   DEFAULT '1';",
			'row_id' => "ALTER TABLE  `pages_rows` ADD  `row_id` int(20) NOT NULL   ;",
			'order' => "ALTER TABLE  `pages_rows` ADD  `order` int(2) NULL   DEFAULT '0';",
			'active' => "ALTER TABLE  `pages_rows` ADD  `active` int(1) NULL   DEFAULT '1';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setClientId() - client_id
//setPageId() - page_id
//setPageContainers() - page_containers
//setRowId() - row_id
//setOrder() - order
//setActive() - active

--------------------------------------

//set_id() - id
//set_client_id() - client_id
//set_page_id() - page_id
//set_page_containers() - page_containers
//set_row_id() - row_id
//set_order() - order
//set_active() - active

*/
/* End of file Pages_rows_model.php */
/* Location: ./application/models/Pages_rows_model.php */
