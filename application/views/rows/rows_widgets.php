<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>


<?php 
    $more_settings = (isset($column_settings->more_settings)) ? json_decode( $column_settings->more_settings ) : false;
?>

 <form method="post">

<?php endif; ?>

<div class="container">
<div class="row">

    <div class="col-md-6">
<div class="panel panel-default">
            <div class="panel-heading">

<?php if( $column_settings ) { ?>

                <a class="btn btn-danger pull-right btn-xs confirm" style="margin-right:5px;" href="<?php echo site_url("rows/delete_column/{$column_settings->id}"); ?>">Delete</a>

                 <button type="submit" class="btn btn-success pull-right btn-xs" style="margin-right:5px;">Save</button>
<?php } else { ?>
     <button type="submit" class="btn btn-success pull-right btn-xs" style="margin-right:5px;">Add Column</button>
<?php } ?>
            Column Settings

 <label><input name="more_settings[generate]" type="checkbox" value="1" <?php echo (isset($more_settings->generate)) ? 'CHECKED' : ''; ?>> Generate</label>

        </div>

<div class="panel-body">

<div class="row">
<?php foreach(array('col_lg', 'col_md', 'col_sm', 'col_xs') as $setting) { ?>
    <div class="col-md-3">
                <div class="form-group">
                    <label><?php echo $setting; ?>-*</label>
                    <select name="more_settings[<?php echo $setting; ?>]" class="form-control">
                    <option value="0">Hidden</option>
                    <?php for($i=1;$i<=12;$i++) { ?>
                        <option <?php echo (isset($more_settings->$setting)&&($more_settings->$setting==$i)) ? 'selected' : ''; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                    </select>
                </div>
    </div>
<?php } ?>
</div>

<div class="row">
<?php foreach(array('col_lg_offset', 'col_md_offset', 'col_sm_offset', 'col_xs_offset') as $setting) { ?>
    <div class="col-md-3">
                <div class="form-group">
                    <label><?php echo $setting; ?>-*</label>
                    <select name="more_settings[<?php echo $setting; ?>]" class="form-control">
                    <option value="0">No Offset</option>
                    <?php for($i=1;$i<=12;$i++) { ?>
                        <option <?php echo (isset($more_settings->$setting)&&($more_settings->$setting==$i)) ? 'selected' : ''; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                    </select>
                </div>
    </div>
<?php } ?>
</div>

<div class="row">
<?php foreach(array('visible_lg', 'visible_md', 'visible_sm', 'visible_xs') as $setting) { ?>
    <div class="col-md-3">
                <div class="form-group">
                    <label><?php echo $setting; ?>-*</label>
                    <select name="more_settings[<?php echo $setting; ?>]" class="form-control">
                    <option value="">none</option>
                    <?php foreach(array('block','inline','inline-block') as $option) { ?>
                        <option <?php echo (isset($more_settings->$setting)&&($more_settings->$setting==$option)) ? 'selected' : ''; ?> value="<?php echo $option; ?>"><?php echo $option; ?></option>
                    <?php } ?>
                    </select>
                </div>
    </div>
<?php } ?>
</div>

<div class="row">
    <div class="col-md-4">
                <div class="form-group">
                    <label>Column Class</label>
                    <input name="column_settings[column_class]" type="text" class="form-control" value="<?php echo (isset($column_settings->class)) ? $column_settings->class : ''; ?>">
                </div>

    </div>
    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget Container</label>
                    <input name="column_settings[widget_container]" type="text" class="form-control" value="<?php echo (isset($column_settings->container)) ? $column_settings->container : ''; ?>">
                </div>
    </div>
    <div class="col-md-4">
                <div class="form-group">
                    <label>Column Wrapper</label>
                    <input name="column_settings[column_wrapper]" type="text" class="form-control" value="<?php echo (isset($column_settings->wrapper)) ? $column_settings->wrapper : ''; ?>">
                </div>
    </div>
</div>

</div>
</div>

    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
<a data-title="Add Widget" href="<?php echo site_url("rows/select_widget/{$row_id}/{$column}/ajax"); ?>" data-hide_footer="1" class="pull-right btn btn-primary btn-xs ajax-modal">Add Widget</a>
<button type="submit" class="btn btn-success pull-right btn-xs" style="margin-right: 5px;">Save</button>
                <h3 class="panel-title">Widgets</h3>
            </div>
           
            <div class="panel-body">


<?php if( $widgets ) { ?>
<?php $widget_types = unserialize( WIDGET_TYPES ); ?>
<div class="list-group sortable">
    <?php foreach($widgets as $widget) { 
$widget_options = json_decode($widget->options);
        ?>
        <div class="list-group-item <?php echo (($this->input->get('highlight'))&&($this->input->get('highlight')==$widget->widget_id)) ? "active" : ""; ?>">
            <input type="hidden" name="order[]" value="<?php echo $widget->widget_id; ?>">
            <?php echo $widget_types[$widget->type]; ?>

            <?php echo (isset($widget_options->title)) ? " - <strong>".$widget_options->title . "</strong>" : ""; ?>

            <div class="btn-group pull-right">

<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>

            <a href="<?php echo site_url("rows/edit_widget/{$widget->widget_id}"); ?>" class="btn btn-success btn-xs">Edit</a>
            <a href="<?php echo site_url("rows/delete_widget/{$widget->widget_id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
            <a href="<?php echo site_url("rows/duplicate_widget/{$widget->widget_id}"); ?>" class="btn btn-warning btn-xs confirm">Duplicate</a>
            </div>
        </div>
    <?php } ?>
</div>
<?php } ?>




            </div>
            
        </div>
    </div>

</div>
</div>
<?php if( isset($output) && ($output!='ajax') ) : ?>

    </form>

<?php $this->load->view('footer'); ?>

<?php endif; ?>