<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$items = false;
$i=0;
if( isset($widget) && ($widget) ) {
    $options = json_decode( $widget->options );
    if( isset($options->items) ) {
      $items = $options->items;
      foreach($items as $item1) {
          $i++;
      }
    }
}
?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<script>
    <!--
        var link_count = <?php echo $i; ?>;
    -->
</script>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">List Widget
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>
                </h3>
            </div>
          
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">

    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget Name</label>
                    <input placeholder="Widget Name" name="title" type="text" class="form-control" value="<?php echo isset($options->title) ? $options->title : ''; ?>">
                </div>
    </div>

    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget ID</label>
                    <input placeholder="Widget ID" name="widget_id" type="text" class="form-control" value="<?php echo isset($options->widget_id) ? $options->widget_id : ''; ?>">
                </div>
    </div>

    <div class="col-md-4">
                <div class="form-group">
                    <label>List Class</label>
                    <input placeholder="List Class" name="list_class" type="text" class="form-control" value="<?php echo isset($options->list_class) ? $options->list_class : ''; ?>">
                </div>
    </div>

</div>

<div class="panel panel-default">
    <div class="panel-heading">
            <button type="button" class="btn btn-warning btn-xs pull-right" id="widget_add_link_button" data-action="add">Add Item</button>
    List Items</div>
  <div class="panel-body">

<div class="list-group sortable" id="list_group">
<?php 
$key = 0;
if( $items ) foreach( $items as $item ) {  ?>
    <div class="list-group-item" id="list_item<?php echo $key; ?>">
<?php foreach($item as $item_key => $item_value) { ?>
     <input type="hidden" id="<?php echo $item_key; ?><?php echo $key; ?>" name="items[<?php echo $key; ?>][<?php echo $item_key; ?>]" value="<?php echo (isset($item_value)) ? $item_value : ''; ?>">
<?php } ?>
    
    <span id="list_item_title<?php echo $key; ?>"><?php echo $item->link_text; ?></span>
     <a href="javascript:void(0);" class="list_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;" data-id="<?php echo $key; ?>">Delete
    </a>
    <a href="javascript:void(0);" class="list_item_edit btn btn-warning btn-xs pull-right" data-id="<?php echo $key; ?>">Edit
    </a>
    </div>
<?php $key++; } ?>
</div>

  </div>
</div>




<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="linkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Link</h4>
      </div>
      <div class="modal-body">
        
    <div class="form-group">
        <label for="link_text">Text</label>
        <input type="text" class="form-control" placeholder="Text" id="link_text">
    </div>

    <div class="form-group">
        <label for="link_desc">Description</label>
        <textarea class="form-control" placeholder="Description" id="link_desc"></textarea>
    </div>

<div class="form-group">
        <label for="link_link">Link</label>
        <input type="text" class="form-control" placeholder="Link" id="link_link">
    </div>

<div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label for="link_icon">Icon</label>
          <input type="text" class="form-control" placeholder="Icon" id="link_icon">
      </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="link_class">Class</label>
            <input type="text" class="form-control" placeholder="Class" id="link_class">
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label for="link_page">Inner Page</label>
          <select name="link_page" class="form-control" id="link_page">
              <option value="0">No Inner Page</option>
              <?php 
              foreach($pages as $page) { ?>
                  <option id="link_page_item_<?php echo $page->id; ?>" value="<?php echo $page->id; ?>"><?php echo $page->page_title; ?> (<?php echo $page->page_slug; ?>)</option>
              <?php } ?>
          </select>
      </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
        <label for="link_target">Link Target</label>
        <input type="text" class="form-control" placeholder="Link Target" id="link_target">
    </div>
    </div>
</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="widget_link_submit">Submit</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>