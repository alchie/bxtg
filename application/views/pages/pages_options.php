<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('pages/pages_header_nav'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo ($page) ? $page->page_title : "Homepage"; ?></h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if($page) { ?>
<div class="row">
    <div class="col-md-12">
            <div class="form-group">
                <label>Page Width</label>
                <select name="option[page_width]" class="form-control" title="-- Use Default --">
                    <option value="__default__">-- Use Default --</option>
                <?php 
                foreach(array('wide','boxed') as $theme) { ?>
                  <option value="<?php echo $theme; ?>" <?php echo (get_option_value('page_width', $options)==$theme) ? 'SELECTED' : ''; ?>><?php echo ucfirst($theme); ?></option>
                <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Rows Container</label>
                <input name="option[rows_container]" type="text" class="form-control" value="<?php echo get_option_value('rows_container', $options); ?>">
            </div>

 <div class="form-group">
                <label>Page Container Tag</label>
                <input name="option[page_container_tag]" type="text" class="form-control" value="<?php echo get_option_value('page_container_tag', $options); ?>">
            </div>

 <div class="form-group">
                <label>Page Container ID</label>
                <input name="option[page_container_id]" type="text" class="form-control" value="<?php echo get_option_value('page_container_id', $options); ?>">
            </div>

 <div class="form-group">
                <label>Page Container Class</label>
                <input name="option[page_container_class]" type="text" class="form-control" value="<?php echo get_option_value('page_container_class', $options); ?>">
            </div>

    </div>
</div>
<?php } ?>

<?php if( isset($output) && ($output=='ajax') ) : ?>
    <a href="" class="btn btn-danger btn-xs">Delete this page</a>
<?php endif; ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("layout/index/{$current_client->id}"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>