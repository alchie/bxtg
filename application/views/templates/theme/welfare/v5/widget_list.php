<?php if( isset($widget_options) && ($widget_options) ) { ?>
<?php if( isset($widget_options->items) && ($widget_options->items) ) { ?>
<ul class="<?php echo $widget_options->list_class; ?>">
	<?php foreach($widget_options->items as $item) { ?>
		<li>
		<?php 
$page = false;
$link_url = $item->link_link; 
if( (($item->link_page != '') && ($item->link_page > 0)) ) {
	$page = get_page($item->link_page);
	$link_url = site_url("templates/view/{$current_client->id}/royal/" . $item->link_page);
	if( $template_output == 'blogger_xml') {
		$link_url = "http://" . $current_client->domain . "/search/label/" . $page->page_slug;
	}
}
?>
<?php if( ($item->link_link != '') || ($page) ) { ?>
		<a href="<?php echo $link_url; ?>">
		<?php } ?>
			<?php echo htmlentities($item->link_text); ?>
<?php if( ($item->link_link != '') || ($page) ) { ?>
		</a>
		<?php } ?>
		</li>
	<?php } ?>
</ul>
<?php } ?>
<?php } ?>