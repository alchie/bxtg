<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Layouts_model Class
 *
 * Manipulates `layouts` table on database

CREATE TABLE `layouts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `location` varchar(100) NOT NULL,
  `layout_key` text NOT NULL,
  `layout_value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin;

ALTER TABLE  `layouts` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `layouts` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `layouts` ADD  `location` varchar(100) NOT NULL   ;
ALTER TABLE  `layouts` ADD  `layout_key` text NOT NULL   ;
ALTER TABLE  `layouts` ADD  `layout_value` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Layouts_model extends MY_Model {

	protected $id;
	protected $client_id;
	protected $location;
	protected $layout_key;
	protected $layout_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'layouts';
		$this->_short_name = 'layouts';
		$this->_fields = array("id","client_id","location","layout_key","layout_value");
		$this->_required = array("client_id","location","layout_key");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: location -------------------------------------- 

	/** 
	* Sets a value to `location` variable
	* @access public
	*/

	public function setLocation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('location', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_location($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('location', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `location` variable
	* @access public
	*/

	public function getLocation() {
		return $this->location;
	}

	public function get_location() {
		return $this->location;
	}

	
// ------------------------------ End Field: location --------------------------------------


// ---------------------------- Start Field: layout_key -------------------------------------- 

	/** 
	* Sets a value to `layout_key` variable
	* @access public
	*/

	public function setLayoutKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('layout_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_layout_key($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('layout_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `layout_key` variable
	* @access public
	*/

	public function getLayoutKey() {
		return $this->layout_key;
	}

	public function get_layout_key() {
		return $this->layout_key;
	}

	
// ------------------------------ End Field: layout_key --------------------------------------


// ---------------------------- Start Field: layout_value -------------------------------------- 

	/** 
	* Sets a value to `layout_value` variable
	* @access public
	*/

	public function setLayoutValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('layout_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_layout_value($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('layout_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `layout_value` variable
	* @access public
	*/

	public function getLayoutValue() {
		return $this->layout_value;
	}

	public function get_layout_value() {
		return $this->layout_value;
	}

	
// ------------------------------ End Field: layout_value --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'location' => (object) array(
										'Field'=>'location',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'layout_key' => (object) array(
										'Field'=>'layout_key',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'layout_value' => (object) array(
										'Field'=>'layout_value',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `layouts` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'client_id' => "ALTER TABLE  `layouts` ADD  `client_id` int(20) NOT NULL   ;",
			'location' => "ALTER TABLE  `layouts` ADD  `location` varchar(100) NOT NULL   ;",
			'layout_key' => "ALTER TABLE  `layouts` ADD  `layout_key` text NOT NULL   ;",
			'layout_value' => "ALTER TABLE  `layouts` ADD  `layout_value` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setClientId() - client_id
//setLocation() - location
//setLayoutKey() - layout_key
//setLayoutValue() - layout_value

--------------------------------------

//set_id() - id
//set_client_id() - client_id
//set_location() - location
//set_layout_key() - layout_key
//set_layout_value() - layout_value

*/
/* End of file Layouts_model.php */
/* Location: ./application/models/Layouts_model.php */
