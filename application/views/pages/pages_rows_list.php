<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('pages/pages_header_nav'); ?>

<?php function display_rows($rows, $container=0, $containers=1) { ?>
<ul class="list-group sortable" style="margin-bottom: 0px;">
<?php foreach($rows as $row) { 
    if( $row->page_containers != $container) {
        continue;
    }
    ?>
    <li class="list-group-item">
        <input type="hidden" name="order[]" value="<?php echo $row->id; ?>">
<?php if( $containers > 1) { ?>
<!-- Single button -->
<div class="btn-group btn-group-xs">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to container <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php for($i=1;$i<=$containers;$i++) { 
if($i==$container) {
    continue;
}
    ?>
    <li><a href="<?php echo site_url("pages/move_to_container/{$row->pr_id}/{$i}") . "?back=" . uri_string(); ?>">Container <?php echo $i; ?></a></li>
<?php } ?>
  </ul>
</div>
<?php } ?>

        <a href="<?php echo site_url("rows/edit_row/{$row->id}/{$row->page_id}"); ?>" class="btn btn-warning btn-xs">Edit</a>
        <a href="<?php echo site_url("pages/delete_row/{$row->client_id}/{$row->page_id}/{$row->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>

<?php if( $row->display_rows == 1 ) { ?>
<div class="btn-group">
    <?php for($col=1;$col<=$row->columns;$col++) { ?>
        <a href="<?php echo site_url("rows/widgets/{$row->id}/{$col}/{$row->page_id}"); ?>" class="btn btn-default btn-xs"><?php echo $col; ?></a>
    <?php } ?>
</div>
<?php } ?>

        <span class="badge"><?php echo $row->title; ?></span>
        <span class="clearfix"></span>
<?php 
    if($row->children) {
        echo "<br>";
        display_rows($row->children);
    } 
?>
    </li>
<?php } ?>
</ul>
<?php } ?>

<div class="container">
<div class="row">
<form method="post">
    <div class="col-md-12">

<?php $containers = (get_option_value('rows_container', $options) > 0) ? get_option_value('rows_container', $options) : 1; 

for($i=1;$i<=$containers;$i++) {
?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-title="Assign Rows" href="<?php echo site_url("pages/add_row/{$client_id}/{$page_id}/ajax"); ?>?container=<?php echo $i; ?>" class="pull-right btn btn-primary btn-xs ajax-modal">Assign Rows</a>
                <button type="submit" class="btn btn-success pull-right btn-xs" style="margin-right:5px;">Save</button>
                <h3 class="panel-title">Container <?php echo $i; ?></h3>
            </div>
            
            <div class="panel-body">

<div class="row">
    <div class="col-md-4">
            <div class="form-group">
                <label>Container Tag</label>
                <input name="container[<?php echo $i; ?>][tag]" type="text" class="form-control" value="<?php echo get_container_value('tag', $i, $page_containers); ?>">
            </div>

            <div class="form-group">
                <label>Container Class</label>
                <input name="container[<?php echo $i; ?>][class]" type="text" class="form-control" value="<?php echo get_container_value('class', $i, $page_containers); ?>">
            </div>

            <div class="form-group">
                <label>Container ID</label>
                <input name="container[<?php echo $i; ?>][id]" type="text" class="form-control" value="<?php echo get_container_value('id', $i, $page_containers); ?>">
            </div>
    </div>
    <div class="col-md-8">
        <?php 
display_rows($rows, $i, $containers);
?>
    </div>
</div>





            </div>
            
        </div>


<?php } ?>
    </div>
    </form>
</div>
</div>
<?php $this->load->view('footer'); ?>

