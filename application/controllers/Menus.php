<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Menus');
		$this->template_data->set('current_uri', 'menus');

		$this->_isAuth('clients', 'clients', 'view');
		$this->_isAuth('clients', 'clients', 'edit');

		$this->check_current_client();

	}

	public function index($id,$output='') {
		
		$menus = new $this->Menus_model;
		$menus->setClientId($id,true);
		$menus->set_select("*");
		$menus->set_select("(SELECT COUNT(*) FROM menus_links WHERE menu_id=menus.id) as links");
		$menu_data = $menus->populate();
		$this->template_data->set('menus', $menu_data);

		$this->load->view('menus/menus_list', $this->template_data->get_data());
	}

	public function add($id, $output='') {

		if( $this->input->post() ) {

			$this->form_validation->set_rules('menu_name', 'Menu Name', 'trim|required');
			
			if( $this->form_validation->run() ) {
				$menu = new $this->Menus_model;
				$menu->setClientId($id);
				$menu->setName($this->input->post('menu_name'));
				$menu->insert();
				redirect('menus/links/' .  $menu->getId());
			}

			$this->postNext();
		}

		$this->template_data->set('output', $output);
		$this->load->view('menus/menus_add', $this->template_data->get_data());
	}

	public function edit($id, $output='') {

		$menu = new $this->Menus_model;
		$menu->setId($id,true);

		if( $this->input->post() ) {

			$this->form_validation->set_rules('menu_name', 'Menu Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$menu->setName($this->input->post('menu_name'),false,true);
				//$menu->setHomeLink( (($this->input->post('home_link')) ? 1 : 0),false,true);
				$menu->update();

				insert_menus_meta($id, "home_link", (($this->input->post('home_link')) ? 1 : 0) );
			}

			$this->postNext();
		}

		$menu_data = $menu->get();

		$this->template_data->set('menu', $menu_data);
		$this->template_data->set('is_home_link', get_menus_meta($id, "home_link"));

		$this->template_data->set('output', $output);
		$this->load->view('menus/menus_edit', $this->template_data->get_data());
	}

	public function links($id, $output='') {

		if( $this->input->post('order') ) {
			foreach($this->input->post('order') as $order=>$link_id) {
				$link = new $this->Menus_links_model;
				$link->setId($link_id,true);
				$link->setOrder($order,false,true);
				$link->update();
			}
		}

		$menus = new $this->Menus_model;
		$menus->setClientId($this->session->userdata('current_client'),true);
		$menus->set_select("*");
		$menus_data = $menus->populate();
		$this->template_data->set('menus', $menus_data);

		$menu = new $this->Menus_model;
		$menu->setId($id,true);
		$menu_data = $menu->get();
		$this->template_data->set('menu', $menu_data);

		$links = new $this->Menus_links_model;
		$links->setMenuId($id,true);
		$links->_set_order('order', 'ASC');
		$links->set_limit(0);
		$this->template_data->set('links', $links->recursive('parent_id', 0));

		$this->template_data->set('output', $output);
		$this->load->view('menus/menus_links', $this->template_data->get_data());
	}

	public function add_link($id, $output='') {

		if( $this->input->post() ) {

			$this->form_validation->set_rules('link_text', 'Link Text', 'trim|required');
			$this->form_validation->set_rules('link_url', 'Link URL', 'trim|required');
			$this->form_validation->set_rules('link_icon', 'Link Icon', 'trim');
			$this->form_validation->set_rules('parent_id', 'Parent Link', 'trim');
			
			if( $this->form_validation->run() ) {
				$link = new $this->Menus_links_model;
				$link->setMenuId($id);
				$link->setText($this->input->post('link_text'));
				$link->setUrl($this->input->post('link_url'));
				$link->setIcon($this->input->post('link_icon'));
				$link->setParentId( ($this->input->post('parent_id')) ? $this->input->post('parent_id') : 0 );
				$link->setOrder($this->input->post('order'));
				$link->setTarget($this->input->post('target'));
				$link->setInnerPage($this->input->post('inner_page'));
				$link->insert();
			}

			$this->postNext();
		}

		$links = new $this->Menus_links_model;
		$links->setMenuId($id,true);
		$links->set_limit(0);
		$links->set_order('order', 'ASC');
		$this->template_data->set('parent_links', $links->recursive('parent_id', 0));

		$menu = new $this->Menus_model;
		$menu->setId($id,true);
		$menu_data = $menu->get();
		$this->template_data->set('menu', $menu_data);

		$pages = new $this->Pages_model('p');
		$pages->setClientId($this->session->userdata('current_client'),true);
		$pages->set_limit(0);
		$pages->set_select('p.*');
		$pages->set_order('p.page_title', 'ASC');
		$this->template_data->set('pages', $pages->recursive('p.parent_page', 0));

		$this->template_data->set('output', $output);
		$this->load->view('menus/menus_add_link', $this->template_data->get_data());
	}

	public function edit_link($id, $output='') {

		$link = new $this->Menus_links_model;
		$link->setId($id,true,false);
		if( $this->input->post() ) {

			$this->form_validation->set_rules('link_text', 'Link Text', 'trim|required');
			$this->form_validation->set_rules('link_url', 'Link URL', 'trim');
			$this->form_validation->set_rules('link_icon', 'Link Icon', 'trim');
			$this->form_validation->set_rules('parent_id', 'Parent Link', 'trim');
			
			if( $this->form_validation->run() ) {
				$link->setText($this->input->post('link_text'),false,true);
				$link->setUrl($this->input->post('link_url'),false,true);
				$link->setIcon($this->input->post('link_icon'),false,true);
				$link->setParentId( (($this->input->post('parent_id')) ? $this->input->post('parent_id') : 0),false,true);
				$link->setOrder($this->input->post('order'),false,true);
				$link->setTarget($this->input->post('target'),false,true);
				$link->setInnerPage($this->input->post('inner_page'),false,true);
				$link->setActive((($this->input->post('active'))?1:0),false,true);
				$link->update();
			}
			$this->postNext();
		}
		$link_data = $link->get();
		$this->template_data->set('link', $link_data);

		$links = new $this->Menus_links_model;
		$links->setMenuId($link_data->menu_id,true);
		$links->set_where('id !=' . $id);
		$links->set_limit(0);
		$links->set_order('order', 'ASC');
		$this->template_data->set('parent_links', $links->recursive('parent_id', 0));

		$menu = new $this->Menus_model;
		$menu->setId($link_data->menu_id,true);
		$menu_data = $menu->get();
		$this->template_data->set('menu', $menu_data);

		$pages = new $this->Pages_model('p');
		$pages->setClientId($this->session->userdata('current_client'),true);
		$pages->set_limit(0);
		$pages->set_select('p.*');
		$pages->set_order('p.page_title', 'ASC');
		$this->template_data->set('pages', $pages->recursive('p.parent_page', 0));
		
		$this->template_data->set('output', $output);
		$this->load->view('menus/menus_edit_link', $this->template_data->get_data());
	}

	public function delete_link($id) {

		$link = new $this->Menus_links_model;
		$link->setId($id,true);
		$link->delete();
		$this->getNext();

	}


}
