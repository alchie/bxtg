<!DOCTYPE html>
<html b:version='2' class='v2' expr:dir='data:blog.languageDirection' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>
  <head>
    <title><?php echo (isset($page)&&($page)) ? $page->page_title : $current_client->name; ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Overpass:300,400,400i,600,700" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/open-iconic-bootstrap.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/animate.css'); ?>" />
    
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/owl.carousel.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/owl.theme.default.min.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/magnific-popup.css'); ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/aos.css'); ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/ionicons.min.css'); ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/bootstrap-datepicker.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/jquery.timepicker.css'); ?>" />

    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/flaticon.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/icomoon.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/welfare/css/style.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/fontawesome-free-5.3.1-web/css/all.min.css'); ?>" />
<style>
 <!--
<?php $this->load->view('templates/theme/welfare/_template5'); ?>
 --> 
</style>
<?php echo get_layout_value('head', 'head', $layouts); ?>
<?php 
$additional_css = trim(get_layout_value('css', 'css', $layouts));
if($additional_css!='') { ?>
<style>
  <!--
<?php echo $additional_css; ?>
  -->
</style>
<?php } ?>
</head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">

<?php if( get_layout_value('header', 'site_url', $layouts) ) { ?>
                  <a class="navbar-brand" href="<?php echo site_url("templates/view/{$current_client->id}/{$template_override}"); ?>">
                      <?php echo get_layout_value('header', 'site_title', $layouts); ?>
                  </a>
<?php } else { ?>
  <?php echo get_layout_value('header', 'site_title', $layouts); ?>
<?php } ?>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
<?php 
$header_nav = get_menu_links( get_layout_value('header', 'header_nav', $layouts) );
foreach( $header_nav as $hnav ) { 
  if( $hnav->active != 1 ) {
    continue;
  }
?>
          <?php if( $hnav->children ) { ?>
          <li class="nav-item"><a href="index.html" class="nav-link">Home</a></li>
          <?php } else { ?>
          <li class="nav-item"><a class="nav-link" href="<?php echo ($hnav->inner_page) ? site_url("templates/view/{$current_client->id}/{$template_override}") . "/" . $hnav->inner_page : $hnav->url; ?>"><?php echo $hnav->text; ?></a></li>
          <?php } ?>
<?php } ?>
        </ul>
      </div>
    </div>
  </nav>
    <!-- END nav -->
    
  
<?php 
function col_class($settings, $col_var, $prefix='', $else='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var > 0)) ? $prefix . $settings->$col_var : $else;
  return ($text!='') ? $text . " " : "";
  } 
function visible_class($settings, $col_var, $prefix='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var != '')) ? $prefix . $settings->$col_var : '';
  return ($text!='') ? $text . " " : "";
  }
?>
<?php function display_rows($rows, $ths) { ?>
<?php foreach( $rows as $row ) { ?>

<?php if($row->blogger_main_includable) { ?>
  <div style="border: 5px solid red; padding: 20px;margin: 20px; text-align: center;">
    <?php echo $row->title; ?> will be inserted here...
  </div>
<?php 
  continue;
} ?>

<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>


  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>
<?php if( $row->display_rows ) { ?>
    <div class="row <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
  <?php } ?>
<?php if(isset($row->columns_data)) { ?>
        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php } ?>
<?php if(isset($row->display_column_settings) && $row->display_column_settings) { ?>
          <div class="<?php 
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-'); 
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/theme/royal/royal_' . $widget->type, array(
                          'widget_options'=> json_decode($widget->options),
                          'widget_id'=>$widget->widget_id,
                        )); 
                ?>
              <?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>

<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>
<?php } ?>
       
<?php if(isset($row->display_column_settings) && $row->display_column_settings) { ?>
</div>
<?php } ?>
       
<?php } ?>
    <?php if( $row->display_rows ) { ?>
          </div>
<?php } ?>
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>
<?php } ?>

<?php if((isset($page)) && ($page)) { ?>

<?php if( $rows ) {  
    display_rows($rows, $this); 
  } ?>

<?php } else { ?>

 <!-- ERROR CONTENT -->
             <div class="hero-wrap" style="background-color:#212529;" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-7 text-center" data-scrollax=" properties: { translateY: '70%' }">
             <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Home</a></span> <span>404 Error</span></p>
            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Page not found!</h1>
          </div>
        </div>
      </div>
    </div>

<?php } ?>

  <footer class="ftco-footer ftco-section img" style="padding: 5px; font-size: 10px;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">

           <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  This template is made with love by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.easing.1.3.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.waypoints.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.stellar.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/owl.carousel.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.magnific-popup.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/aos.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.animateNumber.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/bootstrap-datepicker.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/jquery.timepicker.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/scrollax.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/google-map.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/welfare/js/main.js'); ?>"></script>    

<?php echo get_layout_value('foot', 'foot', $layouts); ?>

<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 115px;
    left: -8px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="blogger_xml" href="<?php echo site_url("templates/generate_blogger/{$current_client->id}/{$template_name}/{$xml_version}"); ?>" class="">XML</a>

</div>

<?php if((isset($page)) && ($page)) { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 190px;
    left: -25px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/" . $page->id ); ?>" class="">Edit Page</a>
<?php } else { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 325px;
    left: -44px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/0" ); ?>" class="">Edit Homepage</a>
<?php } ?>
</div>

  </body>
</html>