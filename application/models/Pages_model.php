<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pages_model Class
 *
 * Manipulates `pages` table on database

CREATE TABLE `pages` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `page_id` varchar(250) NOT NULL,
  `page_title` varchar(250) NOT NULL,
  `page_slug` varchar(250) NOT NULL,
  `order` int(2) DEFAULT 0,
  `active` int(1) NOT NULL DEFAULT 1,
  `parent_page` int(20) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin;

ALTER TABLE  `pages` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `pages` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `pages` ADD  `page_id` varchar(250) NOT NULL   ;
ALTER TABLE  `pages` ADD  `page_title` varchar(250) NOT NULL   ;
ALTER TABLE  `pages` ADD  `page_slug` varchar(250) NOT NULL   ;
ALTER TABLE  `pages` ADD  `order` int(2) NULL   DEFAULT '0';
ALTER TABLE  `pages` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `pages` ADD  `parent_page` int(20) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Pages_model extends MY_Model {

	protected $id;
	protected $client_id;
	protected $page_id;
	protected $page_title;
	protected $page_slug;
	protected $order;
	protected $active;
	protected $parent_page;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'pages';
		$this->_short_name = 'pages';
		$this->_fields = array("id","client_id","page_id","page_title","page_slug","order","active","parent_page");
		$this->_required = array("client_id","page_id","page_title","page_slug","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: page_id -------------------------------------- 

	/** 
	* Sets a value to `page_id` variable
	* @access public
	*/

	public function setPageId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_id` variable
	* @access public
	*/

	public function getPageId() {
		return $this->page_id;
	}

	public function get_page_id() {
		return $this->page_id;
	}

	
// ------------------------------ End Field: page_id --------------------------------------


// ---------------------------- Start Field: page_title -------------------------------------- 

	/** 
	* Sets a value to `page_title` variable
	* @access public
	*/

	public function setPageTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_title($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_title` variable
	* @access public
	*/

	public function getPageTitle() {
		return $this->page_title;
	}

	public function get_page_title() {
		return $this->page_title;
	}

	
// ------------------------------ End Field: page_title --------------------------------------


// ---------------------------- Start Field: page_slug -------------------------------------- 

	/** 
	* Sets a value to `page_slug` variable
	* @access public
	*/

	public function setPageSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_slug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_slug` variable
	* @access public
	*/

	public function getPageSlug() {
		return $this->page_slug;
	}

	public function get_page_slug() {
		return $this->page_slug;
	}

	
// ------------------------------ End Field: page_slug --------------------------------------


// ---------------------------- Start Field: order -------------------------------------- 

	/** 
	* Sets a value to `order` variable
	* @access public
	*/

	public function setOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_order($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `order` variable
	* @access public
	*/

	public function getOrder() {
		return $this->order;
	}

	public function get_order() {
		return $this->order;
	}

	
// ------------------------------ End Field: order --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: parent_page -------------------------------------- 

	/** 
	* Sets a value to `parent_page` variable
	* @access public
	*/

	public function setParentPage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_page', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_parent_page($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_page', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parent_page` variable
	* @access public
	*/

	public function getParentPage() {
		return $this->parent_page;
	}

	public function get_parent_page() {
		return $this->parent_page;
	}

	
// ------------------------------ End Field: parent_page --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'page_id' => (object) array(
										'Field'=>'page_id',
										'Type'=>'varchar(250)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'page_title' => (object) array(
										'Field'=>'page_title',
										'Type'=>'varchar(250)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'page_slug' => (object) array(
										'Field'=>'page_slug',
										'Type'=>'varchar(250)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'order' => (object) array(
										'Field'=>'order',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'parent_page' => (object) array(
										'Field'=>'parent_page',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `pages` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'client_id' => "ALTER TABLE  `pages` ADD  `client_id` int(20) NOT NULL   ;",
			'page_id' => "ALTER TABLE  `pages` ADD  `page_id` varchar(250) NOT NULL   ;",
			'page_title' => "ALTER TABLE  `pages` ADD  `page_title` varchar(250) NOT NULL   ;",
			'page_slug' => "ALTER TABLE  `pages` ADD  `page_slug` varchar(250) NOT NULL   ;",
			'order' => "ALTER TABLE  `pages` ADD  `order` int(2) NULL   DEFAULT '0';",
			'active' => "ALTER TABLE  `pages` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'parent_page' => "ALTER TABLE  `pages` ADD  `parent_page` int(20) NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setClientId() - client_id
//setPageId() - page_id
//setPageTitle() - page_title
//setPageSlug() - page_slug
//setOrder() - order
//setActive() - active
//setParentPage() - parent_page

--------------------------------------

//set_id() - id
//set_client_id() - client_id
//set_page_id() - page_id
//set_page_title() - page_title
//set_page_slug() - page_slug
//set_order() - order
//set_active() - active
//set_parent_page() - parent_page

*/
/* End of file Pages_model.php */
/* Location: ./application/models/Pages_model.php */
