  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingPages">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          Pages
        </a>
      </h4>
    </div>
    <div id="collapsePages" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPages">
      <div class="panel-body">
        
<div class="row">
        <div class="col-md-6">
        <div class="form-group">
          <label>Page Margin</label>
          <select class="form-control" name="layout[page][width]">
              <?php foreach(array(
                'wide' => 'Wide',
                'boxed' => 'Boxed',
                ) as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>" <?php echo (get_layout_value('page','width', $layouts)==$key) ? "SELECTED" : "";?>><?php echo $value; ?></option>
              <?php } ?>
          </select>
        </div>
        </div>

        <div class="col-md-6">
        <div class="form-group">
          <label>Default Template</label>
          <select class="form-control" name="layout[page][template]">
              <?php foreach($templates as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>" <?php echo (get_layout_value('page','template', $layouts)==$key) ? "SELECTED" : "";?>><?php echo $value['name']; ?></option>
              <?php } ?>
          </select>
        </div>
        </div>

</div>


      </div>
    </div>
  </div>