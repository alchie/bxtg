<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Page</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

                <div class="form-group">
                    <label>Page Title</label>
                    <input name="page_title" type="text" class="form-control" value="<?php echo $this->input->post('page_title'); ?>">
                </div>
                <div class="form-group">
                    <label>Parent Page</label>
                    <select name="parent_page" type="text" class="form-control">
                        <option value="0">-- Parent Page --</option>
                        <?php foreach($parent_pages as $ppage) { ?>
                            <option value="<?php echo $ppage->id; ?>" <?php echo ($ppage->id==$parent_id)?"SELECTED":""; ?>><?php echo $ppage->page_title; ?></option>
                        <?php } ?>
                    </select>
                </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("layout/index/{$current_client->id}"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>