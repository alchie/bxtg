  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingFoot">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFoot" aria-expanded="true" aria-controls="collapseFoot">
          Custom Foot Tags
        </a>
      </h4>
    </div>
    <div id="collapseFoot" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFoot">
      <div class="panel-body">

        <div class="form-group">
          <textarea class="form-control" name="layout[foot][foot]" rows="10"><?php echo htmlentities(get_layout_value('foot','foot', $layouts)); ?></textarea>
        </div>
    
      </div>
    </div>
  </div>