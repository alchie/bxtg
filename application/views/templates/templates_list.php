<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">

<?php 

foreach($templates as $key=>$template) { ?>
                  <div class="list-group-item">

                  <div class="btn-group pull-right">
                      <a class="btn btn-warning btn-xs" href="<?php echo site_url("templates/view/{$current_client->id}/{$key}"); ?>" target="_blank">Demo</a>
                      <a class="btn btn-success btn-xs" href="<?php echo site_url("templates/generate_blogger/{$current_client->id}/{$key}/{$template['version']}"); ?>" target="_blank">Generate Blogger XML</a>
                      <a class="btn btn-primary btn-xs" href="<?php echo site_url("templates/view/{$current_client->id}/{$key}"); ?>" target="_blank">Download HTML</a>
                  </div>

                  <span class="dashicons dashicons-layout"></span>
                  <?php echo $template['name']; ?>
                  </div>
<?php } ?>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('footer'); ?>