<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo (isset($page)&&($page)) ? $page->page_title : $current_client->name; ?></title>

<?php 
$additional_css = trim(get_layout_value('css', 'css', $layouts));
if($additional_css!='') { ?>
<style>
  <!--
<?php echo $additional_css; ?>
  -->
</style>
<?php } ?>
<?php echo get_layout_value('head', 'head', $layouts); ?>

</head>
<body>

<?php 
function col_class($settings, $col_var, $prefix='', $else='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var > 0)) ? $prefix . $settings->$col_var : $else;
  return ($text!='') ? $text . " " : "";
  } 
function visible_class($settings, $col_var, $prefix='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var != '')) ? $prefix . $settings->$col_var : '';
  return ($text!='') ? $text . " " : "";
  }
function display_rows($rows, $ths, $container_n=1) { ?>
<?php foreach( $rows as $row ) { 
  if( $row->page_containers != $container_n) {
    continue;
  }
  ?>

<?php if($row->blogger_main_includable) { ?>
  <div style="border: 5px solid red; padding: 20px;margin: 20px; text-align: center;">
    <?php echo $row->title; ?> will be inserted here...
  </div>
<?php 
  continue;
} ?>

<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>
<?php if( $row->display_rows ) { ?>

  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>

   

<?php if(isset($row->columns_data)) { ?>

<?php if(isset($row->generate_row_container)) { ?>
 <div class="row <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
<?php } ?>

        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>

<?php if( isset($more_settings->generate) ) { ?>

<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php } ?>
          <div class="<?php 
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-'); 
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>

<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/_default_widgets/' . $widget->type, array(
                          'widget_id'=>$widget->widget_id,
                          'widget_options'=> json_decode($widget->options)
                       )); 
                ?>
              <?php } ?>

<?php if( isset($more_settings->generate) ) { ?>

<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>
          </div>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>>  
<?php } ?>

<?php } ?>

        <?php } ?>
        
<?php if(isset($row->generate_row_container)) { ?>
   </div>
<?php } ?>

<?php } ?>
   
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths, $containers_n);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>
<?php } ?>

<?php if((isset($page)) && ($page)) { ?>

<?php if( get_option_value('page_container_tag', $options) ) { 
    echo '<' . get_option_value('page_container_tag', $options) . ' class="' . get_option_value('page_container_class', $options) . '" id="' . get_option_value('page_container_id', $options) . '">';
} 
?>

<?php 
if( $rows ) { 
$containers_n = ( get_option_value('rows_container', $options) ) ? get_option_value('rows_container', $options) : 1;

  for($i=1;$i<=$containers_n; $i++) {
    ?>
<?php if( get_container_value('tag', $i, $page_containers) ) { 
    echo '<' . get_container_value('tag', $i, $page_containers) . ' class="' . get_container_value('class', $i, $page_containers) . '" id="' . get_container_value('id', $i, $page_containers) . '">';
} 
?>
    <?php
      display_rows($rows, $this, $i); 
    ?>
<?php if( get_container_value('tag', $i, $page_containers) ) { 
    echo '</' . get_container_value('tag', $i, $page_containers) . '>';
} ?>
<?php
  }
}
?>

<?php if( get_option_value('page_container_tag', $options) ) { 
    echo '</' . get_option_value('page_container_tag', $options) . '>';
} ?>

<?php } else { ?>
<?php } ?>

<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 115px;
    left: -10px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="blogger_xml" href="<?php echo site_url("templates/generate_blogger/{$current_client->id}/{$template_name}/{$xml_version}"); ?>" class="">XML</a>

</div>

<?php if((isset($page)) && ($page)) { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 190px;
    left: -25px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/" . $page->id ); ?>" class="">Edit Page</a>
<?php } else { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 325px;
    left: -44px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/0" ); ?>" class="">Edit Homepage</a>
<?php } ?>
</div>
<?php echo get_layout_value('foot', 'foot', $layouts); ?>
  </body>
</html>