  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingFooter">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFooter" aria-expanded="true" aria-controls="collapseFooter">
          Footer
        </a>
      </h4>
    </div>
    <div id="collapseFooter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooter">
      <div class="panel-body">


<div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>Copyright Name</label>
          <input type="text" class="form-control" placeholder="Copyright" name="layout[footer][copyright_name]" value="<?php echo get_layout_value('footer','copyright_name', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Copyright Year</label>
          <input type="text" class="form-control" placeholder="Copyright" name="layout[footer][copyright_year]" value="<?php echo get_layout_value('footer','copyright_year', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Footer Navigation</label>
          <select class="form-control" name="layout[footer][footer_nav]">
              <option value="">No Navigation</option>
              <?php foreach($menus as $menu) { ?>
                  <option value="<?php echo $menu->id; ?>" <?php echo (get_layout_value('footer','footer_nav', $layouts)==$menu->id) ? "SELECTED" : "";?>><?php echo $menu->name; ?> (<?php echo $menu->total_links; ?>)</option>
              <?php } ?>
          </select>
        </div>
    </div>
</div>


      </div>
    </div>
  </div>