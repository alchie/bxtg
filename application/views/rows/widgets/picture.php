<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$items = false;
$i=0;
if( isset($widget) && ($widget) ) {
    $options = json_decode( $widget->options );
    $items = $options->items;
    foreach($items as $item1) {
        $i++;
    }
}
?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<script>
    <!--
        var image_slider_count = <?php echo $i; ?>;
    -->
</script>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">Picture

<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>

                </h3>
            </div>
          
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">

    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget Name</label>
                    <input placeholder="Widget Name" name="title" type="text" class="form-control" value="<?php echo isset($options->title) ? $options->title : ''; ?>">
                </div>
    </div>

    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget ID</label>
                    <input placeholder="Widget ID" name="widget_id" type="text" class="form-control" value="<?php echo isset($options->widget_id) ? $options->widget_id : ''; ?>">
                </div>
    </div>

    <div class="col-md-4">
                <div class="form-group">
                    <label>List Class</label>
                    <input placeholder="List Class" name="list_class" type="text" class="form-control" value="<?php echo isset($options->list_class) ? $options->list_class : ''; ?>">
                </div>
    </div>

</div>

<div class="row">

    <div class="col-md-3">
                <div class="form-group">
                    <label>
                    <input name="add_container" type="checkbox" value="1" <?php echo isset($options->add_container) ? 'CHECKED' : ''; ?>> Add Container</label>
<br>
                    <label>
                    <input name="add_cols" type="checkbox" value="1" <?php echo isset($options->add_cols) ? 'CHECKED' : ''; ?>> Add Columns</label>
                </div>
    </div>


    <div class="col-md-2">
                <div class="form-group">
                    <label>Col-lg-*</label>
                    <select class="form-control" name="col_width_lg">
                      <?php for($i=12; $i >= 1; $i--) { ?>
                        <option <?php echo (isset($options->col_width_lg) && ($options->col_width_lg==$i)) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                      <?php } ?>
                    </select>
                </div>
    </div>
    <div class="col-md-2">
                <div class="form-group">
                    <label>Col-md-*</label>
                    <select class="form-control" name="col_width_md">
                      <?php for($i=12; $i >= 1; $i--) { ?>
                        <option <?php echo (isset($options->col_width_md) && ($options->col_width_md==$i)) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                      <?php } ?>
                    </select>
                </div>
    </div>
    <div class="col-md-2">
                <div class="form-group">
                    <label>Col-sm-*</label>
                    <select class="form-control" name="col_width_sm">
                      <?php for($i=12; $i >= 1; $i--) { ?>
                        <option <?php echo (isset($options->col_width_sm) && ($options->col_width_sm==$i)) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                      <?php } ?>
                    </select>
                </div>
    </div>
    <div class="col-md-2">
                <div class="form-group">
                    <label>Col-xs-*</label>
                    <select class="form-control" name="col_width_xs">
                      <?php for($i=12; $i >= 1; $i--) { ?>
                        <option <?php echo (isset($options->col_width_xs) && ($options->col_width_xs==$i)) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                      <?php } ?>
                    </select>
                </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
            <button type="button" class="btn btn-warning btn-xs pull-right" id="widget_add_image_slider_button" data-action="add">Add</button>
    List Items</div>
    
  <div class="panel-body">

<div class="list-group sortable" id="image_slider_group">
<?php 
$key = 0;
if( $items ) foreach( $items as $item ) { ?>
    <div class="list-group-item" id="image_slider_item<?php echo $key; ?>">
     <input type="hidden" id="image_url<?php echo $key; ?>" name="items[<?php echo $key; ?>][image_url]" value="<?php echo (isset($item->image_url)) ? $item->image_url : ''; ?>">
     <input type="hidden" id="image_title<?php echo $key; ?>" name="items[<?php echo $key; ?>][image_title]" value="<?php echo (isset($item->image_title)) ? $item->image_title : ''; ?>">
     <input type="hidden" id="image_desc<?php echo $key; ?>" name="items[<?php echo $key; ?>][image_desc]" value="<?php echo (isset($item->image_desc)) ? $item->image_desc : ''; ?>">
     <input type="hidden" id="image_link<?php echo $key; ?>" name="items[<?php echo $key; ?>][image_link]" value="<?php echo (isset($item->image_link)) ? $item->image_link : ''; ?>">
     <input type="hidden" id="show_title<?php echo $key; ?>" name="items[<?php echo $key; ?>][show_title]" value="<?php echo (isset($item->show_title)) ? $item->show_title : ''; ?>">
     <input type="hidden" id="show_desc<?php echo $key; ?>" name="items[<?php echo $key; ?>][show_desc]" value="<?php echo (isset($item->show_desc)) ? $item->show_desc : ''; ?>">
     <input type="hidden" id="show_caption<?php echo $key; ?>" name="items[<?php echo $key; ?>][show_caption]" value="<?php echo (isset($item->show_caption)) ? $item->show_caption : ''; ?>">
     <input type="hidden" id="image_page<?php echo $key; ?>" name="items[<?php echo $key; ?>][image_page]" value="<?php echo (isset($item->image_page)) ? $item->image_page : 0; ?>">
     <input type="hidden" id="container_class<?php echo $key; ?>" name="items[<?php echo $key; ?>][container_class]" value="<?php echo (isset($item->container_class)) ? $item->container_class : ''; ?>">
    <span id="image_slider_item_title<?php echo $key; ?>"><?php echo $item->image_title; ?></span>
     
 <a href="javascript:void(0);" class="image_slider_item_duplicate btn btn-warning btn-xs pull-right" style="margin-left:5px;" data-id="<?php echo $key; ?>">Duplicate
    </a>

     <a href="javascript:void(0);" class="image_slider_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;" data-id="<?php echo $key; ?>">Delete
    </a>

    <a href="javascript:void(0);" class="image_slider_item_edit btn btn-success btn-xs pull-right" data-id="<?php echo $key; ?>">Edit
    </a>

    </div>
<?php $key++; } ?>
</div>


<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="imageSliderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Picture</h4>
      </div>
      <div class="modal-body">
        
    <div class="form-group">
        <label for="image_url">Image URL</label>
        <input type="text" class="form-control" placeholder="Image URL" id="image_url">
    </div>

    <div class="form-group">
        <label for="image_title">Image Title</label>
        <input type="text" class="form-control" placeholder="Image Title" id="image_title">
    </div>

    <div class="form-group">
        <label for="image_desc">Image Description</label>
        <textarea class="form-control" placeholder="Image Description" id="image_desc"></textarea>
    </div>

    <div class="form-group">
        <label for="image_link">Image Link</label>
        <input type="text" class="form-control" placeholder="Image Link" id="image_link">
    </div>

    <div class="form-group">
        <label for="image_page">Inner Page</label>
        <select name="image_page" class="form-control" id="image_page">
            <option value="0">No Inner Page</option>
            <?php 
            foreach($hpages as $page) { ?>
                <option id="image_page_item_<?php echo $page->id; ?>" value="<?php echo $page->id; ?>"><?php echo $page->page_title; ?> (<?php echo $page->page_slug; ?>)</option>
                <?php foreach($page->children as $cpage) { ?>
                  <option id="image_page_item_<?php echo $cpage->id; ?>" value="<?php echo $cpage->id; ?>">- - - <?php echo $cpage->page_title; ?> (<?php echo $cpage->page_slug; ?>)</option>
                <?php } ?>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label for="show_caption"><input type="checkbox" id="show_caption" value="1"> Show Caption</label><br>
        <label for="show_title"><input type="checkbox" id="show_title" value="1"> Show Title</label><br>
        <label for="show_desc"><input type="checkbox" id="show_desc" value="1"> Show Description</label>
    </div>
    

<div class="row">
  <div class="col-md-4">
        <div class="form-group">
          <label for="container_class">Container Class</label>
          <input type="text" class="form-control" placeholder="Container Class" id="container_class">
        </div>
  </div>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="widget_image_slider_submit">Submit</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>