<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rows_model Class
 *
 * Manipulates `rows` table on database

CREATE TABLE `rows` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `columns` int(2) NOT NULL DEFAULT 12,
  `row_class` varchar(200) DEFAULT NULL,
  `container` int(1) DEFAULT 0,
  `container_class` varchar(200) DEFAULT NULL,
  `wrapper_class` varchar(200) DEFAULT NULL,
  `display_rows` int(1) DEFAULT 1,
  `parent_row` int(20) DEFAULT 0,
  `more_settings` text DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `group_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_row` (`parent_row`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

ALTER TABLE  `rows` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `rows` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `rows` ADD  `title` varchar(200) NULL   ;
ALTER TABLE  `rows` ADD  `columns` int(2) NOT NULL   DEFAULT '12';
ALTER TABLE  `rows` ADD  `row_class` varchar(200) NULL   ;
ALTER TABLE  `rows` ADD  `container` int(1) NULL   DEFAULT '0';
ALTER TABLE  `rows` ADD  `container_class` varchar(200) NULL   ;
ALTER TABLE  `rows` ADD  `wrapper_class` varchar(200) NULL   ;
ALTER TABLE  `rows` ADD  `display_rows` int(1) NULL   DEFAULT '1';
ALTER TABLE  `rows` ADD  `parent_row` int(20) NULL   DEFAULT '0';
ALTER TABLE  `rows` ADD  `more_settings` text NULL   ;
ALTER TABLE  `rows` ADD  `active` int(1) NULL   DEFAULT '1';
ALTER TABLE  `rows` ADD  `group_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Rows_model extends MY_Model {

	protected $id;
	protected $client_id;
	protected $title;
	protected $columns;
	protected $row_class;
	protected $container;
	protected $container_class;
	protected $wrapper_class;
	protected $display_rows;
	protected $parent_row;
	protected $more_settings;
	protected $active;
	protected $group_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'rows';
		$this->_short_name = 'rows';
		$this->_fields = array("id","client_id","title","columns","row_class","container","container_class","wrapper_class","display_rows","parent_row","more_settings","active","group_id");
		$this->_required = array("client_id","columns");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: title -------------------------------------- 

	/** 
	* Sets a value to `title` variable
	* @access public
	*/

	public function setTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_title($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `title` variable
	* @access public
	*/

	public function getTitle() {
		return $this->title;
	}

	public function get_title() {
		return $this->title;
	}

	
// ------------------------------ End Field: title --------------------------------------


// ---------------------------- Start Field: columns -------------------------------------- 

	/** 
	* Sets a value to `columns` variable
	* @access public
	*/

	public function setColumns($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('columns', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_columns($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('columns', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `columns` variable
	* @access public
	*/

	public function getColumns() {
		return $this->columns;
	}

	public function get_columns() {
		return $this->columns;
	}

	
// ------------------------------ End Field: columns --------------------------------------


// ---------------------------- Start Field: row_class -------------------------------------- 

	/** 
	* Sets a value to `row_class` variable
	* @access public
	*/

	public function setRowClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_row_class($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `row_class` variable
	* @access public
	*/

	public function getRowClass() {
		return $this->row_class;
	}

	public function get_row_class() {
		return $this->row_class;
	}

	
// ------------------------------ End Field: row_class --------------------------------------


// ---------------------------- Start Field: container -------------------------------------- 

	/** 
	* Sets a value to `container` variable
	* @access public
	*/

	public function setContainer($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_container($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `container` variable
	* @access public
	*/

	public function getContainer() {
		return $this->container;
	}

	public function get_container() {
		return $this->container;
	}

	
// ------------------------------ End Field: container --------------------------------------


// ---------------------------- Start Field: container_class -------------------------------------- 

	/** 
	* Sets a value to `container_class` variable
	* @access public
	*/

	public function setContainerClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_container_class($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `container_class` variable
	* @access public
	*/

	public function getContainerClass() {
		return $this->container_class;
	}

	public function get_container_class() {
		return $this->container_class;
	}

	
// ------------------------------ End Field: container_class --------------------------------------


// ---------------------------- Start Field: wrapper_class -------------------------------------- 

	/** 
	* Sets a value to `wrapper_class` variable
	* @access public
	*/

	public function setWrapperClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('wrapper_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_wrapper_class($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('wrapper_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `wrapper_class` variable
	* @access public
	*/

	public function getWrapperClass() {
		return $this->wrapper_class;
	}

	public function get_wrapper_class() {
		return $this->wrapper_class;
	}

	
// ------------------------------ End Field: wrapper_class --------------------------------------


// ---------------------------- Start Field: display_rows -------------------------------------- 

	/** 
	* Sets a value to `display_rows` variable
	* @access public
	*/

	public function setDisplayRows($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('display_rows', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_display_rows($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('display_rows', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `display_rows` variable
	* @access public
	*/

	public function getDisplayRows() {
		return $this->display_rows;
	}

	public function get_display_rows() {
		return $this->display_rows;
	}

	
// ------------------------------ End Field: display_rows --------------------------------------


// ---------------------------- Start Field: parent_row -------------------------------------- 

	/** 
	* Sets a value to `parent_row` variable
	* @access public
	*/

	public function setParentRow($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_row', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_parent_row($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_row', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parent_row` variable
	* @access public
	*/

	public function getParentRow() {
		return $this->parent_row;
	}

	public function get_parent_row() {
		return $this->parent_row;
	}

	
// ------------------------------ End Field: parent_row --------------------------------------


// ---------------------------- Start Field: more_settings -------------------------------------- 

	/** 
	* Sets a value to `more_settings` variable
	* @access public
	*/

	public function setMoreSettings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('more_settings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_more_settings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('more_settings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `more_settings` variable
	* @access public
	*/

	public function getMoreSettings() {
		return $this->more_settings;
	}

	public function get_more_settings() {
		return $this->more_settings;
	}

	
// ------------------------------ End Field: more_settings --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: group_id -------------------------------------- 

	/** 
	* Sets a value to `group_id` variable
	* @access public
	*/

	public function setGroupId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_group_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_id` variable
	* @access public
	*/

	public function getGroupId() {
		return $this->group_id;
	}

	public function get_group_id() {
		return $this->group_id;
	}

	
// ------------------------------ End Field: group_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'title' => (object) array(
										'Field'=>'title',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'columns' => (object) array(
										'Field'=>'columns',
										'Type'=>'int(2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'12',
										'Extra'=>''
									),

			'row_class' => (object) array(
										'Field'=>'row_class',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'container' => (object) array(
										'Field'=>'container',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'container_class' => (object) array(
										'Field'=>'container_class',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'wrapper_class' => (object) array(
										'Field'=>'wrapper_class',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'display_rows' => (object) array(
										'Field'=>'display_rows',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'parent_row' => (object) array(
										'Field'=>'parent_row',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'more_settings' => (object) array(
										'Field'=>'more_settings',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'group_id' => (object) array(
										'Field'=>'group_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `rows` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'client_id' => "ALTER TABLE  `rows` ADD  `client_id` int(20) NOT NULL   ;",
			'title' => "ALTER TABLE  `rows` ADD  `title` varchar(200) NULL   ;",
			'columns' => "ALTER TABLE  `rows` ADD  `columns` int(2) NOT NULL   DEFAULT '12';",
			'row_class' => "ALTER TABLE  `rows` ADD  `row_class` varchar(200) NULL   ;",
			'container' => "ALTER TABLE  `rows` ADD  `container` int(1) NULL   DEFAULT '0';",
			'container_class' => "ALTER TABLE  `rows` ADD  `container_class` varchar(200) NULL   ;",
			'wrapper_class' => "ALTER TABLE  `rows` ADD  `wrapper_class` varchar(200) NULL   ;",
			'display_rows' => "ALTER TABLE  `rows` ADD  `display_rows` int(1) NULL   DEFAULT '1';",
			'parent_row' => "ALTER TABLE  `rows` ADD  `parent_row` int(20) NULL   DEFAULT '0';",
			'more_settings' => "ALTER TABLE  `rows` ADD  `more_settings` text NULL   ;",
			'active' => "ALTER TABLE  `rows` ADD  `active` int(1) NULL   DEFAULT '1';",
			'group_id' => "ALTER TABLE  `rows` ADD  `group_id` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setClientId() - client_id
//setTitle() - title
//setColumns() - columns
//setRowClass() - row_class
//setContainer() - container
//setContainerClass() - container_class
//setWrapperClass() - wrapper_class
//setDisplayRows() - display_rows
//setParentRow() - parent_row
//setMoreSettings() - more_settings
//setActive() - active
//setGroupId() - group_id

--------------------------------------

//set_id() - id
//set_client_id() - client_id
//set_title() - title
//set_columns() - columns
//set_row_class() - row_class
//set_container() - container
//set_container_class() - container_class
//set_wrapper_class() - wrapper_class
//set_display_rows() - display_rows
//set_parent_row() - parent_row
//set_more_settings() - more_settings
//set_active() - active
//set_group_id() - group_id

*/
/* End of file Rows_model.php */
/* Location: ./application/models/Rows_model.php */
