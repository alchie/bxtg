<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends MY_Controller {
	
	public function __construct() 
	{
		parent::__construct();

		$this->template_data->set('current_page', 'Images');
		$this->template_data->set('current_uri', 'images');

		$this->_isAuth('clients', 'clients', 'view');
		$this->_isAuth('clients', 'clients', 'edit');

		$this->check_current_client();

	}

	public function index()
	{

		$clients = new $this->Clients_model;
		$clients->setId($this->session->userdata('current_client'),true);
		$this->template_data->set('current_client', $clients->get());

		$images = new $this->Images_model;
        $images->setClientId( $this->session->userdata('current_client') );
        $this->template_data->set('images', $images->populate());

		$this->load->view('images/images_list', $this->template_data->get_data());

	}

	public function upload($output='')
	{

		$clients = new $this->Clients_model;
		$clients->setId($this->session->userdata('current_client'),true);
		$this->template_data->set('current_client', $clients->get());

		$this->template_data->set('output', $output);
		$this->load->view('images/images_upload', $this->template_data->get_data());

	}

	public function do_upload() {
		
		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 2880;
        $config['max_height']           = 1800;

        $this->load->library('upload', $config);

        if ( $this->upload->do_upload('userfile') )
        {
                $data = $this->upload->data();
                $images = new $this->Images_model;
                $images->setClientId( $this->session->userdata('current_client') );
                $images->setFileName($data['file_name']);
                $images->setFileType($data['file_type']);
                $images->setFileExt($data['file_ext']);
                $images->setFileSize($data['file_size']);
                $images->setImageWidth($data['image_width']);
                $images->setImageHeight($data['image_height']);
                $images->setImageType($data['image_type']);
                $images->insert();
        		redirect( site_url("images") . "?success=Upload Successfully" );
        } else {
        	redirect( site_url("images") . "?error=1" );
        }
        

	}

	public function delete_image($id) {

		$this->load->helper('file');
		$image = new $this->Images_model;
        $image->setClientId( $this->session->userdata('current_client'),true );
        $image->setId($id,true);
        $image_data = $image->get();
        if( $image->delete() ) {
        	delete_files("./uploads/{$image_data->file_name}");
        }
        redirect($this->input->get('back'));
	}

	public function edit_image($id, $output="") {

		$clients = new $this->Clients_model;
		$clients->setId($this->session->userdata('current_client'),true);
		$this->template_data->set('current_client', $clients->get());

		$image = new $this->Images_model;
        $image->setClientId( $this->session->userdata('current_client'),true );
        $image->setId($id,true);
        if( $this->input->post() ) {
        	$image->setTitle($this->input->post('title'),false,true);
        	$image->setUploadUrl($this->input->post('upload_url'),false,true);
        	$image->update();
        }
        $this->template_data->set('current_image', $image->get());

		$this->template_data->set('output', $output);
		$this->load->view('images/images_edit', $this->template_data->get_data());

	}

}
