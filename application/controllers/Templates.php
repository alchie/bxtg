<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Templates');
		$this->template_data->set('current_uri', 'templates');

		$this->_isAuth('clients', 'clients', 'view');

		$this->check_current_client();

	}

	public function index($id='blank_template',$output='') {
		
		$this->_isAuth('clients', 'clients', 'edit');

		$this->load->view('templates/templates_list', $this->template_data->get_data());
	}

	public function view($client_id, $override=0, $page_id=0, $page_type='homepage') {
		
		$template_name = new $this->Layouts_model;
		$template_name->setClientId($this->session->userdata('current_client'),true);
		$template_name->setLocation('page',true);
		$template_name->setLayoutKey('template',true);
		$template_name_data = $template_name->get();
		$template_value = ($template_name_data) ? $template_name_data->layout_value : 'royal';

		$template_name = ($override) ? $override : $template_value;

		$this->_isAuth('clients', 'clients', 'edit');

		$this->template_data->set('client_id', $this->session->userdata('current_client'));
		$this->template_data->set('template_name', $template_name);
		$this->template_data->set('template_override', $override);
		
		$xml_version = 1;
		$templates = $this->config->item('bxtg_templates');
		if( isset($templates[$template_name]) ) {
			$current_template = $templates[$template_name];
			$xml_version = $current_template['version'];
		}

		$this->template_data->set('xml_version', $xml_version);

		$layouts = new $this->Layouts_model;
		$layouts->setClientId($this->session->userdata('current_client'),true);
		$layouts->set_limit(0);
		$layouts_data = $layouts->populate();
		$this->template_data->set('layouts', $layouts_data);

		$this->template_data->set('homepage_options', $this->_get_page_options($this->session->userdata('current_client'),0));

		$this->template_data->set('options', $this->_get_page_options($this->session->userdata('current_client'),$page_id));

		$page_found = true;
		$is_homepage = false;
		if( intval($page_id) > 0 )  {
			$page = new $this->Pages_model;
			$page->setId($page_id,true);
			if( $page->nonEmpty()) {
				$this->template_data->set('page', $page->get());
			} else {
				$page_found = false;
			}
		} else {
			switch($page_type) {
				case 'homepage':
					$page = new $this->Pages_model;
					$page->setId(get_layout_value('common_pages','homepage', $layouts_data),true);
					if( $page->nonEmpty()) {
						$page_id = get_layout_value('common_pages','homepage', $layouts_data);
						$this->template_data->set('page', $page->get());
					}
				break;
				case 'single_page':
					$page = new $this->Pages_model;
					$page->setId(get_layout_value('common_pages','single_page', $layouts_data),true);
					if( $page->nonEmpty()) {
						$page_id = get_layout_value('common_pages','single_page', $layouts_data);
						$this->template_data->set('page', $page->get());
					}
				break;
				default:
					$page = new $this->Pages_model;
					$page->setId(get_layout_value('common_pages','error_404', $layouts_data),true);
					if( $page->nonEmpty()) {
						$page_id = get_layout_value('common_pages','error_404', $layouts_data);
						$this->template_data->set('page', $page->get());
					}
				break;
			}
		}

		if( $page->nonEmpty() ) {
				$current_page = $page->get_results();
				$this->template_data->set('options', $this->_get_page_options($this->session->userdata('current_client'),$current_page->id));

				$page_containers = new $this->Pages_containers_model;
				$page_containers->setClientId($this->session->userdata('current_client'), true);
				$page_containers->setId($current_page->id, true);
				$this->template_data->set('page_containers', $page_containers->populate());

		}

		$rows = new $this->Pages_rows_model('pr');
		$rows->set_join('rows r', 'r.id=pr.row_id');
		$rows->setClientId($this->session->userdata('current_client'),true);
		$rows->setPageId($page_id,true);
		$rows->set_order('order', 'ASC');
		$rows->set_where("r.active=1");
		$rows->set_select("*");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='display_column_settings' LIMIT 1) as display_column_settings");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='blogger_main_includable' LIMIT 1) as blogger_main_includable");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='blogger_post_name' LIMIT 1) as blogger_post_name");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='blogger_post_row_class' LIMIT 1) as blogger_post_row_class");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='generate_row_container' LIMIT 1) as generate_row_container");

		$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='remove_row_class' LIMIT 1) as remove_row_class");

		$rows_data = $this->_assign_row_widgets( $rows->recursive('r.parent_row', 0) );
		$this->template_data->set('rows', $rows_data);

		$this->template_data->set('page_found', $page_found);
		$this->template_data->set('is_homepage', $is_homepage);
		$this->template_data->set('template_output', 'demo');

		$this->load->view("templates/theme/{$template_name}/{$template_name}", $this->template_data->get_data());
	}

	private function _get_widgets($row_id, $column) {
		$widgets = new $this->Rows_columns_widgets_model;
		$widgets->setRowId($row_id,true);
		$widgets->setColumn($column,true);
		$widgets->set_order('order', 'ASC');
		$widgets->set_limit(0);
		return $widgets->populate();
	}

	private function _get_column_settings($row_id, $column) {
		$column_settings = new $this->Rows_columns_model;
		$column_settings->setRowId($row_id,true);
		$column_settings->setColumn($column,true);
		$column_settings->set_order('column_order', 'ASC');
		$column_settings->set_order('column', 'ASC');
		return $column_settings->get();
	}

	private function _assign_row_widgets($rows_data) {
		foreach($rows_data as $row_i=>$row_d) {
			$columns = array();

			$column_settings = new $this->Rows_columns_model;
			$column_settings->setRowId($row_d->id,true);
			$column_settings->set_limit( $row_d->columns );
			$column_settings->set_order('column_order', 'ASC');
			$column_settings->set_order('column', 'ASC');
			foreach($column_settings->populate() as $col) {
				$columns[$col->column]['widgets'] = $this->_get_widgets($row_d->id, $col->column);
				$columns[$col->column]['settings'] = $col;
			}
			$rows_data[$row_i]->columns_data = $columns;
			if( $row_d->children ) {
				$this->_assign_row_widgets( $row_d->children );
			}
		}
		return $rows_data;
	}

	private function _get_page_options($client_id, $page_id) {
		$options = new $this->Pages_options_model;
		$options->setClientId($this->session->userdata('current_client'),true);
		$options->setId($page_id,true);
		$options->set_limit(0);
		return $options->populate();
	}

	public function generate_blogger($id, $name, $version=1) {
		
		$this->_isAuth('clients', 'clients', 'edit');

		$this->template_data->set('template_name', $name);

		$layouts = new $this->Layouts_model;
		$layouts->setClientId($this->session->userdata('current_client'),true);
		$layouts->set_limit(0);
		$layouts_data = $layouts->populate();
		$this->template_data->set('layouts', $layouts_data);

		$pages = new $this->Pages_model;
		$pages->setClientId($this->session->userdata('current_client'),true);
		$pages->set_limit(0);
		$pages_data = $pages->populate();
		
		$new_page_data = array();

		$homepage_page = new $this->Pages_model;
		$homepage_page->setClientId($this->session->userdata('current_client'),true);
		$homepage_page->setId(get_layout_value('common_pages','homepage', $layouts_data),true);
		$homepage_data = $homepage_page->get();
		$this->template_data->set('homepage_page', $homepage_data);

		$new_page_data['homepage']['page'] = $homepage_data;
		$new_page_data['homepage']['options'] = $this->_get_page_options($this->session->userdata('current_client'), get_layout_value('common_pages','homepage', $layouts_data));

		$homepage_rows = new $this->Pages_rows_model('pr');
		$homepage_rows->set_join('rows r', 'r.id=pr.row_id');
		$homepage_rows->setClientId($this->session->userdata('current_client'),true);
		$homepage_rows->setPageId(get_layout_value('common_pages','homepage', $layouts_data),true);
		$homepage_rows->setActive(1,true);
		$homepage_rows->set_limit(0);
		$homepage_rows->set_order('order', 'ASC');
		$homepage_rows->set_select("*");

		$homepage_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

		foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
			$homepage_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

		foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
			$homepage_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}
		

// single_page
$single_page_data = array();
if( get_layout_value('common_pages','single_page', $layouts_data) ) {
		$single_page = new $this->Pages_model;
		$single_page->setClientId($this->session->userdata('current_client'),true);
		$single_page->setId(get_layout_value('common_pages','single_page', $layouts_data),true);
		
		$single_page_data['page'] = $single_page->get();
		$single_page_data['options'] = $this->_get_page_options($this->session->userdata('current_client'), get_layout_value('common_pages','single_page', $layouts_data));

		$single_page_rows = new $this->Pages_rows_model('pr');
		$single_page_rows->set_join('rows r', 'r.id=pr.row_id');
		$single_page_rows->setClientId($this->session->userdata('current_client'),true);
		$single_page_rows->setPageId(get_layout_value('common_pages','single_page', $layouts_data),true);
		$single_page_rows->setActive(1,true);
		$single_page_rows->set_limit(0);
		$single_page_rows->set_order('order', 'ASC');
		$single_page_rows->set_select("*");

		$single_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

		foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
			$single_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

		foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
			$single_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

}

// error_404
$error_404_data = array();
if( get_layout_value('common_pages','error_404', $layouts_data) ) {
		$error_404_page = new $this->Pages_model;
		$error_404_page->setClientId($this->session->userdata('current_client'),true);
		$error_404_page->setId(get_layout_value('common_pages','error_404', $layouts_data),true);
		
		$error_404_data['page'] = $error_404_page->get();
		$error_404_data['options'] = $this->_get_page_options($this->session->userdata('current_client'), get_layout_value('common_pages','error_404', $layouts_data));

		$error404_rows = new $this->Pages_rows_model('pr');
		$error404_rows->set_join('rows r', 'r.id=pr.row_id');
		$error404_rows->setClientId($this->session->userdata('current_client'),true);
		$error404_rows->setPageId(get_layout_value('common_pages','error_404', $layouts_data),true);
		$error404_rows->setActive(1,true);
		$error404_rows->set_limit(0);
		$error404_rows->set_order('order', 'ASC');
		$error404_rows->set_select("*");

		$error404_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

		foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
			$error404_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

		foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
			$error404_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

}

// blog_page
$blog_page_data = array();
if( get_layout_value('common_pages','blog_page', $layouts_data) ) {
		
		$blog_page = new $this->Pages_model;
		$blog_page->setClientId($this->session->userdata('current_client'),true);
		$blog_page->setId(get_layout_value('common_pages','blog_page', $layouts_data),true);
		$blog_page_data['page'] = $blog_page->get();
		$blog_page_data['options'] = $this->_get_page_options($this->session->userdata('current_client'), get_layout_value('common_pages','blog_page', $layouts_data));

		$blog_page_rows = new $this->Pages_rows_model('pr');
		$blog_page_rows->set_join('rows r', 'r.id=pr.row_id');
		$blog_page_rows->setClientId($this->session->userdata('current_client'),true);
		$blog_page_rows->setPageId(get_layout_value('common_pages','blog_page', $layouts_data),true);
		$blog_page_rows->setActive(1,true);
		$blog_page_rows->set_limit(0);
		$blog_page_rows->set_order('order', 'ASC');
		$blog_page_rows->set_select("*");

		$blog_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");
		
		foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
			$blog_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}

		foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
			$blog_page_rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}
}

		switch($version) {
			case '1':
			if( get_layout_value('common_pages','homepage', $layouts_data) ) {
				$new_page_data['homepage']['rows'] = $this->_assign_row_widgets( $homepage_rows->recursive('r.parent_row', 0) );
			}
			if( get_layout_value('common_pages','single_page', $layouts_data) ) {
				$single_page_data['rows'] = $this->_assign_row_widgets( $single_page_rows->recursive('r.parent_row', 0) );
			}
			if( get_layout_value('common_pages','error_404', $layouts_data) ) {
				$error_404_data['rows'] = $this->_assign_row_widgets( $error404_rows->recursive('r.parent_row', 0) );
			}
			if( get_layout_value('common_pages','blog_page', $layouts_data) ) {
				$blog_page_data['rows'] = $this->_assign_row_widgets( $blog_page_rows->recursive('r.parent_row', 0) );
			}

			foreach($pages_data as $page_id=>$page_d) {
				$new_page_data[$page_d->page_slug]['page'] = $page_d;
				$new_page_data[$page_d->page_slug]['options'] = $this->_get_page_options($id, $page_d->id);
				
				$rows = new $this->Pages_rows_model('pr');
				$rows->set_join('rows r', 'r.id=pr.row_id');
				$rows->setClientId($id,true);
				$rows->setPageId($page_d->id,true);
				$rows->setActive(1,true);
				$rows->set_limit(0);
				$rows->set_order('order', 'ASC');
				$rows->set_select("*");

				$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

				foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
					$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}

				foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
					$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}


				$new_page_data[$page_d->page_slug]['rows'] = $this->_assign_row_widgets( $rows->recursive('parent_row', 0) );
			}
			break;
			case '2':
			case '3':
			case '4':
			case '5':

			if( get_layout_value('common_pages','homepage', $layouts_data) ) {
				$new_page_data['homepage']['rows'] = $homepage_rows->populate();
			}
			if( get_layout_value('common_pages','single_page', $layouts_data) ) {
				$single_page_data['rows'] = $this->_assign_row_widgets( $single_page_rows->recursive('r.parent_row', 0) );
			}
			if( get_layout_value('common_pages','error_404', $layouts_data) ) {
				$error_404_data['rows'] = $error404_rows->populate();
			}
			if( get_layout_value('common_pages','blog_page', $layouts_data) ) {
				$blog_page_data['rows'] = $blog_page_rows->populate();
			}

			$rows = new $this->Rows_model('r');
			$rows->setClientId($this->session->userdata('current_client'),true);
			$rows->set_order('active', 'DESC');
			$rows->set_order('id', 'DESC');
			$rows->set_limit(0);
			$rows->set_select("*");

			$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

				foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
					$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}

				foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
					$rows->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=r.id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}

			$this->template_data->set('rows', $this->_assign_row_widgets( $rows->recursive('parent_row', 0) ) );

			foreach($pages_data as $page_id=>$page_d) {

				$parent_page = new $this->Pages_model('pp');
				$parent_page->setId($page_d->parent_page,true);
				$page_d->parent_data = $parent_page->get();
				
				$new_page_data[$page_d->page_slug]['page'] = $page_d;
				$new_page_data[$page_d->page_slug]['options'] = $this->_get_page_options($id, $page_d->id);

				$rows2 = new $this->Pages_rows_model('pr');
				$rows2->set_join('rows r', 'r.id=pr.row_id');
				$rows2->setClientId($id,true);
				$rows2->setPageId($page_d->id,true);
				$rows2->setActive(1,true);
				$rows2->set_limit(0);
				$rows2->set_order('order', 'ASC');
				$rows2->set_select("*");

				$rows2->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='display_title' LIMIT 1) as row_display_title");

				foreach( array('display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
					$rows2->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}

				foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
					$rows2->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=pr.row_id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
				}

				$new_page_data[$page_d->page_slug]['rows'] = $rows2->populate();
			}

			break;
		}
		
		$this->template_data->set('single_page', $single_page_data);
		$this->template_data->set('error_404_page', $error_404_data);
		$this->template_data->set('blog_page', $blog_page_data);

		$this->template_data->set('pages', $new_page_data);

		$this->template_data->set('template_output', 'blogger_xml');

		$this->output->set_content_type('text/plain');
		header('Content-Type: text/plain');
		
		$this->load->view("templates/theme/{$name}/xml_{$name}_{$version}", $this->template_data->get_data());
	}

}
