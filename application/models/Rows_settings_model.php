<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rows_settings_model Class
 *
 * Manipulates `rows_settings` table on database

CREATE TABLE `rows_settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `setting_key` varchar(100) NOT NULL,
  `setting_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `rows_settings` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `rows_settings` ADD  `row_id` int(20) NOT NULL   ;
ALTER TABLE  `rows_settings` ADD  `setting_key` varchar(100) NOT NULL   ;
ALTER TABLE  `rows_settings` ADD  `setting_value` text NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Rows_settings_model extends MY_Model {

	protected $id;
	protected $row_id;
	protected $setting_key;
	protected $setting_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'rows_settings';
		$this->_short_name = 'rows_settings';
		$this->_fields = array("id","row_id","setting_key","setting_value");
		$this->_required = array("row_id","setting_key","setting_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: row_id -------------------------------------- 

	/** 
	* Sets a value to `row_id` variable
	* @access public
	*/

	public function setRowId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_row_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `row_id` variable
	* @access public
	*/

	public function getRowId() {
		return $this->row_id;
	}

	public function get_row_id() {
		return $this->row_id;
	}

	
// ------------------------------ End Field: row_id --------------------------------------


// ---------------------------- Start Field: setting_key -------------------------------------- 

	/** 
	* Sets a value to `setting_key` variable
	* @access public
	*/

	public function setSettingKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('setting_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_setting_key($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('setting_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `setting_key` variable
	* @access public
	*/

	public function getSettingKey() {
		return $this->setting_key;
	}

	public function get_setting_key() {
		return $this->setting_key;
	}

	
// ------------------------------ End Field: setting_key --------------------------------------


// ---------------------------- Start Field: setting_value -------------------------------------- 

	/** 
	* Sets a value to `setting_value` variable
	* @access public
	*/

	public function setSettingValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('setting_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_setting_value($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('setting_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `setting_value` variable
	* @access public
	*/

	public function getSettingValue() {
		return $this->setting_value;
	}

	public function get_setting_value() {
		return $this->setting_value;
	}

	
// ------------------------------ End Field: setting_value --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'row_id' => (object) array(
										'Field'=>'row_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'setting_key' => (object) array(
										'Field'=>'setting_key',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'setting_value' => (object) array(
										'Field'=>'setting_value',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `rows_settings` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'row_id' => "ALTER TABLE  `rows_settings` ADD  `row_id` int(20) NOT NULL   ;",
			'setting_key' => "ALTER TABLE  `rows_settings` ADD  `setting_key` varchar(100) NOT NULL   ;",
			'setting_value' => "ALTER TABLE  `rows_settings` ADD  `setting_value` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setRowId() - row_id
//setSettingKey() - setting_key
//setSettingValue() - setting_value

--------------------------------------

//set_id() - id
//set_row_id() - row_id
//set_setting_key() - setting_key
//set_setting_value() - setting_value

*/
/* End of file Rows_settings_model.php */
/* Location: ./application/models/Rows_settings_model.php */
