<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>
<form method="post">
<div class="container">
<div class="row">
	<div class="col-md-3">
<div class="panel panel-default">
	    	<div class="panel-heading">
	    	<button type="submit" class="btn btn-warning btn-xs pull-right margin-left-5">Save Changes</button>

	    		<h3 class="panel-title">Fixed Pages</h3>
	    	</div>
	    	<div class="panel-body">
	    			<?php $this->load->view('layout/_layout_common_pages'); ?>
	    	</div>
</div>
	</div>
	<div class="col-md-9">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<button type="submit" class="btn btn-warning btn-xs pull-right margin-left-5">Save Changes</button>

	    		<h3 class="panel-title">Design Settings</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>


<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	<?php $this->load->view('layout/_layout_head'); ?>
	<?php $this->load->view('layout/_layout_css'); ?>
	<?php $this->load->view('layout/_layout_top_nav'); ?>
	<?php $this->load->view('layout/_layout_header'); ?>
	<?php $this->load->view('layout/_layout_pages'); ?>
	<?php $this->load->view('layout/_layout_footer'); ?>
	<?php $this->load->view('layout/_layout_foot'); ?>

</div>


<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
	    
    </div>
</div>
</div>
</form>
<?php endif; ?>

<?php $this->load->view('footer'); ?>