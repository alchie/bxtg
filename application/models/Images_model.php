<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Images_model Class
 *
 * Manipulates `images` table on database

CREATE TABLE `images` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_ext` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `image_width` int(10) NOT NULL,
  `image_height` int(10) NOT NULL,
  `image_type` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  `upload_url` text DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin;

ALTER TABLE  `images` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `images` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `images` ADD  `file_name` varchar(200) NOT NULL   ;
ALTER TABLE  `images` ADD  `file_type` varchar(100) NOT NULL   ;
ALTER TABLE  `images` ADD  `file_ext` varchar(100) NOT NULL   ;
ALTER TABLE  `images` ADD  `file_size` varchar(100) NOT NULL   ;
ALTER TABLE  `images` ADD  `image_width` int(10) NOT NULL   ;
ALTER TABLE  `images` ADD  `image_height` int(10) NOT NULL   ;
ALTER TABLE  `images` ADD  `image_type` varchar(100) NOT NULL   ;
ALTER TABLE  `images` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `images` ADD  `upload_url` text NULL   ;
ALTER TABLE  `images` ADD  `title` varchar(100) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Images_model extends MY_Model {

	protected $id;
	protected $client_id;
	protected $file_name;
	protected $file_type;
	protected $file_ext;
	protected $file_size;
	protected $image_width;
	protected $image_height;
	protected $image_type;
	protected $active;
	protected $upload_url;
	protected $title;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'images';
		$this->_short_name = 'images';
		$this->_fields = array("id","client_id","file_name","file_type","file_ext","file_size","image_width","image_height","image_type","active","upload_url","title");
		$this->_required = array("client_id","file_name","file_type","file_ext","file_size","image_width","image_height","image_type","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: file_name -------------------------------------- 

	/** 
	* Sets a value to `file_name` variable
	* @access public
	*/

	public function setFileName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_file_name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `file_name` variable
	* @access public
	*/

	public function getFileName() {
		return $this->file_name;
	}

	public function get_file_name() {
		return $this->file_name;
	}

	
// ------------------------------ End Field: file_name --------------------------------------


// ---------------------------- Start Field: file_type -------------------------------------- 

	/** 
	* Sets a value to `file_type` variable
	* @access public
	*/

	public function setFileType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_file_type($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `file_type` variable
	* @access public
	*/

	public function getFileType() {
		return $this->file_type;
	}

	public function get_file_type() {
		return $this->file_type;
	}

	
// ------------------------------ End Field: file_type --------------------------------------


// ---------------------------- Start Field: file_ext -------------------------------------- 

	/** 
	* Sets a value to `file_ext` variable
	* @access public
	*/

	public function setFileExt($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_ext', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_file_ext($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_ext', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `file_ext` variable
	* @access public
	*/

	public function getFileExt() {
		return $this->file_ext;
	}

	public function get_file_ext() {
		return $this->file_ext;
	}

	
// ------------------------------ End Field: file_ext --------------------------------------


// ---------------------------- Start Field: file_size -------------------------------------- 

	/** 
	* Sets a value to `file_size` variable
	* @access public
	*/

	public function setFileSize($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_size', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_file_size($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('file_size', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `file_size` variable
	* @access public
	*/

	public function getFileSize() {
		return $this->file_size;
	}

	public function get_file_size() {
		return $this->file_size;
	}

	
// ------------------------------ End Field: file_size --------------------------------------


// ---------------------------- Start Field: image_width -------------------------------------- 

	/** 
	* Sets a value to `image_width` variable
	* @access public
	*/

	public function setImageWidth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_width', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_image_width($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_width', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `image_width` variable
	* @access public
	*/

	public function getImageWidth() {
		return $this->image_width;
	}

	public function get_image_width() {
		return $this->image_width;
	}

	
// ------------------------------ End Field: image_width --------------------------------------


// ---------------------------- Start Field: image_height -------------------------------------- 

	/** 
	* Sets a value to `image_height` variable
	* @access public
	*/

	public function setImageHeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_height', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_image_height($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_height', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `image_height` variable
	* @access public
	*/

	public function getImageHeight() {
		return $this->image_height;
	}

	public function get_image_height() {
		return $this->image_height;
	}

	
// ------------------------------ End Field: image_height --------------------------------------


// ---------------------------- Start Field: image_type -------------------------------------- 

	/** 
	* Sets a value to `image_type` variable
	* @access public
	*/

	public function setImageType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_image_type($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('image_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `image_type` variable
	* @access public
	*/

	public function getImageType() {
		return $this->image_type;
	}

	public function get_image_type() {
		return $this->image_type;
	}

	
// ------------------------------ End Field: image_type --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: upload_url -------------------------------------- 

	/** 
	* Sets a value to `upload_url` variable
	* @access public
	*/

	public function setUploadUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('upload_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_upload_url($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('upload_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `upload_url` variable
	* @access public
	*/

	public function getUploadUrl() {
		return $this->upload_url;
	}

	public function get_upload_url() {
		return $this->upload_url;
	}

	
// ------------------------------ End Field: upload_url --------------------------------------


// ---------------------------- Start Field: title -------------------------------------- 

	/** 
	* Sets a value to `title` variable
	* @access public
	*/

	public function setTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_title($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `title` variable
	* @access public
	*/

	public function getTitle() {
		return $this->title;
	}

	public function get_title() {
		return $this->title;
	}

	
// ------------------------------ End Field: title --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'file_name' => (object) array(
										'Field'=>'file_name',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'file_type' => (object) array(
										'Field'=>'file_type',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'file_ext' => (object) array(
										'Field'=>'file_ext',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'file_size' => (object) array(
										'Field'=>'file_size',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'image_width' => (object) array(
										'Field'=>'image_width',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'image_height' => (object) array(
										'Field'=>'image_height',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'image_type' => (object) array(
										'Field'=>'image_type',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'upload_url' => (object) array(
										'Field'=>'upload_url',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'title' => (object) array(
										'Field'=>'title',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `images` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'client_id' => "ALTER TABLE  `images` ADD  `client_id` int(20) NOT NULL   ;",
			'file_name' => "ALTER TABLE  `images` ADD  `file_name` varchar(200) NOT NULL   ;",
			'file_type' => "ALTER TABLE  `images` ADD  `file_type` varchar(100) NOT NULL   ;",
			'file_ext' => "ALTER TABLE  `images` ADD  `file_ext` varchar(100) NOT NULL   ;",
			'file_size' => "ALTER TABLE  `images` ADD  `file_size` varchar(100) NOT NULL   ;",
			'image_width' => "ALTER TABLE  `images` ADD  `image_width` int(10) NOT NULL   ;",
			'image_height' => "ALTER TABLE  `images` ADD  `image_height` int(10) NOT NULL   ;",
			'image_type' => "ALTER TABLE  `images` ADD  `image_type` varchar(100) NOT NULL   ;",
			'active' => "ALTER TABLE  `images` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'upload_url' => "ALTER TABLE  `images` ADD  `upload_url` text NULL   ;",
			'title' => "ALTER TABLE  `images` ADD  `title` varchar(100) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setClientId() - client_id
//setFileName() - file_name
//setFileType() - file_type
//setFileExt() - file_ext
//setFileSize() - file_size
//setImageWidth() - image_width
//setImageHeight() - image_height
//setImageType() - image_type
//setActive() - active
//setUploadUrl() - upload_url
//setTitle() - title

--------------------------------------

//set_id() - id
//set_client_id() - client_id
//set_file_name() - file_name
//set_file_type() - file_type
//set_file_ext() - file_ext
//set_file_size() - file_size
//set_image_width() - image_width
//set_image_height() - image_height
//set_image_type() - image_type
//set_active() - active
//set_upload_url() - upload_url
//set_title() - title

*/
/* End of file Images_model.php */
/* Location: ./application/models/Images_model.php */
