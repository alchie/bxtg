<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">
    <?php echo form_open_multipart('images/do_upload');?>
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Upload</h3>
            </div>

            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<input type="submit" value="Upload Now" class="pull-right btn btn-xs btn-primary" />
<input type="file" name="userfile" size="20" />



<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>

        </div>
    </div>
</form>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>