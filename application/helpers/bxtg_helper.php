<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_layout_value'))
{
     function get_layout_value($location, $key, $layouts) {
       if( $layouts ) {
         foreach($layouts as $layout) {
              if ( ( $layout->location == $location) && ( $layout->layout_key == $key ) ) {
                  return $layout->layout_value;
              }
         }
        }
        return false;
     }
}

if ( ! function_exists('get_layout_data'))
{
     function get_layout_data($location, $key) {
         $CI = get_instance();
         $layout = new $CI->Layouts_model;
         $layout->setClientId($CI->session->userdata('current_client'),true);
         $layout->setLocation($location,true);
         $layout->setLayoutKey($key,true);
         $layout->set_select("layout_value");
         $data = $layout->get();
         return (($data) && (isset($data->layout_value))) ? $data->layout_value : false;
     }
}

if ( ! function_exists('get_menu'))
{
     function get_menu($id) {
        $CI = get_instance();
        $CI->load->model('Menus_model');
        $links = new $CI->Menus_model('ml');
        $links->setId($id,true);
        return $links->get();
     }
}

if ( ! function_exists('get_menu_links'))
{
     function get_menu_links($id) {
        $CI = get_instance();
        $CI->load->model('Menus_links_model');
        $links = new $CI->Menus_links_model('ml');
        $links->setMenuId($id,true);
        $links->set_limit(0);
        $links->_set_order('order', 'ASC');
        $links->set_select("ml.*");
        $links->set_select("(SELECT page_slug FROM pages WHERE ml.inner_page=pages.id) as page_slug");
        return $links->recursive('parent_id', 0);
     }
}

if ( ! function_exists('get_page'))
{
     function get_page($id) {
        $CI = get_instance();
        $CI->load->model('Pages_model');
        $page = new $CI->Pages_model('p');
        $page->setId($id,true);
        $page->set_limit(1);
        return $page->get();
     }
}

if ( ! function_exists('get_option_value'))
{
     function get_option_value($key, $options) {
       if( $options ) {
           foreach($options as $option) {
                if ( $option->opt_key == $key)  {
                    return $option->opt_value;
                }
           }
        }
        return false;
     }
}

if ( ! function_exists('get_container_value'))
{
     function get_container_value($key, $i, $options) {
       if( $options ) { 
           foreach($options as $option) {
                if ( ( $option->opt_key == $key ) && ( $option->page_container == $i )  )  {
                    return $option->opt_value;
                }
           }
        }
        return false;
     }
}

// ------------------------------------------------------------------------
if ( ! function_exists('get_clients_meta'))
{
     function get_clients_meta($id, $meta_key) {
        $CI = get_instance();
        $CI->load->model('Clients_meta_model');
        $meta = new $CI->Clients_meta_model('m');
        $meta->setId($id,true);
        $meta->setMetaKey($meta_key, true);
        $meta->set_limit(1);
        $meta_data = $meta->get();
        return ($meta_data) ? $meta_data->meta_value : false;
     }
}

if ( ! function_exists('insert_clients_meta'))
{
     function insert_clients_meta($id, $meta_key, $meta_value, $multiple=false) {
        $CI = get_instance();
        $CI->load->model('Clients_meta_model');
        $meta = new $CI->Clients_meta_model('m');
        $meta->setClientId($id,true,true);
        $meta->setMetaKey($meta_key, true, true);
        $meta->setMetaValue($meta_value, false, true);
        if( $multiple ) {
            $meta->insert();
        } else {
            if( $meta->nonEmpty() ) {
                $meta->update();
            } else {
                $meta->insert();
            }
        }
     }
}
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
if ( ! function_exists('get_menus_meta'))
{
     function get_menus_meta($id, $meta_key) {
        $CI = get_instance();
        $CI->load->model('Menus_meta_model');
        $meta = new $CI->Menus_meta_model('m');
        $meta->setId($id,true);
        $meta->setMetaKey($meta_key, true);
        $meta->set_limit(1);
        $meta_data = $meta->get();
        return ($meta_data) ? $meta_data->meta_value : false;
     }
}

if ( ! function_exists('insert_menus_meta'))
{
     function insert_menus_meta($id, $meta_key, $meta_value, $multiple=false) {
        $CI = get_instance();
        $CI->load->model('Menus_meta_model');
        $meta = new $CI->Menus_meta_model('m');
        $meta->setMenuId($id,true,true);
        $meta->setMetaKey($meta_key, true, true);
        $meta->setMetaValue($meta_value, false, true);
        if( $multiple ) {
            $meta->insert();
        } else {
            if( $meta->nonEmpty() ) {
                $meta->update();
            } else {
                $meta->insert();
            }
        }
     }
}
// ------------------------------------------------------------------------

if ( ! function_exists('get_image'))
{
     function get_image($id) {
        $CI = get_instance();
        $CI->load->model('Images_model');
        $meta = new $CI->Images_model('m');
        $meta->setId($id,true);
        $meta_data = $meta->get();
        return $meta_data;
     }
}

// ------------------------------------------------------------------------