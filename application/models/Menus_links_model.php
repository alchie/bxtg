<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Menus_links_model Class
 *
 * Manipulates `menus_links` table on database

CREATE TABLE `menus_links` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `menu_id` int(20) NOT NULL,
  `text` text NOT NULL,
  `url` text NOT NULL,
  `icon` text DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `inner_page` int(20) DEFAULT 0,
  `order` int(2) DEFAULT 0,
  `target` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin;

ALTER TABLE  `menus_links` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `menus_links` ADD  `menu_id` int(20) NOT NULL   ;
ALTER TABLE  `menus_links` ADD  `text` text NOT NULL   ;
ALTER TABLE  `menus_links` ADD  `url` text NOT NULL   ;
ALTER TABLE  `menus_links` ADD  `icon` text NULL   ;
ALTER TABLE  `menus_links` ADD  `parent_id` int(20) NULL   ;
ALTER TABLE  `menus_links` ADD  `active` int(1) NULL   DEFAULT '1';
ALTER TABLE  `menus_links` ADD  `inner_page` int(20) NULL   DEFAULT '0';
ALTER TABLE  `menus_links` ADD  `order` int(2) NULL   DEFAULT '0';
ALTER TABLE  `menus_links` ADD  `target` varchar(50) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Menus_links_model extends MY_Model {

	protected $id;
	protected $menu_id;
	protected $text;
	protected $url;
	protected $icon;
	protected $parent_id;
	protected $active;
	protected $inner_page;
	protected $order;
	protected $target;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'menus_links';
		$this->_short_name = 'menus_links';
		$this->_fields = array("id","menu_id","text","url","icon","parent_id","active","inner_page","order","target");
		$this->_required = array("menu_id","text","url");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: menu_id -------------------------------------- 

	/** 
	* Sets a value to `menu_id` variable
	* @access public
	*/

	public function setMenuId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('menu_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_menu_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('menu_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `menu_id` variable
	* @access public
	*/

	public function getMenuId() {
		return $this->menu_id;
	}

	public function get_menu_id() {
		return $this->menu_id;
	}

	
// ------------------------------ End Field: menu_id --------------------------------------


// ---------------------------- Start Field: text -------------------------------------- 

	/** 
	* Sets a value to `text` variable
	* @access public
	*/

	public function setText($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('text', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_text($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('text', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `text` variable
	* @access public
	*/

	public function getText() {
		return $this->text;
	}

	public function get_text() {
		return $this->text;
	}

	
// ------------------------------ End Field: text --------------------------------------


// ---------------------------- Start Field: url -------------------------------------- 

	/** 
	* Sets a value to `url` variable
	* @access public
	*/

	public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_url($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `url` variable
	* @access public
	*/

	public function getUrl() {
		return $this->url;
	}

	public function get_url() {
		return $this->url;
	}

	
// ------------------------------ End Field: url --------------------------------------


// ---------------------------- Start Field: icon -------------------------------------- 

	/** 
	* Sets a value to `icon` variable
	* @access public
	*/

	public function setIcon($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('icon', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_icon($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('icon', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `icon` variable
	* @access public
	*/

	public function getIcon() {
		return $this->icon;
	}

	public function get_icon() {
		return $this->icon;
	}

	
// ------------------------------ End Field: icon --------------------------------------


// ---------------------------- Start Field: parent_id -------------------------------------- 

	/** 
	* Sets a value to `parent_id` variable
	* @access public
	*/

	public function setParentId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_parent_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parent_id` variable
	* @access public
	*/

	public function getParentId() {
		return $this->parent_id;
	}

	public function get_parent_id() {
		return $this->parent_id;
	}

	
// ------------------------------ End Field: parent_id --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: inner_page -------------------------------------- 

	/** 
	* Sets a value to `inner_page` variable
	* @access public
	*/

	public function setInnerPage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('inner_page', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_inner_page($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('inner_page', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `inner_page` variable
	* @access public
	*/

	public function getInnerPage() {
		return $this->inner_page;
	}

	public function get_inner_page() {
		return $this->inner_page;
	}

	
// ------------------------------ End Field: inner_page --------------------------------------


// ---------------------------- Start Field: order -------------------------------------- 

	/** 
	* Sets a value to `order` variable
	* @access public
	*/

	public function setOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_order($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `order` variable
	* @access public
	*/

	public function getOrder() {
		return $this->order;
	}

	public function get_order() {
		return $this->order;
	}

	
// ------------------------------ End Field: order --------------------------------------


// ---------------------------- Start Field: target -------------------------------------- 

	/** 
	* Sets a value to `target` variable
	* @access public
	*/

	public function setTarget($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('target', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_target($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('target', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `target` variable
	* @access public
	*/

	public function getTarget() {
		return $this->target;
	}

	public function get_target() {
		return $this->target;
	}

	
// ------------------------------ End Field: target --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'menu_id' => (object) array(
										'Field'=>'menu_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'text' => (object) array(
										'Field'=>'text',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'url' => (object) array(
										'Field'=>'url',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'icon' => (object) array(
										'Field'=>'icon',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'parent_id' => (object) array(
										'Field'=>'parent_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'inner_page' => (object) array(
										'Field'=>'inner_page',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'order' => (object) array(
										'Field'=>'order',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'target' => (object) array(
										'Field'=>'target',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `menus_links` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'menu_id' => "ALTER TABLE  `menus_links` ADD  `menu_id` int(20) NOT NULL   ;",
			'text' => "ALTER TABLE  `menus_links` ADD  `text` text NOT NULL   ;",
			'url' => "ALTER TABLE  `menus_links` ADD  `url` text NOT NULL   ;",
			'icon' => "ALTER TABLE  `menus_links` ADD  `icon` text NULL   ;",
			'parent_id' => "ALTER TABLE  `menus_links` ADD  `parent_id` int(20) NULL   ;",
			'active' => "ALTER TABLE  `menus_links` ADD  `active` int(1) NULL   DEFAULT '1';",
			'inner_page' => "ALTER TABLE  `menus_links` ADD  `inner_page` int(20) NULL   DEFAULT '0';",
			'order' => "ALTER TABLE  `menus_links` ADD  `order` int(2) NULL   DEFAULT '0';",
			'target' => "ALTER TABLE  `menus_links` ADD  `target` varchar(50) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setMenuId() - menu_id
//setText() - text
//setUrl() - url
//setIcon() - icon
//setParentId() - parent_id
//setActive() - active
//setInnerPage() - inner_page
//setOrder() - order
//setTarget() - target

--------------------------------------

//set_id() - id
//set_menu_id() - menu_id
//set_text() - text
//set_url() - url
//set_icon() - icon
//set_parent_id() - parent_id
//set_active() - active
//set_inner_page() - inner_page
//set_order() - order
//set_target() - target

*/
/* End of file Menus_links_model.php */
/* Location: ./application/models/Menus_links_model.php */
