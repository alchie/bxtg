<div class="container" style="margin-bottom:10px;">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
<?php if( (isset($page)) && ($page) ) { ?>
              <li role="presentation" class="<?php echo ($location=='edit')? 'active' : ''; ?>"><a href="<?php echo site_url("pages/edit/{$page_id}"); ?>">Edit Page</a></li>
<?php } ?>
<?php if( (isset($page)) && ($page) && ($page->subpages > 0) ) { ?>
              <li role="presentation" class="<?php echo ($location=='subpages')? 'active' : ''; ?>"><a href="<?php echo site_url("pages/subpages/{$page_id}"); ?>">Sub-Pages</a></li>
<?php } ?>
              <li role="presentation" class="<?php echo ($location=='options')? 'active' : ''; ?>"><a href="<?php echo site_url("pages/options/{$client_id}/{$page_id}"); ?>">Page Options</a></li>
              <li role="presentation" class="<?php echo ($location=='widgets')? 'active' : ''; ?>"><a href="<?php echo site_url("pages/rows/{$client_id}/{$page_id}"); ?>">Page Rows</a></li>
            </ul>
        </div>
    </div>
</div>