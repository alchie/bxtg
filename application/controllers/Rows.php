<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rows extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Rows');
		$this->template_data->set('current_uri', 'rows');

		$this->_isAuth('clients', 'clients', 'view');
		$this->_isAuth('clients', 'clients', 'edit');

		$this->check_current_client();

	}

	public function index($start=0) {

		$this->template_data->set('group_id', false);

		$rows = new $this->Rows_model;
		$rows->setClientId($this->session->userdata('current_client'),true);
		$rows->set_order('active', 'DESC');
		$rows->set_order('id', 'DESC');
		$rows->set_start($start);

		if( $this->input->get('q') ) {
			$rows->set_where('(title LIKE "%'.$this->input->get('q').'%")');
		}

		$rows->set_select('rows.*');
		$rows->set_select("(SELECT rg.group_name FROM rows_groups rg WHERE rg.group_id=rows.group_id) as group_name");

		$this->template_data->set('rows', $rows->recursive('parent_row', 0));
		$this->template_data->set('rows_count', $rows->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/rows/index"),
			'total_rows' => $rows->count_all_results(),
			'per_page' => $rows->get_limit()
		)));


		$groups = new $this->Rows_groups_model;
		$groups->setClientId($this->session->userdata('current_client'),true);
		$groups->set_order('group_name', 'ASC');
		$groups->set_limit(0);
		$this->template_data->set('groups', $groups->populate());

		$this->load->view('rows/rows_list', $this->template_data->get_data());

	}

	public function group($group_id, $start=0) {

		$this->template_data->set('group_id', $group_id);

		$rows = new $this->Rows_model;
		$rows->setClientId($this->session->userdata('current_client'),true);
		$rows->setGroupId($group_id,true);
		$rows->set_order('active', 'DESC');
		$rows->set_order('id', 'DESC');
		$rows->set_start($start);

		$this->template_data->set('rows', $rows->recursive('parent_row', 0));
		$this->template_data->set('rows_count', $rows->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/rows/group/{$group_id}"),
			'total_rows' => $rows->count_all_results(),
			'per_page' => $rows->get_limit()
		)));

		$groups = new $this->Rows_groups_model;
		$groups->setClientId($this->session->userdata('current_client'),true);
		$groups->set_order('group_name', 'ASC');
		$groups->set_limit(0);
		$this->template_data->set('groups', $groups->populate());

		$current_group = new $this->Rows_groups_model;
		$current_group->setGroupId($group_id,true);
		$this->template_data->set('current_group', $current_group->get());

		$this->load->view('rows/rows_list', $this->template_data->get_data());

	}

	public function add_row($output='') {

		if( $this->input->post() ) {

			$this->form_validation->set_rules('columns', 'Columns', 'trim|required');
			
			if( $this->form_validation->run() ) {
				$row = new $this->Rows_model;
				$row->setClientId($this->session->userdata('current_client'));
				$row->setTitle($this->input->post('row_title'));
				$row->setColumns($this->input->post('columns'));
				$row->setRowClass($this->input->post('row_class'));
				$row->setContainer((($this->input->post('container'))?1:0));
				$row->setContainerClass($this->input->post('container_class'));
				$row->setWrapperClass($this->input->post('wrapper_class'));
				$row->setGroupId($this->input->post('group_id'));
				$row->setActive(1);
				$row->insert();
				redirect("rows");
			}
		}

		$groups = new $this->Rows_groups_model;
		$groups->setClientId($this->session->userdata('current_client'),true);
		$groups->set_order('group_name', 'ASC');
		$groups->set_limit(0);
		$this->template_data->set('groups', $groups->populate());

		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_add_row', $this->template_data->get_data());
	}

	public function edit_row($row_id, $page_id='', $output='') {

		$row = new $this->Rows_model;
		$row->setId($row_id,true);

		if( $this->input->post() ) {

			if( $this->input->post('columns')) {
				$this->form_validation->set_rules('columns', 'Columns', 'trim|required');
				if( $this->form_validation->run() ) {
					$row->setTitle($this->input->post('row_title'),false,true);
					$row->setColumns($this->input->post('columns'),false,true);
					$row->setRowClass($this->input->post('row_class'),false,true);
					$row->setContainer((($this->input->post('container'))?1:0),false,true);
					$row->setContainerClass($this->input->post('container_class'),false,true);
					$row->setWrapperClass($this->input->post('wrapper_class'),false,true);
					$row->setDisplayRows((($this->input->post('display_rows'))?1:0),false,true);
					$row->setParentRow($this->input->post('parent_row'),false,true);
					$row->setActive((($this->input->post('active'))?1:0),false,true);
					$row->setGroupId($this->input->post('group_id'),false,true);
					$row->update();
					
				}
			}

			$row_settings = $this->input->post('row_settings');

			foreach( array('display_title', 'display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
				if( isset($row_settings[$key]) ) {
					$this->setSettings($row_id, $key, $row_settings[$key]);
				} else {
					$this->setSettings($row_id, $key, '', true);
				}
			}

			foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
				if( isset($row_settings[$key]) ) {
					$this->setSettings($row_id, $key, $row_settings[$key]);
				} else {
					$this->setSettings($row_id, $key, '', true);
				}
			}

			if( $this->input->post('column_order') ) {
				foreach( $this->input->post('column_order') as $order=>$column_id  ) {
					$column = new $this->Rows_columns_model;
					$column->setId($column_id,true);
					$column->setColumnOrder($order,false,true);
					$column->update();
				}
			}

			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') );
			}
			
		}
		$row->set_select("*");
		foreach( array('display_title', 'display_column_settings', 'blogger_main_includable', 'blogger_post_name', 'blogger_post_row_class', 'generate_row_container', 'remove_row_class') as $key ) {
			$row->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=rows.id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}
		foreach( array('blogger_postlist_textbox', 'blogger_postlist_textbox_time', 'blogger_postlist_textbox_snippet', 'blogger_postlist_textbox_title') as $key ) {
			$row->set_select("(SELECT rs.setting_value FROM rows_settings rs WHERE rs.row_id=rows.id AND rs.setting_key='{$key}' LIMIT 1) as {$key}");
		}
		
		$row_data = $row->get();
		$this->template_data->set('row', $row_data );

		$columns = new $this->Rows_columns_model('rc');
		$columns->setRowId($row_id,true);
		$columns->set_limit(0);
		$columns->set_order('rc.column_order', 'ASC');
		$columns->set_order('rc.column', 'ASC');
		$columns->set_select("rc.*");
		$columns->set_select("(SELECT COUNT(*) FROM rows_columns_widgets rcw WHERE rcw.row_id=rc.row_id AND rcw.column=rc.column) as widgets_count");
		$this->template_data->set('columns', $columns->populate() );

		$parent = new $this->Rows_model;
		$parent->setClientId($row_data->client_id,true);
		$parent->setActive(1,true);
		$parent->set_where('id !=', $row_id);
		$parent->set_limit(0);
		$parent_data = $parent->recursive('parent_row', 0);

		if( $page_id != '' ) {
			if( $page_id > 0) {
				$page = new $this->Pages_model;
				$page->setId($page_id, true);
				$this->template_data->set('page', $page->get());
			} else {
				$this->template_data->set('page', (object) array(
					'page_title' => 'Homepage',
					'client_id' => $row_data->client_id,
					'id' => $page_id
				));
			}
		}

		$pages = new $this->Pages_model('p');
		$pages->setClientId($row_data->client_id,true);
		$pages->set_limit(0);
		$pages->set_select('p.*');

			$pr_id = new $this->Pages_rows_model('pr');
			$pr_id->set_where('p.id=pr.page_id');
			$pr_id->set_where('p.client_id=pr.client_id');
			$pr_id->set_where('pr.row_id='. $row_id);
			$pr_id->set_select('pr.id');
			$pr_id->set_limit(1);
			$pages->set_select('('.$pr_id->get_compiled_select().') as pr_id');
			$pages->set_where('(('.$pr_id->get_compiled_select().') IS NOT NULL)');

			$pr_active = new $this->Pages_rows_model('pr');
			$pr_active->set_where('p.id=pr.page_id');
			$pr_active->set_where('p.client_id=pr.client_id');
			$pr_active->set_where('pr.row_id='. $row_id);
			$pr_active->set_select('pr.active');
			$pr_active->set_limit(1);
			$pages->set_select('('.$pr_active->get_compiled_select().') as pr_active');
			//$pages->set_where('(('.$pr_active->get_compiled_select().') = "1")');

		$pages->set_order('p.page_title', 'ASC');
		$this->template_data->set('row_pages', $pages->recursive('p.parent_page', 0));


		$this->template_data->set('location', 'rows');
		$this->template_data->set('actions', 'edit_rows');
		$this->template_data->set('column', 0);
		$this->template_data->set('row_id', $row_id);

		$this->template_data->set('parent_rows', $parent_data);
		$this->template_data->set('client_id', $row_data->client_id);


		$groups = new $this->Rows_groups_model;
		$groups->setClientId($this->session->userdata('current_client'),true);
		$groups->set_order('group_name', 'ASC');
		$groups->set_limit(0);
		$this->template_data->set('groups', $groups->populate());

		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_edit_row', $this->template_data->get_data());
	}

	public function deactivate_row($row_id, $output='') {
		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		$row->setActive(0,false,true);
		$row->update();
		redirect("rows");
	}

	public function delete_row($row_id, $output='') {
		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row->setActive(0,true);
		$row_data = $row->get();
		$client_id = $row_data->client_id;
		if( $row_data ) {

			$columns = new $this->Rows_columns_model;
			$columns->setRowId($row_id,true);
			$columns->delete();

			$widget = new $this->Rows_columns_widgets_model;
			$widget->setRowId($row_id,true);
			$widget->delete();

			$row->delete();
		}

		redirect("rows");

	}

	public function activate_row($row_id, $output='') {
		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		$row->setActive(1,false,true);
		$row->update();
		redirect("rows");
	}

	public function widgets($row_id, $column, $page_id='',$output='') {

		$this->template_data->set('location', 'rows');
		$this->template_data->set('row_id', $row_id);
		$this->template_data->set('column', $column);

		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		$this->template_data->set('row', $row_data );

		$column_settings = new $this->Rows_columns_model;
		$column_settings->setRowId($row_id,true);
		$column_settings->setColumn($column,true);
		if( $this->input->post() ) {
			$cs_post = $this->input->post('column_settings');
			$more_settings = $this->input->post('more_settings'); 
			$column_settings->setClass($cs_post['column_class'],false,true);
			$column_settings->setContainer($cs_post['widget_container'],false,true);
			$column_settings->setWrapper($cs_post['column_wrapper'],false,true);
			$column_settings->setMoreSettings(json_encode($more_settings),false,true);
			if( $column_settings->nonEmpty() ) {
				$column_settings->update();
			} else {
				$column_settings->setRowId($row_id,true,true);
				$column_settings->setColumn($column,true,true);
				$column_settings->insert();
			}
			if( $this->input->post('order') ) {
				foreach( $this->input->post('order') as $order=>$widget_id) {
					$widget = new $this->Rows_columns_widgets_model;
					$widget->setRowId($row_id,true,false);
					$widget->setColumn($column,true,false);
					$widget->setWidgetId($widget_id,true,false);
					$widget->setOrder($order,false,true);
					$widget->update();
				}
			}
		}

		$this->template_data->set('column_settings', $column_settings->get() );

		$this->template_data->set('client_id', $row_data->client_id);
		
		$columns = new $this->Rows_columns_model('rc');
		$columns->setRowId($row_id,true);
		$columns->set_limit(0);
		$columns->set_order('rc.column_order', 'ASC');
		$columns->set_order('rc.column', 'ASC');
		$this->template_data->set('columns', $columns->populate() );

		if( $page_id != '' ) {
			if( $page_id > 0) {
				$page = new $this->Pages_model;
				$page->setId($page_id, true);
				$this->template_data->set('page', $page->get());
			} else {
				$this->template_data->set('page', (object) array(
					'page_title' => 'Homepage',
					'client_id' => $row_data->client_id,
					'id' => $page_id
				));
			}
		}

		$widgets = new $this->Rows_columns_widgets_model;
		$widgets->setRowId($row_id,true);
		$widgets->setColumn($column,true);
		$widgets->_set_order('order', 'ASC');
		$this->template_data->set('widgets', $widgets->populate());
		
		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_widgets', $this->template_data->get_data());
	}

	public function select_widget($row_id, $column, $output='') {

		$this->template_data->set('location', 'rows');
		$this->template_data->set('row_id', $row_id);
		$this->template_data->set('column', $column);
		
		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		$this->template_data->set('row', $row_data );

		$this->template_data->set('client_id', $row_data->client_id);

		$this->template_data->set('output', $output);

		$this->load->view('rows/rows_widgets_select', $this->template_data->get_data());
	}

	public function add_widget($row_id, $column, $type='', $output='') {

		if( $this->input->post() ) { 
			$widget = new $this->Rows_columns_widgets_model;
			$widget->setRowId($row_id,true);
			$widget->setColumn($column,true);
			$widget->setType($type,true);
			$widget->setOptions( json_encode( $this->input->post() ) );
			$widget->insert();
			redirect("rows/widgets/{$row_id}/{$column}"); 
		}		

		$this->template_data->set('location', 'rows');
		$this->template_data->set('row_id', $row_id);
		$this->template_data->set('column', $column);

		$columns = new $this->Rows_columns_model('rc');
		$columns->setRowId($row_id,true);
		$columns->set_limit(0);
		$columns->set_order('rc.column_order', 'ASC');
		$columns->set_order('rc.column', 'ASC');
		$this->template_data->set('columns', $columns->populate() );

		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		$this->template_data->set('row', $row_data );

		$menus = new $this->Menus_model;
		$menus->setClientId($this->session->userdata('current_client'),true);
		$menus->set_select("*");
		$menus->set_select("(SELECT COUNT(*) FROM menus_links WHERE menus_links.menu_id=menus.id) as links_count");
		$menus->set_limit(0);
		$menus->set_order('name', 'ASC');
		$this->template_data->set('menus', $menus->populate());

		$images = new $this->Images_model;
        $images->setClientId( $this->session->userdata('current_client') );
        $this->template_data->set('images', $images->populate());

		$this->template_data->set('client_id', $this->session->userdata('current_client'));

		$this->template_data->set('output', $output);
		$this->load->view('rows/widgets/' . $type, $this->template_data->get_data());
		
	}

	public function edit_widget($widget_id, $output='') {

		$widget = new $this->Rows_columns_widgets_model;
		$widget->setWidgetId($widget_id,true);
		if( $this->input->post() ) {
			$widget->setOptions( json_encode( $this->input->post() ), false, true );
			$widget->update();
		}		

		$widget_data = $widget->get();
		$this->template_data->set('widget', $widget_data);

		$this->template_data->set('location', 'rows');
		$this->template_data->set('row_id', $widget_data->row_id);
		$this->template_data->set('column', $widget_data->column);
		
		$row = new $this->Rows_model;
		$row->setId($widget_data->row_id,true);
		$row_data = $row->get();
		$this->template_data->set('row', $row_data );

		$menus = new $this->Menus_model;
		$menus->setClientId($this->session->userdata('current_client'),true);
		$menus->set_select("*");
		$menus->set_select("(SELECT COUNT(*) FROM menus_links WHERE menus_links.menu_id=menus.id) as links_count");
		$menus->set_limit(0);
		$menus->set_order('name', 'ASC');
		$this->template_data->set('menus', $menus->populate());

		$columns = new $this->Rows_columns_model('rc');
		$columns->setRowId($widget_data->row_id,true);
		$columns->set_limit(0);
		$columns->set_order('rc.column_order', 'ASC');
		$columns->set_order('rc.column', 'ASC');
		$this->template_data->set('columns', $columns->populate() );

		$hpages = new $this->Pages_model('p');
		$hpages->setClientId($this->session->userdata('current_client'),true);
		$hpages->set_limit(0);
		$this->template_data->set('hpages', $hpages->recursive('p.parent_page', 0));

		$images = new $this->Images_model;
        $images->setClientId( $this->session->userdata('current_client') );
        $this->template_data->set('images', $images->populate());

		$this->template_data->set('output', $output);
		$this->load->view('rows/widgets/' . $widget_data->type, $this->template_data->get_data());

	}

	public function delete_column($column_id) {
		$column = new $this->Rows_columns_model;
		$column->setId($column_id,true);
		$column_data = $column->get();
		$column->delete();
		redirect("rows/edit_row/{$column_data->row_id}");
	}

	public function delete_widget($widget_id, $page_id, $output='') {
		$widget = new $this->Rows_columns_widgets_model;
		$widget->setWidgetId($widget_id,true);
		$widget_data = $widget->get();
		$widget->delete();
		redirect("rows/widgets/{$widget_data->row_id}/{$widget_data->column}/{$page_id}");
	}


	public function move_widget($widget_id, $col) {
		$widget = new $this->Rows_columns_widgets_model;
		$widget->setWidgetId($widget_id,true);
		$widget_data = $widget->get();

		$widget->setColumn($col,false,true);
		$widget->update();
		
		redirect( site_url("rows/widgets/{$widget_data->row_id}/{$col}") . "?highlight=" . $widget_id );
		//redirect("rows/edit_widget/{$widget_id}");
	}

	public function duplicate_widget($widget_id) {

		$widget = new $this->Rows_columns_widgets_model;
		$widget->setWidgetId($widget_id,true);
		$widget_data = $widget->get();

		if( $widget_data ) {
			$new_widget = new $this->Rows_columns_widgets_model;
			$new_widget->setRowId($widget_data->row_id,true);
			$new_widget->setColumn($widget_data->column,true);
			$new_widget->setType($widget_data->type,true);
			$new_widget->setOptions( $widget_data->options );
			$new_widget->insert();
		}

		redirect("rows/widgets/{$widget_data->row_id}/{$widget_data->column}");

	}

	public function add_page_row($row_id, $page_id) {
		$row = new $this->Rows_model;
		$row->setId($row_id,true);
		$row_data = $row->get();
		
		$pr = new $this->Pages_rows_model;
		$pr->setClientId($row_data->client_id);
		$pr->setPageId($page_id);
		$pr->setRowId($row_id);
		$pr->setOrder(0);
		$pr->setActive(1);
		$pr->insert();

		redirect("rows/edit_row/{$row_id}/0");
	}

	public function activate_page_row($row_id, $pr_id) {
		$pr = new $this->Pages_rows_model;
		$pr->setId($pr_id,true);
		$pr->setActive(1,false,true);
		$pr->update();

		redirect("rows/edit_row/{$row_id}/0");
	}

	public function deactivate_page_row($row_id, $pr_id) {
		$pr = new $this->Pages_rows_model;
		$pr->setId($pr_id,true);
		$pr->setActive(0,false,true);
		$pr->update();

		redirect("rows/edit_row/{$row_id}/0");
	}

	public function delete_page_row($row_id, $pr_id) {
		$pr = new $this->Pages_rows_model;
		$pr->setId($pr_id,true);
		$pr->delete();

		redirect("rows/edit_row/{$row_id}/0");
	}

	public function setSettings($row_id, $key, $value='', $delete=false) {
		$setting = new $this->Rows_settings_model;
		$setting->setRowId($row_id, true);
		$setting->setSettingKey($key, true);

		if( $delete ) {
			$setting->delete();
			return;
		}
		if( $setting->nonEmpty() ) {
			if( empty($value) || ($value=='') ) {
				$setting->delete();
			} else {
				$setting->setSettingValue($value,false,true);
				$setting->update();
			}
		} else {
			$setting->setSettingValue($value);
			$setting->insert();
		}
	}

	public function add_group($output='') {

		if( $this->input->post() ) {

			$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');
			
			if( $this->form_validation->run() ) {
				$row = new $this->Rows_groups_model;
				$row->setClientId($this->session->userdata('current_client'));
				$row->setGroupName($this->input->post('group_name'));
				$row->insert();
				redirect("rows/group/" . $row->get_inserted_id() );
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_add_group', $this->template_data->get_data());
	}

	public function edit_group($group_id, $output='') {

		$current_group = new $this->Rows_groups_model;
		$current_group->setGroupId($group_id,true);

		if( $this->input->post() ) {

			$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');
			
			if( $this->form_validation->run() ) {
				$current_group->setGroupName($this->input->post('group_name'),false,true);
				$current_group->update();

				redirect( (($this->input->get('next')) ? $this->input->get('next') : "rows" ) );
			}
		}

		$this->template_data->set('current_group', $current_group->get());

		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_edit_group', $this->template_data->get_data());
	}

	public function delete_group($group_id) {
		$current_group = new $this->Rows_groups_model;
		$current_group->setGroupId($group_id,true);
		$current_group->delete();

		redirect("rows");
		
	}

	public function assign_page($row_id, $output='') {

		$this->template_data->set('row_id', $row_id);

		$pages = new $this->Pages_model('p');
		$pages->setClientId($this->session->userdata('current_client'),true);
		$pages->set_limit(0);
		$pages->set_select('p.*');

			$pr_id = new $this->Pages_rows_model('pr');
			$pr_id->set_where('p.id=pr.page_id');
			$pr_id->set_where('p.client_id=pr.client_id');
			$pr_id->set_where('pr.row_id='. $row_id);
			$pr_id->set_select('pr.id');
			$pr_id->set_limit(1);
			//$pages->set_select('('.$pr_id->get_compiled_select().') as pr_id');
			$pages->set_where('(('.$pr_id->get_compiled_select().') IS NULL)');
/*
			$pr_active = new $this->Pages_rows_model('pr');
			$pr_active->set_where('p.id=pr.page_id');
			$pr_active->set_where('p.client_id=pr.client_id');
			$pr_active->set_where('pr.row_id='. $row_id);
			$pr_active->set_select('pr.active');
			$pr_active->set_limit(1);
			$pages->set_select('('.$pr_active->get_compiled_select().') as pr_active');
*/

		$pages->set_order('p.page_title', 'ASC');
		$this->template_data->set('row_pages', $pages->recursive('p.parent_page', 0));

		$this->template_data->set('output', $output);
		$this->load->view('rows/rows_assign_page', $this->template_data->get_data());
	}

}
