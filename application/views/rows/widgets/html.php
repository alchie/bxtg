<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
if( isset($widget) && ($widget) ) {
  $widget_options = json_decode( $widget->options );  
}
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">HTML / Text
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>

<label><input name="active" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->active))&&($widget_options->active==1)) ? 'CHECKED' : ''; ?>> Active</label>

                </h3>
            </div>
          
            <div class="panel-body">

<?php endif; ?>

<div class="form-group">
<label class="pull-right"><input name="show_title" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_title))&&($widget_options->show_title==1)) ? 'CHECKED' : ''; ?>> Show Title</label>
    <label>Title</label>
    <input type="text" class="form-control" name="title" placeholder="Title" value="<?php echo (isset($widget_options)&&isset($widget_options->title)) ? $widget_options->title : ''; ?>">
</div>

<div class="form-group">
    <label>Content</label>
    <textarea class="form-control" name="content" placeholder="Content" value="" rows="5"><?php echo (isset($widget_options)&&isset($widget_options->content)) ? $widget_options->content : ''; ?></textarea>
</div>

<div class="row">

    <div class="col-md-4">

<label class="pull-right"><input name="generate_container" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->generate_container))&&($widget_options->generate_container==1)) ? 'CHECKED' : ''; ?>> Generate Container</label>

<div class="form-group">
    <label>Container Tag</label>
    <input type="text" class="form-control" name="container_tag" placeholder="Container Class" value="<?php echo (isset($widget_options)&&isset($widget_options->container_tag)) ? $widget_options->container_tag : ''; ?>">
</div>

    </div>

    <div class="col-md-4">
<div class="form-group">
    <label>Container Class</label>
    <input type="text" class="form-control" name="container_class" placeholder="Container Class" value="<?php echo (isset($widget_options)&&isset($widget_options->container_class)) ? $widget_options->container_class : ''; ?>">
</div>

    </div>
    <div class="col-md-4">
                <div class="form-group">
                    <label>Widget ID</label>
                    <input placeholder="Widget ID" name="widget_id" type="text" class="form-control" value="<?php echo isset($widget_options->widget_id) ? $widget_options->widget_id : ''; ?>">
                </div>
    </div>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>