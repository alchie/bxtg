var blog_list_default = function(json) {
  if( json.feed.entry.length == 0 ) return;
document.write('<ul class="blog_list_default">');
for( var i in json.feed.entry ) {
  for (var j = 0; j < json.feed.entry[i].link.length; j++) {
        if (json.feed.entry[i].link[j].rel == 'alternate') {
          var postUrl = json.feed.entry[i].link[j].href;
          break;
        }
      }
document.write('<li>');
document.write('<a href="'+postUrl+'"><img src="'+json.feed.entry[i].media$thumbnail.url+'" alt=""></a>');
document.write('<h4><a href="'+postUrl+'">'+json.feed.entry[i].title.$t+'</a></h4>');
  if ( typeof json.feed.entry[i].summary != "undefined" ) {
  document.write(json.feed.entry[i].summary.$t);
  }
  if ( typeof json.feed.entry[i].content != "undefined" ) {
  document.write(json.feed.entry[i].content.$t);
  }
document.write('</li>');
}
document.write('</ul>');
};

var blog_list_sidebar = function(json) {
	if( json.feed.entry.length == 0 ) return;
document.write('<ul class="blog_list_sidebar">');
for( var i in json.feed.entry ) {
	for (var j = 0; j < json.feed.entry[i].link.length; j++) {
        if (json.feed.entry[i].link[j].rel == 'alternate') {
          var postUrl = json.feed.entry[i].link[j].href;
          break;
        }
      }
document.write('<li>');
document.write('<div>');
document.write('<h4><a href="'+postUrl+'">'+json.feed.entry[i].title.$t+'</a></h4>');
document.write('<div class="meta">');
  if ( typeof json.feed.entry[i].summary != "undefined" ) {
  document.write(json.feed.entry[i].summary.$t);
  }
  if ( typeof json.feed.entry[i].content != "undefined" ) {
  document.write(json.feed.entry[i].content.$t);
  }
document.write('</div>');
document.write('</div>');
document.write('</li>');
}
document.write('</ul>');

};

var blog_list_titles = function(json) {
  if( json.feed.entry.length == 0 ) return;
document.write('<ul class="blog_list_titles">');
for( var i in json.feed.entry ) {
  for (var j = 0; j < json.feed.entry[i].link.length; j++) {
        if (json.feed.entry[i].link[j].rel == 'alternate') {
          var postUrl = json.feed.entry[i].link[j].href;
          break;
        }
      }
document.write('<li>');
document.write('<a href="'+postUrl+'">'+json.feed.entry[i].title.$t+'</a>');
document.write('</li>');
}
document.write('</ul>');
};

var blog_list_thumbtitles = function(json) {
  if( json.feed.entry.length == 0 ) return;
document.write('<ul class="blog_list_thumbtitles">');
for( var i in json.feed.entry ) {
  for (var j = 0; j < json.feed.entry[i].link.length; j++) {
        if (json.feed.entry[i].link[j].rel == 'alternate') {
          var postUrl = json.feed.entry[i].link[j].href;
          break;
        }
      }
document.write('<li>');
document.write('<a href="'+postUrl+'"><img src="'+json.feed.entry[i].media$thumbnail.url+'" alt=""></a>');
document.write('<h4><a href="'+postUrl+'">'+json.feed.entry[i].title.$t+'</a></h4>');
document.write('</li>');
}
document.write('</ul>');
};