<?php if (isset($widget_options)&&(isset($widget_options->container_class))&&($widget_options->container_class!='')) { ?>
	<div class="<?php echo $widget_options->container_class; ?>">
<?php } ?>
<?php if (isset($widget_options)&&(isset($widget_options->show_title))&&($widget_options->show_title==1)) { ?>
	<h5><?php echo $widget_options->title; ?></h5>
<?php } ?>
<?php echo $widget_options->content; ?>
<?php if (isset($widget_options)&&(isset($widget_options->container_class))&&($widget_options->container_class!='')) { ?>
	</div>
<?php } ?>