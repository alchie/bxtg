  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingHeader">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
          Header
        </a>
      </h4>
    </div>
    <div id="collapseHeader" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHeader">
      <div class="panel-body">
        

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
          <label>Site Title</label>
          <input type="text" class="form-control" placeholder="Site Title" name="layout[header][site_title]" value="<?php echo get_layout_value('header','site_title', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-6">
         <div class="form-group">
          <label>Site URL</label>
          <input type="text" class="form-control" placeholder="Site URL" name="layout[header][site_url]" value="<?php echo get_layout_value('header','site_url', $layouts); ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
         <div class="form-group">
          <label>Site Logo Image</label>
          
          <!--<input type="text" class="form-control" placeholder="Site Logo URL" name="layout[header][site_logo_url]" value="<?php echo get_layout_value('header','site_logo_url', $layouts); ?>">-->

          <select class="form-control" name="layout[header][site_logo_image]">
              <option value="">-- No Image --</option>
              <?php foreach($images as $image) { ?>
                  <option value="<?php echo $image->id; ?>" <?php echo (get_layout_value('header','site_logo_image', $layouts)==$image->id) ? "SELECTED" : "";?>><?php echo $image->title; ?> - <?php echo $image->file_name; ?></option>
              <?php } ?>
          </select>

        </div>
    </div>
      <div class="col-md-4">
         <div class="form-group">
          <label>Site Logo Width</label>
          <input type="text" class="form-control" placeholder="Site Logo Width" name="layout[header][site_logo_width]" value="<?php echo get_layout_value('header','site_logo_width', $layouts); ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>Header Navigation</label>
          <select class="form-control" name="layout[header][header_nav]">
              <option value="">No Navigation</option>
              <?php foreach($menus as $menu) { ?>
                  <option value="<?php echo $menu->id; ?>" <?php echo (get_layout_value('header','header_nav', $layouts)==$menu->id) ? "SELECTED" : "";?>><?php echo $menu->name; ?> (<?php echo $menu->total_links; ?>)</option>
              <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Navigation Effects</label>
          <select class="form-control" name="layout[header][nav_effects]">
              <?php foreach(array(
                'static' => 'Static',
                'fixed' => 'Fixed',
                ) as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>" <?php echo (get_layout_value('header','nav_effects', $layouts)==$key) ? "SELECTED" : "";?>><?php echo $value; ?></option>
              <?php } ?>
          </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>Special Button Text</label>
          <input type="text" class="form-control" placeholder="Special Button Text" name="layout[header][special_button_text]" value="<?php echo get_layout_value('header','special_button_text', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
         <div class="form-group">
          <label>Special Button URL</label>
          <input type="text" class="form-control" placeholder="Special Button URL" name="layout[header][special_button_url]" value="<?php echo get_layout_value('header','special_button_url', $layouts); ?>">
        </div>
    </div>
</div>

      </div>
    </div>
  </div>