<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pages_containers_model Class
 *
 * Manipulates `pages_containers` table on database

CREATE TABLE `pages_containers` (
  `client_id` int(20) NOT NULL,
  `id` int(20) NOT NULL DEFAULT 0,
  `page_container` int(10) NOT NULL,
  `opt_key` varchar(100) NOT NULL,
  `opt_value` text NOT NULL,
  KEY `id` (`id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `pages_containers` ADD  `client_id` int(20) NOT NULL   ;
ALTER TABLE  `pages_containers` ADD  `id` int(20) NOT NULL   DEFAULT '0';
ALTER TABLE  `pages_containers` ADD  `page_container` int(10) NOT NULL   ;
ALTER TABLE  `pages_containers` ADD  `opt_key` varchar(100) NOT NULL   ;
ALTER TABLE  `pages_containers` ADD  `opt_value` text NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Pages_containers_model extends MY_Model {

	protected $client_id;
	protected $id;
	protected $page_container;
	protected $opt_key;
	protected $opt_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'pages_containers';
		$this->_short_name = 'pages_containers';
		$this->_fields = array("client_id","id","page_container","opt_key","opt_value");
		$this->_required = array("client_id","id","page_container","opt_key","opt_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: client_id -------------------------------------- 

	/** 
	* Sets a value to `client_id` variable
	* @access public
	*/

	public function setClientId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_client_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('client_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `client_id` variable
	* @access public
	*/

	public function getClientId() {
		return $this->client_id;
	}

	public function get_client_id() {
		return $this->client_id;
	}

	
// ------------------------------ End Field: client_id --------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: page_container -------------------------------------- 

	/** 
	* Sets a value to `page_container` variable
	* @access public
	*/

	public function setPageContainer($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_page_container($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('page_container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `page_container` variable
	* @access public
	*/

	public function getPageContainer() {
		return $this->page_container;
	}

	public function get_page_container() {
		return $this->page_container;
	}

	
// ------------------------------ End Field: page_container --------------------------------------


// ---------------------------- Start Field: opt_key -------------------------------------- 

	/** 
	* Sets a value to `opt_key` variable
	* @access public
	*/

	public function setOptKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('opt_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_opt_key($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('opt_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `opt_key` variable
	* @access public
	*/

	public function getOptKey() {
		return $this->opt_key;
	}

	public function get_opt_key() {
		return $this->opt_key;
	}

	
// ------------------------------ End Field: opt_key --------------------------------------


// ---------------------------- Start Field: opt_value -------------------------------------- 

	/** 
	* Sets a value to `opt_value` variable
	* @access public
	*/

	public function setOptValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('opt_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_opt_value($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('opt_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `opt_value` variable
	* @access public
	*/

	public function getOptValue() {
		return $this->opt_value;
	}

	public function get_opt_value() {
		return $this->opt_value;
	}

	
// ------------------------------ End Field: opt_value --------------------------------------



	
	public function get_table_options() {
		return array(
			'client_id' => (object) array(
										'Field'=>'client_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'page_container' => (object) array(
										'Field'=>'page_container',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'opt_key' => (object) array(
										'Field'=>'opt_key',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'opt_value' => (object) array(
										'Field'=>'opt_value',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'client_id' => "ALTER TABLE  `pages_containers` ADD  `client_id` int(20) NOT NULL   ;",
			'id' => "ALTER TABLE  `pages_containers` ADD  `id` int(20) NOT NULL   DEFAULT '0';",
			'page_container' => "ALTER TABLE  `pages_containers` ADD  `page_container` int(10) NOT NULL   ;",
			'opt_key' => "ALTER TABLE  `pages_containers` ADD  `opt_key` varchar(100) NOT NULL   ;",
			'opt_value' => "ALTER TABLE  `pages_containers` ADD  `opt_value` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setClientId() - client_id
//setId() - id
//setPageContainer() - page_container
//setOptKey() - opt_key
//setOptValue() - opt_value

--------------------------------------

//set_client_id() - client_id
//set_id() - id
//set_page_container() - page_container
//set_opt_key() - opt_key
//set_opt_value() - opt_value

*/
/* End of file Pages_containers_model.php */
/* Location: ./application/models/Pages_containers_model.php */
