<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
            <form method="post">
    <div class="col-md-6">
        <div class="panel panel-default">

            <div class="panel-heading">
                <button type="submit" class="btn btn-success btn-xs pull-right">Save Changes</button>
<?php if($row->active==0) { ?>
    <a href="<?php echo site_url("rows/delete_row/{$row->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm" style="margin-right: 5px;">Delete</a>
<?php } ?>

                <h3 class="panel-title">Edit Row</h3>
            </div>
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

    
            <div class="form-group">
             <label class="pull-right"><input name="active" type="checkbox" value="1" <?php echo ($row->active==1) ? 'CHECKED' : ''; ?>> Active</label>
                <label>Row Title</label>
                <input name="row_title" type="text" class="form-control" value="<?php echo $row->title; ?>">
            </div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label>Row Columns</label>
                <select name="columns" class="form-control" title="- - Number of Columns - -">
                <?php for($i=1;$i<=12;$i++) { ?>
                  <option value="<?php echo $i; ?>" <?php echo ($row->columns==$i) ? 'SELECTED' : ''; ?>><?php echo $i; ?></option>
                <?php } ?>
                </select>
              </div>
    </div>
    <div class="col-md-6">
<div class="form-group">
                <label>Additional Row Class</label>
                <input name="row_class" type="text" class="form-control" value="<?php echo $row->row_class; ?>">
            </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6">
                <div class="form-group">
                <label><input name="display_rows" type="checkbox" value="1" <?php echo ((isset($row->display_rows))&&($row->display_rows==1)) ? 'CHECKED' : ''; ?>> Display Rows</label>
<br>
                    <label><input name="container" type="checkbox" value="1" <?php echo ((isset($row->container))&&($row->container==1)) ? 'CHECKED' : ''; ?>> Add Container</label>
 <br>
                    <label><input name="row_settings[display_title]" type="checkbox" value="1" <?php echo ((isset($row->display_title))&&($row->display_title==1)) ? 'CHECKED' : ''; ?>> Display Title</label>
<br>
<label><input name="row_settings[display_column_settings]" type="checkbox" value="1" <?php echo ((isset($row->display_column_settings))&&($row->display_column_settings==1)) ? 'CHECKED' : ''; ?>> Display Column Settings</label>

<br>
<label><input name="row_settings[generate_row_container]" type="checkbox" value="1" <?php echo ((isset($row->generate_row_container))&&($row->generate_row_container==1)) ? 'CHECKED' : ''; ?>> Generate Row Container</label>
<br>
<label><input name="row_settings[remove_row_class]" type="checkbox" value="1" <?php echo ((isset($row->remove_row_class))&&($row->remove_row_class==1)) ? 'CHECKED' : ''; ?>> Remove Row Class</label>
                </div>
</div>
<div class="col-md-6">
<div class="form-group">
                <label>Additional Container Class</label>
                <input name="container_class" type="text" class="form-control" value="<?php echo $row->container_class; ?>">
            </div>
</div></div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label>Parent Row</label>
                <select name="parent_row" class="form-control" title="- - Select Parent Row - -">
                    <option value="0">- - Parent Row - -</option>
                <?php 
function display_options($parent_rows, $parent_row_selected, $prep="") {
                foreach( $parent_rows as $pRow) {
                    ?>
                  <option value="<?php echo $pRow->id; ?>" <?php echo ($parent_row_selected==$pRow->id) ? 'SELECTED' : ''; ?>><?php echo $prep; ?><?php echo $pRow->title; ?></option>
                   <?php if( $pRow->children ) { display_options($pRow->children, $parent_row_selected, $prep . "- - "); } ?>
                <?php } ?>
               
<?php } 
display_options($parent_rows, $row->parent_row);
?>

                </select>
              </div>
    </div>
    <div class="col-md-6">
<div class="form-group">
                <label>Wrapper Class</label>
                <input name="wrapper_class" type="text" class="form-control" value="<?php echo $row->wrapper_class; ?>">
            </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            <div class="form-group">
                <label>Group</label>
                <?php  $selected = $row->group_id; ?>
                <select name="group_id" class="form-control" title="- - Row Group - -">
                     <option value="" <?php echo ($selected==false) ? 'SELECTED' : ''; ?>>- - None - -</option>
                <?php 

                foreach($groups as $group) { ?>
                    ?>
                  <option value="<?php echo $group->group_id; ?>" <?php echo ($selected==$group->group_id) ? 'SELECTED' : ''; ?>><?php echo $group->group_name; ?></option>
                <?php } ?>
                </select>
              </div>
    </div>
    <div class="col-md-6">
<label>Blogger Settings</label><br>
<label><input name="row_settings[blogger_main_includable]" type="checkbox" value="1" <?php echo ((isset($row->blogger_main_includable))&&($row->blogger_main_includable==1)) ? 'CHECKED' : ''; ?>> Set as Blogger Main / Item Page</label>
<?php if((isset($row->blogger_main_includable))&&($row->blogger_main_includable==1)) { ?>
    <div class="form-group">
                <label>Blogger Post Name</label>
                <input name="row_settings[blogger_post_name]" type="text" class="form-control" value="<?php echo $row->blogger_post_name; ?>">
    </div>

    <div class="form-group">
                <label>Post Row Class</label>
                <input name="row_settings[blogger_post_row_class]" type="text" class="form-control" value="<?php echo @$row->blogger_post_row_class; ?>">
    </div>
<?php } ?>
    </div>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Save Changes</button>
<?php if( $this->input->get('next') ) { ?>
                <a href="<?php echo site_url( (($this->input->get('next')) ? $this->input->get('next') : "rows" ) ); ?>" class="btn btn-warning">Back</a>
<?php } ?>
            </div>

        </div>

<?php if( isset($columns) && ($columns) && (count($columns) > 1) ) { ?>

<div class="panel panel-default">
            <div class="panel-heading">
            <button type="submit" class="btn btn-success btn-xs pull-right">Save</button>
                <h3 class="panel-title">Columns Order</h3>
            </div>
            <div class="panel-body">

            <ul class="list-group sortable">
                <?php foreach($columns as $col) { ?>
                     <li class="list-group-item">
<span class="badge"><?php echo $col->widgets_count; ?></span>
<span class="glyphicon glyphicon-sort"></span>
                     Column <?php echo $col->column; ?>
                         <input type="hidden" name="column_order[]" value="<?php echo $col->id; ?>">
                     </li>
                <?php } ?>
            </ul>

            </div>
</div>

<?php } ?>


</div>

 <div class="col-md-6">
     <?php if((isset($row->blogger_main_includable))&&($row->blogger_main_includable==1)) { ?>

    <div class="panel panel-default">
            <div class="panel-heading">
            <button type="submit" class="btn btn-success btn-xs pull-right">Save</button>
                <h3 class="panel-title">Blog Posts List Settings</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                         <label><input name="row_settings[blogger_postlist_textbox]" type="checkbox" value="1" <?php echo ((isset($row->blogger_postlist_textbox))&&($row->blogger_postlist_textbox==1)) ? 'CHECKED' : ''; ?>>Show Text Box</label><br>
                        <label><input name="row_settings[blogger_postlist_textbox_time]" type="checkbox" value="1" <?php echo ((isset($row->blogger_postlist_textbox_time))&&($row->blogger_postlist_textbox_time==1)) ? 'CHECKED' : ''; ?>>Time</label><br>
                        <label><input name="row_settings[blogger_postlist_textbox_title]" type="checkbox" value="1" <?php echo ((isset($row->blogger_postlist_textbox_title))&&($row->blogger_postlist_textbox_title==1)) ? 'CHECKED' : ''; ?>>Title</label><br>
                        <label><input name="row_settings[blogger_postlist_textbox_snippet]" type="checkbox" value="1" <?php echo ((isset($row->blogger_postlist_textbox_snippet))&&($row->blogger_postlist_textbox_snippet==1)) ? 'CHECKED' : ''; ?>>Snippet</label>
                    </div>
                    <div class="col-md-6"></div>
                </div>

            </div>
    </div>

<?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                 <a data-title="Assign a Page" data-hide_footer="1" href="<?php echo site_url("rows/assign_page/{$row->id}/ajax"); ?>" class="btn btn-success btn-xs pull-right ajax-modal"><span class="glyphicon glyphicon-plus"></span> Assign a Page</a>

                <h3 class="panel-title">Page Assignments</h3>
            </div>
            <div class="panel-body">
                
<ul class="list-group">
<?php function display_li($page, $row, $pre="") { ?>
                    <li class="list-group-item"><?php echo $pre; ?>
<?php if( is_null($page->pr_id) ) { ?>
    <a href="<?php echo site_url("rows/add_page_row/{$row->id}/{$page->id}"); ?>" class="btn btn-success btn-xs pull-right">Add</a>
<?php } else { ?>
    <?php if( $page->pr_active == 0 ) { ?>
        <div class="btn-group pull-right" role="group" aria-label="...">
            <a href="<?php echo site_url("rows/activate_page_row/{$row->id}/{$page->pr_id}"); ?>" class="btn btn-success btn-xs">Activate</a>
            <a href="<?php echo site_url("rows/delete_page_row/{$row->id}/{$page->pr_id}"); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
        </div>
    <?php } else { ?>
        <a href="<?php echo site_url("rows/deactivate_page_row/{$row->id}/{$page->pr_id}"); ?>" class="btn btn-warning btn-xs pull-right confirm">Deactivate</a>
    <?php } ?>
<?php } ?>
                    <a href="<?php echo site_url("pages/rows/{$page->client_id}/{$page->id}"); ?>"><?php echo $page->page_title; ?></a>
                    </li>

                   <?php if( $page->children ) foreach($page->children as $page1) { display_li($page1, $row, '- - - - '); } ?> 

<?php } ?>
<?php if( $row_pages ) foreach($row_pages as $page) { display_li($page, $row); } ?>
</ul>

            </div>
        </div>


 </div>

</form>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>