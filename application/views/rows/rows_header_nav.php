<div class="container" style="margin-bottom:10px;">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
<?php 
$page_id = '';

if( isset($page) && ($page) ) { 
  $page_id = (isset($page->id)) ? $page->id : '';
?>
              <li role="presentation" class="<?php echo ($location=='widgets')? 'active' : ''; ?>"><a href="<?php echo site_url("pages/rows/{$page->client_id}/{$page->id}"); ?>"><?php echo $page->page_title; ?></a></li>
<?php } else { ?>
              <li role="presentation" class="<?php echo ($location=='widgets')? 'active' : ''; ?>"><a href="<?php echo site_url("rows"); ?>">Rows</a></li>
<?php } ?>              
<?php if( $location == 'rows' ) { ?>
                <li role="presentation" class="<?php echo ((isset($actions)) && ($actions=='edit_rows')) ? 'active' : ''; ?>"><a href="<?php echo site_url("rows/edit_row/{$row_id}/{$page_id}"); ?>">Edit Row <?php echo (isset($row)) ? " : <strong>" . $row->title . "</strong>" : ""; ?></a></li>
		<?php 
$columns_count = 0;
    foreach($columns as $col) { 
      ?>
              <li role="presentation" class="<?php echo ((!isset($widget)) && ($col->column==$column)) ? 'active' : ''; ?>"><a href="<?php echo site_url("rows/widgets/{$col->row_id}/{$col->column}/{$page_id}"); ?>">Column <?php echo $col->column; ?></a></li>
		<?php $columns_count++;
    } ?>



<?php if( isset($widget) ) { ?>
  <li role="presentation" class="active"><a href="javascript:void(0);">Edit Widget</a></li>
<?php } ?>

<?php if( count($columns) < $row->columns ) { ?>
  
    <li role="presentation" class="pull-right"><a href="<?php echo site_url("rows/widgets/{$row_id}/".($column+1)."/{$page_id}"); ?>"><span class="glyphicon glyphicon-plus"></span></a></li>
<?php } ?>

<?php } ?>
            </ul>
        </div>
    </div>
</div>