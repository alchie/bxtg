<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Link</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

                <div class="form-group">
                    <label>Link Text</label>
                    <textarea name="link_text" class="form-control" REQUIRED><?php echo $this->input->post('link_text'); ?></textarea>
                </div>
<div class="row">
    <div class="col-md-6">
                <div class="form-group">
                    <label>Link URL</label>
                    <input name="link_url" type="text" class="form-control" value="#" required>
                </div>
</div>
    <div class="col-md-6">
                <div class="form-group">
                    <label>Icon</label>
                    <input name="link_icon" type="text" class="form-control" value="<?php echo $this->input->post('link_icon'); ?>">
                </div>
</div></div>

<div class="row">
 <div class="col-md-6">
                <div class="form-group">
                    <label>Inner Page</label>
                    <select name="inner_page" class="form-control">
                        <option value="0">No Inner Page</option>
                        <?php 
                        foreach($pages as $page) {  ?>
                            <option value="<?php echo $page->id; ?>"><?php echo $page->page_title; ?> (<?php echo $page->page_slug; ?>)</option>

                            <?php if(  $page->children ) foreach( $page->children as $subpage ) { ?>
                            <option value="<?php echo $subpage->id; ?>">- - <?php echo $subpage->page_title; ?> (<?php echo $subpage->page_slug; ?>)</option>
                            <?php } ?>

                        <?php } ?>
                    </select>
                </div>

</div>
    <div class="col-md-6">
 <div class="form-group">
                    <label>Order</label>
                    <input name="order" type="text" class="form-control" value="0">
                </div>
</div></div>

<div class="row">
    <div class="col-md-6">
<?php if($parent_links) { ?>
                <div class="form-group">
                    <label>Parent Link</label>
                    <select name="parent_id" class="form-control">
                        <option value="">No Parent</option>
                        <?php 
function display_options($parent_links, $pre="") {
                        foreach($parent_links as $plink) { ?>
                            <option value="<?php echo $plink->id; ?>"><?php echo $pre . $plink->text; ?> (<?php echo $plink->url; ?>)</option>
                        <?php 
if( $plink->children ) {
    display_options($plink->children, $pre . "- - - ");
}
                    } 

}
display_options($parent_links);
                        ?>
                    </select>
                </div>
<?php } ?>

</div>
   
    <div class="col-md-6">
 <div class="form-group">
                    <label>Target</label>
                    <input name="target" type="text" class="form-control" value="">
                </div>

</div></div>
<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("menus/links/{$link->menu_id}"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>