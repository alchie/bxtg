<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

if( isset($widget) && ($widget) ) {
  $widget_options = json_decode( $widget->options );  
}
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">Menu
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>

<label><input name="active" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->active))&&($widget_options->active==1)) ? 'CHECKED' : ''; ?>> Active</label>
                </h3>

            </div>
          
            <div class="panel-body">

<?php endif; ?>



<div class="form-group">
<label class="pull-right"><input name="show_title" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_title))&&($widget_options->show_title==1)) ? 'CHECKED' : ''; ?>> Show Title</label>
    <label>Title</label>
    <textarea class="form-control" name="title" placeholder="Title"><?php echo (isset($widget_options)&&(isset($widget_options->title))) ? $widget_options->title : ''; ?></textarea>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">

    <div class="pull-right">
  <label><input name="show_icon" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_icon))&&($widget_options->show_icon==1)) ? 'CHECKED' : ''; ?>> Show Icon</label>

  <label><input name="show_text" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_text))&&($widget_options->show_text==1)) ? 'CHECKED' : ''; ?>> Show Text</label>

</div>
        <label for="menu_id">Menu (Left)</label>
        <select name="menu_id" class="form-control" id="menu_id">
          <option value="">None</option>
            <?php 
            foreach($menus as $menu) { ?>
                <option value="<?php echo $menu->id; ?>" <?php echo (isset($widget_options) && ($menu->id==$widget_options->menu_id)) ? 'SELECTED' : ''; ?>><?php echo $menu->name; ?> (<?php echo $menu->links_count; ?>)</option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">

    <div class="pull-right">
  <label><input name="show_icon_right" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_icon_right))&&($widget_options->show_icon_right==1)) ? 'CHECKED' : ''; ?>> Show Icon</label>

  <label><input name="show_text_right" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_text_right))&&($widget_options->show_text_right==1)) ? 'CHECKED' : ''; ?>> Show Text</label>

</div>
        <label for="menu_id">Menu (Right)</label>
        <select name="menu_id_right" class="form-control" id="menu_id_right">
          <option value="">None</option>
            <?php 
            foreach($menus as $menu) { ?>
                <option value="<?php echo $menu->id; ?>" <?php echo (isset($widget_options) && ($menu->id==$widget_options->menu_id_right)) ? 'SELECTED' : ''; ?>><?php echo $menu->name; ?> (<?php echo $menu->links_count; ?>)</option>
            <?php } ?>
        </select>
    </div>
</div>

</div>
<div class="row">
  <div class="col-md-6">
<div class="form-group">
  <label class="pull-right"><input name="show_list" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_list))&&($widget_options->show_list==1)) ? 'CHECKED' : ''; ?>> Show as List</label>
    <label>List Class</label>
    <input type="text" class="form-control" name="list_class" placeholder="List Class" value="<?php echo (isset($widget_options)&&(isset($widget_options->list_class))) ? $widget_options->list_class : ''; ?>">
</div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
        <label>List Item Class</label>
        <input type="text" class="form-control" name="list_item_class" placeholder="List Class" value="<?php echo (isset($widget_options)&&(isset($widget_options->list_item_class))) ? $widget_options->list_item_class : ''; ?>">
    </div>
  </div>
</div>

<div class="row">

   <div class="col-md-4">
<div class="form-group">

    <label>Widget ID</label>
    <input type="text" class="form-control" name="widget_id" placeholder="Widget ID" value="<?php echo (isset($widget_options)&&isset($widget_options->widget_id)) ? $widget_options->widget_id : ''; ?>">
</div>
  </div>

  <div class="col-md-4">
<div class="form-group">

<label class="pull-right"><input name="generate_container" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->generate_container))&&($widget_options->generate_container==1)) ? 'CHECKED' : ''; ?>> Generate Container</label>
   

    <label>Container Class</label>
    <input type="text" class="form-control" name="container_class" placeholder="Container Class" value="<?php echo (isset($widget_options)&&isset($widget_options->container_class)) ? $widget_options->container_class : ''; ?>">
</div>
  </div>



  <div class="col-md-4">
<div class="form-group">
    <label>Menu Type</label>
    <select name="menu_type" class="form-control" id="menu_type">
            <?php 
            $widget_menu_type = (isset($widget_options->menu_type)) ? $widget_options->menu_type : '';
            foreach(array('bootstrap') as $menu_type) { ?>
                <option value="<?php echo $menu_type; ?>" <?php echo (isset($widget_options) && ($menu_type==$widget_menu_type)) ? 'SELECTED' : ''; ?>><?php echo $menu_type; ?></option>
            <?php } ?>
        </select>
</div>
  </div>
</div>

<div class="row">

   <div class="col-md-4">
<div class="form-group">

    <label>Nav ID</label>
    <input type="text" class="form-control" name="nav_id" placeholder="Nav ID" value="<?php echo (isset($widget_options)&&isset($widget_options->nav_id)) ? $widget_options->nav_id : ''; ?>">
</div>
  </div>

  <div class="col-md-4">
<div class="form-group">  

    <label>Nav Class</label>
    <input type="text" class="form-control" name="nav_class" placeholder="Nav Class" value="<?php echo (isset($widget_options)&&isset($widget_options->nav_class)) ? $widget_options->nav_class : ''; ?>">
</div>
  </div>
  <div class="col-md-4">
    <label class="pull-right"><input name="search_box" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->search_box))&&($widget_options->search_box==1)) ? 'CHECKED' : ''; ?>> Show Search Box</label>
  </div>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>