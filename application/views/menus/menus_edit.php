<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Menu</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif;  ?>

                <div class="form-group">
                    <label>Menu Name</label>
                    <input name="menu_name" type="text" class="form-control" value="<?php echo $menu->name; ?>">
                </div>

                <div class="form-group">
                    <label><input name="home_link" type="checkbox" value="1" <?php echo (isset($is_home_link) && ($is_home_link==1)) ? 'CHECKED' : ''; ?>> Show Home Link</label>
                    
                </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("menus/index/{$current_client->id}"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>