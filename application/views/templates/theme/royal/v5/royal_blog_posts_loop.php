<div id="<?php echo ((isset($widget_options->widget_id))&&($widget_options->widget_id!='')) ? $widget_options->widget_id : 'widget_'. $widget_id; ?>">
<?php if (isset($widget_options)&&(isset($widget_options->show_wrapper))&&($widget_options->show_wrapper==1)) { ?>
	<div class="<?php echo $widget_options->wrapper_class; ?>">
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->show_container))&&($widget_options->show_container==1)) { ?>
	<div class="<?php echo $widget_options->container_class; ?>">
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->show_row))&&($widget_options->show_row==1)) { ?>
	<div class="<?php echo $widget_options->row_class; ?>">
<?php } ?>

<b:loop values='data:posts' var='post'>

 <?php if (isset($widget_options)&&(isset($widget_options->show_column))&&($widget_options->show_column==1)) { ?>
	<div class="<?php echo $widget_options->column_class; ?>">
<?php } ?>

<?php switch($widget_options->loop_template) { ?>
<?php default: ?>
<div class="aboutImage">
      <a expr:href="data:post.url">
        <img expr:src="data:post.firstImageUrl" alt="" class="img-responsive"/>
        <div class="overlay">
          <p><data:post.snippet/></p>
        </div>
        <span class="captionLink"><data:post.title/><span></span></span>
      </a>
</div>
<?php break; ?>
<?php } ?>
 <?php if (isset($widget_options)&&(isset($widget_options->show_column))&&($widget_options->show_column==1)) { ?>
	</div>
<?php } ?>

</b:loop>

<?php if (isset($widget_options)&&(isset($widget_options->show_row))&&($widget_options->show_row==1)) { ?>
	</div>
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->show_container))&&($widget_options->show_container==1)) { ?>
	</div>
<?php } ?>



<?php if (isset($widget_options)&&(isset($widget_options->show_wrapper))&&($widget_options->show_wrapper==1)) { ?>
	</div>
<?php } ?>
</div>