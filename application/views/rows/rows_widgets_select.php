<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
 <form method="post">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Select a Widget Type</h3>
            </div>
           
            

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

    <div class="list-group">

<?php $widget_types = unserialize( WIDGET_TYPES ); ?>
<?php foreach($widget_types as $key=>$type) { 
    $yes_ajax = 'ajax';
    $yes_ajax_class = 'ajax-modal-inner';
if( in_array($key, array('image_slider', 'picture', 'list') ) ) {
    $yes_ajax = '';
    $yes_ajax_class = '';
}
    ?>
    <a data-title="<?php echo $type; ?>" href="<?php echo site_url("rows/add_widget/{$row->id}/{$column}/{$key}/{$yes_ajax}"); ?>" class="list-group-item <?php echo $yes_ajax_class; ?>"><?php echo $type; ?></a>
<?php } ?>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            
        </div>
    </div>
    </form>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>