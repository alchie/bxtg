  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingTopNav">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTopNav" aria-expanded="true" aria-controls="collapseTopNav">
          Top Nav
        </a>
      </h4>
    </div>
    <div id="collapseTopNav" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTopNav">
      <div class="panel-body">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>Email Address</label>
          <input type="email" class="form-control" placeholder="Email" name="layout[top_nav][email_address]" value="<?php echo get_layout_value('top_nav','email_address', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Phone Number</label>
          <input type="text" class="form-control" placeholder="Phone Number" name="layout[top_nav][phone_number]" value="<?php echo get_layout_value('top_nav','phone_number', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Social Media</label>
          <select class="form-control" name="layout[top_nav][social_media]">
              <option value="">No Social Media</option>
              <?php foreach($menus as $menu) { ?>
                  <option value="<?php echo $menu->id; ?>" <?php echo (get_layout_value('top_nav','social_media', $layouts)==$menu->id) ? "SELECTED" : "";?>><?php echo $menu->name; ?> (<?php echo $menu->total_links; ?>)</option>
              <?php } ?>
          </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
          <label>Search URL</label>
          <input type="text" class="form-control" placeholder="Search URL" name="layout[top_nav][search_url]" value="<?php echo get_layout_value('top_nav','search_url', $layouts); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          <label>Top Nav</label>
          <select class="form-control" name="layout[top_nav][top_nav]">
              <option value="">No Social Media</option>
              <?php foreach($menus as $menu) { ?>
                  <option value="<?php echo $menu->id; ?>" <?php echo (get_layout_value('top_nav','top_nav', $layouts)==$menu->id) ? "SELECTED" : "";?>><?php echo $menu->name; ?> (<?php echo $menu->total_links; ?>)</option>
              <?php } ?>
          </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
<label><input type="radio" name="layout[top_nav][status]" value="1" <?php echo (get_layout_value('top_nav','status', $layouts)==1) ? "CHECKED" : "";?>> Enabled</label><br>
<label><input type="radio" name="layout[top_nav][status]" value="0" <?php echo (get_layout_value('top_nav','status', $layouts)==0) ? "CHECKED" : "";?>> Disabled</label>
        </div>
    </div>
</div>



      </div>
    </div>
  </div>