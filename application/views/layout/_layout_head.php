  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingHead">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHead" aria-expanded="true" aria-controls="collapseHead">
          Custom Head Tags
        </a>
      </h4>
    </div>
    <div id="collapseHead" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHead">
      <div class="panel-body">

        <div class="form-group">
          <textarea class="form-control" name="layout[head][head]" rows="10"><?php echo htmlentities(get_layout_value('head','head', $layouts)); ?></textarea>
        </div>
    
      </div>
    </div>
  </div>