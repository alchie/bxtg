(function($){

var modal_backlink = [];
var current_uri = '';

 var init_sortable = function() {
    $( ".sortable" ).sortable();
    $( ".sortable" ).disableSelection();
 };

var init_datepicker = function() {
  $('.datepicker').datepicker();
  $('select').selectpicker({
    liveSearch : true,
  });
};


var confirmButton = function() {
  $('.confirm').click(function(){
  	if(confirm('Are you sure?')) {
  		return true;
  	} else {
  		return false;
  	}
  });
};

var confirmRemove = function() {
  $('.confirm_remove').each(function(){
      var href = $(this).prop('href');
      var data_url = $(this).attr('data-url');
      var data_morph = $(this).attr('data-morph');
      var target = $(this).attr('data-target');
      if( data_morph != 'confirmRemove') {
        $(this).attr('data-url', href);
        $(this).prop('href', 'javascript:void(0);');
        $(this).attr('data-morph', 'confirmRemove');
        $(this).click(function(){
           var divBody = $('#bodyWrapper');
            var loadingDiv = $('<div class="loading-wait"></div>');
            loadingDiv.css('height', ( divBody.parent().height() - 43 ) );
            var loadingImg = $('<img src="/assets/images/loader4.gif"/>');
            loadingImg.css('margin-top', Math.ceil( divBody.parent().height() / 2 ));
            loadingDiv.html( loadingImg );
            divBody.prepend( loadingDiv );
            var ajax_url = $(this).attr('data-url');
          if(confirm('Are you sure?')) {
              $.ajax({
                url: ajax_url,
                method: 'GET',
                success: function( data ) {
                      $(target).remove();
                }
              }); // $.ajax()
          }
          loadingDiv.remove();
        });
      }
  });
};

var addDeposits = function() {
  $('.add_deposits').click(function(){
  	$('#addDepositModal-title').text($(this).attr('data-title'));
  	$('#addDepositModal-bankid').val($(this).attr('data-bankid'));	
  	$('#addDepositModal-amount').val($(this).text().replace(/,/g,'').trim());	
  });
  $('.add_disbursement').click(function(){
  	$('#addDisbursementModal-title').text($(this).attr('data-title'));
  	$('#addDisbursementModal-bankid').val($(this).attr('data-bankid'));
  	$('#addDisbursementModal-amount').val($(this).text().replace(/,/g,'').trim());	
  });
  $('#addDepositModal').on('shown.bs.modal', function () {
    $('#addDepositModal-amount').focus().select();
  });
  $('#addDisbursementModal').on('shown.bs.modal', function () {
    $('#addDisbursementModal-amount').focus().select();
  });
};

var hoverPop = function() {
  $('.hoverPopBottom').popover({
    trigger : 'hover',
    placement : 'bottom',
    html : true,
  });

   $('.hoverPopRight').popover({
    trigger : 'hover',
    placement : 'right',
    html : true,
  });
};

var select_account_titles = function() {
  $('select.select_account_titles').click(function(){
    var at = JSON.parse(account_titles);
    var html = '<option value="">- - Account Title - -</option>';
    var set_options = function(at,p) {
       for(a in at) {
        html += '<option value="'+at[a].id+'">'+p+" "+at[a].title+'</option>';
        if( at[a].children.length > 0) {
          set_options(at[a].children, p + " - -");
        }
       }
    };
    set_options(at,'');
    //$(this).html(html);
  });
};

var autocomplete_navbarsearch = function() {
  $('.autocomplete-navbarsearch').autocomplete({
      source: function( request, response ) {
        var ths = $(this);
        var source = ths[0]['element'][0]['dataset'].source;
        $.ajax({
          url: source,
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
        var redirect = event['target']['dataset'].redirect;
        if( redirect == '1') {
          window.location.href=ui.item.redirect;
        }
      }
    });
};

var navbar_search_employee = function() {
  $('.autocomplete-search_employee').autocomplete({
      source: function( request, response ) {
        var ths = $(this);
        var source = ths[0]['element'][0]['dataset'].source;
        var current_sub_uri = ths[0]['element'][0]['dataset'].current_sub_uri;
        $.ajax({
          url: source,
          dataType: "json",
          data: {
            term: request.term,
            sub_uri : current_sub_uri
          },
          success: function( data ) {
            response( data );
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
          //window.location.href=ui.item.redirect;
              var divBody = $('#bodyWrapper');
              var loadingDiv = $('<div class="loading-wait"></div>');
              loadingDiv.css('height', ( divBody.parent().height() - 43 ) );
              var loadingImg = $('<img src="/assets/images/loader4.gif"/>');
              loadingImg.css('margin-top', Math.ceil( divBody.parent().height() / 2 ));
              loadingDiv.html( loadingImg );
              divBody.prepend( loadingDiv );
              $.ajax({
                url: ui.item.redirect,
                method: 'POST',
                data: {
                  output: 'body_wrapper',
                },
                success: function( data ) {
                    divBody.slideUp( function(){
                      $(this).html( data );
                      $(this).slideDown(function(){
                        loadingDiv.remove();
                        window.history.replaceState('Object', $(document).prop('title'), ui.item.redirect);
                        $('.autocomplete-member_change').val('');
                        init_coop();
                      });
                    });
                }
              }); // $.ajax()
          
      }
    });

var autocomplete_name_select_change_name = function(){
    var name_id = $(this).attr('data-name_id');
    var timestamp = $(this).attr('data-timestamp');
    $('#'+name_id).val('');
    $('.autocomplete-name_select-name-display-'+ timestamp).remove();
    $('.autocomplete-name_select-name-input-'+ timestamp).val('').show().focus().removeClass( 'autocomplete-name_select-name-input-'+ timestamp );
  };

  $('#changeName').click(autocomplete_name_select_change_name);

  $('.changeName').click(autocomplete_name_select_change_name);

};

var autocomplete_name_select_options = {
      source: function( request, response ) {
        var ths = $(this);
        var source = ths[0]['element'][0]['dataset'].source;
        $.ajax({
          url: source,
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
        var name_id = event['target']['dataset'].name_id;
        var full_name = event['target']['dataset'].full_name;
        var target = $(event['target']);
        var name = $('<div/>').addClass('form-control');
        var link = $('<a/>').addClass('badge');
        var timestamp = $.now();

        target.addClass('autocomplete-name_select-name-input-' + timestamp );
        
        name.html( link.text( ui.item.label ) );
        name.addClass('autocomplete-name_select-name-display-' + timestamp );
        name.insertAfter( target );

        link.prop('href', '#changeName');
        link.attr('data-id', ui.item.id );
        link.attr('data-name_id', name_id );
        link.attr('data-timestamp', timestamp );

          link.click(function(){
            var name_id = $(this).attr('data-name_id');
            var timestamp = $(this).attr('data-timestamp');
            $('#'+name_id).val('');
            $('.autocomplete-name_select-name-display-'+ timestamp).remove();
            $('.autocomplete-name_select-name-input-'+ timestamp).val('').show().focus().removeClass( 'autocomplete-name_select-name-input-'+ timestamp );
          });
        name.prepend( link );

        target.hide();
        $('#'+name_id).val(ui.item.id);
        $('#'+full_name).val(ui.item.label);
      }
    };

  $('.autocomplete-name_select').autocomplete( autocomplete_name_select_options );

  $('#split_bank_entry').click(function(){ 
    var timestamp = $.now();
    var id = $(this).attr('data-id');
    var link = $('<a/>').addClass('pull-right');
        link.prop('href', '#removeThisSplit');
        link.attr('data-timestamp', timestamp );
        link.html('<span class="glyphicon glyphicon-remove"></span>');
        link.click(function(){
          var timestamp = $(this).attr('data-timestamp');
          $('.duplicate_bank_entry_'+timestamp).remove();
        });
    $('#'+id).after( "<tr class='duplicate_bank_entry_"+timestamp+"'>" + $('#'+id).html() + "</tr>" );
    $('.duplicate_bank_entry_'+timestamp+' .split_bank_entry').after( link );
    $('.duplicate_bank_entry_'+timestamp+' .split_bank_entry').remove();
    $('.duplicate_bank_entry_'+timestamp+' td.class_names').html( "<select type=\"text\" class=\"form-control class_names\" name=\"class_name[]\" title=\"Select a Class...\">" + $('#bank_entry td.class_names select.class_names').html() + "</select>" );
    $('.duplicate_bank_entry_'+timestamp+' td.class_names select.class_names').selectpicker({
      liveSearch : true,
    });

  });

  var edit_invoices = false;
  var edit_invoices_func = function() {

  $('.edit_invoices').click(function(){
    if( edit_invoices ) {
      $('.edit_invoices_item').hide();
      $(this).text('Edit Invoices');
      edit_invoices = false;
    } else {
      $('.edit_invoices_item').show();
      $(this).text('Cancel Edit Invoices');
      edit_invoices = true;
    }
  });
  $('.edit_invoices_select_all').click(function(){
    if($(this).prop('checked')) {
      $('input.edit_invoices_delete_item').prop('checked',true);
    } else {
      $('input.edit_invoices_delete_item').prop('checked',false);
    }
  });

  };

  var autosum = function(input, output){
    var total = 0;
    $(input).each(function(){
      total += parseFloat( $(this).val().replace(',','') );
    });
    $(output).text( (Math.round(total * 100) / 100) );
  };

  var init_payment_buttons = function() {
 
  $('.payment_autoapply').click(function(){
    var max_amount = parseFloat($(this).attr('data-max_amount'));
    var input = $(this).attr('data-input');
    $('.'+input).each(function(){
      var limit = parseFloat( $(this).attr('data-limit') );
      var amount = (max_amount > limit) ? limit : max_amount;
      $(this).val( (amount > 0) ? (Math.round(amount * 100) / 100) : 0 );
       max_amount -= parseFloat( limit );
    });
    autosum('.'+input, '#autosum-output');
  });

  $('.payment_unapply').click(function(){
      var input = $(this).attr('data-input');
      $('.'+input).each(function(){
        $(this).val( '0.00' );
      });
      autosum('.'+input, '#autosum-output');
  });

};

var total_deposits = function(){
      var total_selected_deposits = 0;
      $('.deposit_item').each(function(){
        var rId = $(this).val();
        var amount = parseFloat( $(this).attr('data-amount') );
        
        if( $(this).prop('checked') ) {
          total_selected_deposits += amount;
        }
        
        $('.total_selected_deposits').text( numeral( total_selected_deposits ).format('0,0.00')  );

        if( total_selected_deposits > 0) {
          $('.deposits_receipts_selected').show();
        } else {
          $('.deposits_receipts_selected').hide();
        }
      });
};

var checkSelectedReceipts = function() {
   $('#deposits_select_all').click(function(){
      if($(this).prop('checked')) {
        $('input.deposit_item').prop('checked',true);
        $('.select_sales_receipt').addClass('success');
      } else {
        $('input.deposit_item').prop('checked',false);
        $('.select_sales_receipt').removeClass('success');
      }
      total_deposits();
    });

   $('input.deposit_item').click(function(){
      var rId = $(this).val();
      if($(this).prop('checked')) {
          $('#sales-receipt-' + rId).addClass('success');
      } else {
          $('#sales-receipt-' + rId).removeClass('success');
      }
      total_deposits();
   });

   $('.select_sales_receipt td.clickable').click(function(){
       var rId = $(this).parent().attr('data-id');
       $('#deposit_item-' + rId).trigger('click');
   });
};

var bodyWrapper = function() {
   $('.body_wrapper').each(function(){
      var href = $(this).prop('href');
      var data_url = $(this).attr('data-url');
      var data_morph = $(this).attr('data-morph');
      if( data_morph != 'bodyWrapper') {
        $(this).attr('data-url', href);
        $(this).prop('href', 'javascript:void(0);');
        $(this).attr('data-morph', 'bodyWrapper');
        $(this).click(function() {
              var divBody = $('#bodyWrapper');
              var loadingDiv = $('<div class="loading-wait"></div>');
              loadingDiv.css('height', ( divBody.parent().height() - 43 ) );
              var loadingImg = $('<img src="/assets/images/loader4.gif"/>');
              loadingImg.css('margin-top', Math.ceil( divBody.parent().height() / 2 ));
              loadingDiv.html( loadingImg );
              divBody.prepend( loadingDiv );

              var ajax_url = $(this).attr('data-url');

              $.ajax({
                url: ajax_url,
                method: 'POST',
                data: {
                  output: 'body_wrapper',
                },
                success: function( data ) {
                    divBody.slideUp( function(){
                      $(this).html( data );
                      $(this).slideDown(function(){
                        loadingDiv.remove();
                        window.history.replaceState('Object', $(document).prop('title'), ajax_url);
                        $('.autocomplete-member_change').attr('data-current_sub_uri', ajax_url);
                        init_coop();
                      });
                    });
                }
              }); // $.ajax()
        }); // $(this).click()
      }
   });
 }; // bodyWrapper

var loadLib = function() {

    bodyWrapper();
    init_sortable();

$('#ajaxModal .datepicker').datepicker();
        $('#ajaxModal select').selectpicker({
          liveSearch : true,
        });
        $('#ajaxModal .confirm').click(function(){
          if(confirm('Are you sure?')) {
            return true;
          } else {
            return false;
          }
        });
    $('#ajaxModal .autocomplete-name_select').autocomplete({
      source: function( request, response ) {
        var ths = $(this);
        var source = ths[0]['element'][0]['dataset'].source;
        $.ajax({
          url: source,
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
        var name_id = event['target']['dataset'].name_id;
        var full_name = event['target']['dataset'].full_name;
        var target = $(event['target']);
        var name = $('<div/>').addClass('form-control');
        var link = $('<a/>').addClass('badge');
        var timestamp = $.now();

        target.addClass('autocomplete-name_select-name-input-' + timestamp );
        
        name.html( link.text( ui.item.label ) );
        name.addClass('autocomplete-name_select-name-display-' + timestamp );
        name.insertAfter( target );

        link.prop('href', '#changeName');
        link.attr('data-id', ui.item.id );
        link.attr('data-name_id', name_id );
        link.attr('data-timestamp', timestamp );

          link.click(function(){
            var name_id = $(this).attr('data-name_id');
            var timestamp = $(this).attr('data-timestamp');
            $('#'+name_id).val('');
            $('.autocomplete-name_select-name-display-'+ timestamp).remove();
            $('.autocomplete-name_select-name-input-'+ timestamp).val('').show().focus().removeClass( 'autocomplete-name_select-name-input-'+ timestamp );
          });
        name.prepend( link );

        target.hide();
        $('#'+name_id).val(ui.item.id);
        $('#'+full_name).val(ui.item.label);
      }
    });

    $('#ajaxModal #changeName').click(function(){
      var name_id = $(this).attr('data-name_id');
      var timestamp = $(this).attr('data-timestamp');
      $('#'+name_id).val('');
      $('.autocomplete-name_select-name-display-'+ timestamp).remove();
      $('.autocomplete-name_select-name-input-'+ timestamp).val('').show().focus().removeClass( 'autocomplete-name_select-name-input-'+ timestamp );
    });

$('.ajax-modal-inner').each(function(){
  var href = $(this).prop('href');
  $(this).attr('data-url', href);
  $(this).prop('href', 'javascript:void(0);');
  $(this).click(function(){
    ajaxModalUrl = $(this).attr('data-url');

    $('#ajaxModal form').prop( 'action', ajaxModalUrl );
    $('#ajaxModal .modal-title').text( $(this).attr('data-title') );

    var back_level_n = 0;
    if( $(this).attr('data-back_level') ) {
        modal_backlink.splice((modal_backlink.length-1),1);
    } else {
        modal_backlink.push({
          url : $(this).attr('data-url'),
          title : $(this).attr('data-title'), 
          hide_footer : $(this).attr('data-hide_footer')
        });
    }
    back_level_n = (modal_backlink.length-2);

    var back_link = $('#ajax-modal-backlink');
    if( back_link.length ) {
      back_link.remove();
    }

  if( modal_backlink[back_level_n] ) {
      var back_link = $('<button class="close ajax-modal-inner" id="ajax-modal-backlink"><span class="glyphicon glyphicon-arrow-left"></span></button>');
      back_link.css('float', 'left');
      back_link.css('font-size', 'large');
      back_link.attr('data-url', modal_backlink[back_level_n].url);
      back_link.attr('data-title', modal_backlink[back_level_n].title);
      back_link.attr('title', modal_backlink[back_level_n].title);
      back_link.attr('data-hide_footer', modal_backlink[back_level_n].hide_footer);
      back_link.attr('data-back_level', 1);
       $('#ajaxModal .modal-header').prepend(back_link);
    }
       
    $('#ajaxModal .loader').slideDown('slow');
    var hide_footer = $(this).attr('data-hide_footer');
    if( hide_footer ) {
      $('#ajaxModal .modal-footer').slideUp();
    } else {
      $('#ajaxModal .modal-footer').slideDown();
    }
    $('#ajaxModal .output').slideUp('slow').html( '' );
      $.ajax({
        url : ajaxModalUrl,
        method : 'GET',
        dataType : 'html'
      }).success(function(html){
        $('#ajaxModal .loader').slideUp('slow', function(){
          $('#ajaxModal .output').css('display', 'none').html( html ).slideDown('slow', function(){
            loadLib();
          });
        });
      });

  });
});

  }; // loadLib
  
var ajaxModalUrl = null;
var setupAjaxModal = function(){
    $('.ajax-modal').each(function(){
        var href = $(this).prop('href');
        var data_morph = $(this).attr('data-morph');
        var data_url = $(this).attr('data-url');
        if( (typeof(data_morph) == 'undefined') && (typeof(data_url) == 'undefined') ) {
            $(this).attr('data-url', href);
            $(this).attr('data-morph', 'ajax_modal');
            $(this).prop('href', 'javascript:void(0);');
            $(this).click(function(){
                $('#ajaxModal').modal();
                $('#ajaxModal').find('.modal-title').text( $(this).attr('data-title') );
                var button_title = $(this).attr('data-button_title');
                if( button_title !== undefined ) {
                  $('#ajaxModal').find('.modal-footer button[type="submit"]').text( button_title );
                }
                ajaxModalUrl = $(this).attr('data-url');
                var hide_footer = $(this).attr('data-hide_footer');
                 if( hide_footer ) {
                    $('#ajaxModal').find('.modal-footer').slideUp();
                  } else {
                    $('#ajaxModal').find('.modal-footer').slideDown();
                  }
                var multipart = $(this).attr('data-multipart');
                if( multipart ) {
                  $('#ajaxModalForm').prop('enctype','multipart/form-data').attr('accept-charset','utf-8');
                } else {
                  $('#ajaxModalForm').removeProp('enctype').removeAttr('accept-charset');
                }
                modal_backlink = [];
                modal_backlink.push({ 
                  url : $(this).attr('data-url'), 
                  title : $(this).attr('data-title'),
                  hide_footer : $(this).attr('data-hide_footer'),
                });
            });
        }
    });

  $('#ajaxModal').on('shown.bs.modal', function () {
    $('#ajaxModal form').prop( 'action', ajaxModalUrl );
      $.ajax({
        url : ajaxModalUrl,
        method : 'GET',
        dataType : 'html'
      }).success(function(html){
        $('#ajaxModal .loader').slideUp('slow', function(){
          $('#ajaxModal .output').css('display', 'none').html( html ).slideDown('slow', function(){
            loadLib();
          });
        });
  });

  }).on('hidden.bs.modal', function (e) {
      $('#ajaxModal .loader').show();
      $('#ajaxModal .modal-footer').hide();
      $('#ajaxModal .output').html( '' );
      $('#ajax-modal-backlink').remove();
  });

};

var loadAjaxModal = function() {
  $('.ajax-dialog').click(function(){
     ajaxModalUrl = $(this).attr('data-url');
    $('#ajaxDialog form').prop( 'action', ajaxModalUrl );
    $('#ajaxDialog .loader').slideDown('slow');
    $('#ajaxDialog .output').slideUp('slow').html( '' );
      $.ajax({
        url : ajaxModalUrl,
        method : 'GET',
        dataType : 'html'
      }).success(function(html){
        $('#ajaxDialog .loader').slideUp('slow', function(){
          $('#ajaxDialog .output').css('display', 'none').html( html ).slideDown('slow');
        });
        
      });
    $('#ajaxDialog').dialog({
      resizable: false,
      height: "auto",
      width: '80%',
      modal: true,
      title : $(this).attr('data-title'),
    }).css('overflow', 'inherit');
  });

};

loadAjaxModal();

  $('#recon input.recon_item').click(function(){ 

    var trn_id = $(this).val();
   if( $(this).prop('checked') ) {
      $('#list-group-item-'+trn_id).addClass('highlight');
   } else {
      $('#list-group-item-'+trn_id).removeClass('highlight');
   }

    var recon_balance = {
      'deposit' : 0,
      'disburse' : 0,
    };

    $('#recon input.recon_item:checked').each(function(){
        var type = $(this).attr('data-type');
        var amount = $(this).attr('data-amount');
          recon_balance[type] = parseFloat( recon_balance[type] ) + parseFloat( amount );
    });

    var balance_beg = parseFloat( $('#recon_balance_beg').text().replace(',', '') );
    var balance_end = parseFloat( $('#recon_balance_end').text().replace(',', '') );
    var balance_cleared = 0;
    
    balance_cleared = (balance_beg + recon_balance.deposit) - recon_balance.disburse; 
    balance_difference = ((balance_end - balance_beg) + recon_balance.disburse) - recon_balance.deposit; 
   
    $('#recon_output_disburse').text( numeral( recon_balance['disburse'] ).format('0,0.00') );
    $('#recon_output_deposit').text( numeral( recon_balance['deposit'] ).format('0,0.00') );
    $('#recon_output_cleared').text( numeral( balance_cleared ).format('0,0.00') );
    $('#recon_output_difference').text( numeral( balance_difference ).format('0,0.00') );

    if( numeral( balance_difference ).format('0') == 0) {
      $('#reconcile_button').removeClass('hidden');
    } else {
      $('#reconcile_button').addClass('hidden');
    }

  });
  
  var debit_credit_equal = function(debit,credit){
    if(debit==credit) {
      $('#saveEntriesButton').show();
    } else {
      $('#saveEntriesButton').hide();
    }
  };

  var debit_total = function(){
    var debit_total = 0;
    $('.debit_amount').each(function(){ 
      var amount = parseFloat(numeral( $(this).val() ).format('0'));
      debit_total = debit_total + amount;
    });
    $('#debit_total').text( numeral( debit_total ).format('0,0.00') );
    return debit_total;
  };
  var credit_total = function(){
    var credit_total = 0;
    $('.credit_amount').each(function(){ 
      var amount = parseFloat(numeral( $(this).val() ).format('0'));
      credit_total = credit_total + amount;
    });
    $('#credit_total').text( numeral( credit_total ).format('0,0.00') );
    return credit_total;
  };
  debit_total();
  credit_total();
  $('.debit_amount').keyup(function(){
    debit_total();
    debit_credit_equal(debit_total(), credit_total());
  });
  $('.credit_amount').keyup(function(){
    credit_total();
    debit_credit_equal(debit_total(), credit_total());
  });
  var removeJournalLine = function(){
    $(this).parent().parent().remove();
    debit_credit_equal(debit_total(), credit_total());
  };
  var insertJournalLine = function(){
    var timestamp = $.now();
    var line = $(this).parent().parent();
    var clone = $(this).parent().parent().clone();
    clone.find('a.insertJournalLine').click( insertJournalLine );
    clone.find('a.removeJournalLine').click( removeJournalLine );
    clone.find('input').val('');
    clone.find('div.autocomplete-name_select').remove();
    var name_select = clone.find('input.autocomplete-name_select')
    name_select.show();
    name_select.autocomplete( autocomplete_name_select_options );
    var name_id = name_select.attr('data-name_id');
    var new_name_id = 'journal_name_id_' + timestamp;
    name_select.attr('data-name_id', new_name_id);
    clone.find('input[type="hidden"]#' + name_id).prop('id', new_name_id);
    clone.insertBefore( line );
      var select_clone = clone.find($('select'));
      select_clone.each(function(){
        $(this).find('option:selected').prop('selected', false);
        var parent = $(this).parent();
        $(this).insertBefore(parent);
        parent.remove();
        $(this).selectpicker({
          liveSearch : true,
        });
      });

    $('.debit_amount').keyup(function(){
      debit_total();
      debit_credit_equal(debit_total(), credit_total());
    });
    $('.credit_amount').keyup(function(){
      credit_total();
      debit_credit_equal(debit_total(), credit_total());
    });
  };
  var unlockJournalLine = function() {
    var line_number = $(this).attr('data-line_number');
    $('.je_input-'+line_number).prop('disabled', false);
    $('.je_input-'+line_number).selectpicker('refresh');
  };

  var journalLine = function() {
    $('.unlockJournalLine').click(unlockJournalLine);
    $('.insertJournalLine').click(insertJournalLine);
    $('.removeJournalLine').click(removeJournalLine);
  };

var lending_schedule_details = function() {
  $('.lending-schedule-details').click(function(){
    $('.lending-schedule-details').removeClass('btn-success').addClass('btn-default');
    $(this).addClass('btn-success');
    var type = $(this).attr('data-type');
    if( type == 'minimal' ) {
      $('.detailed-schedule').hide();
    } else if( type == 'detailed') {
      $('.detailed-schedule').show();
    }
  });
};

  var panelHeight = function() {
    var innerPage = $('#ajaxBodyInnerPage');
    var panel = innerPage.parent();
    var inner_height = innerPage.height() + 73;
    var panel_height =  panel.height();
    var current_height = parseInt(panel.css('height'));
    if( (current_height > panel_height) && (current_height > inner_height) ) {
      panel.css( 'height', current_height);
    } else {
      if( panel_height > inner_height ) {
        panel.css( 'min-height', panel_height);
      } else {
        panel.css( 'min-height', inner_height);
      }
    }
  } 
  
  var ajaxPagination = function() {

    $('.ajaxPage').each(function(){
      var href = $(this).prop('href');
      var data_url = $(this).attr('data-url');
      var data_morph = $(this).attr('data-morph');
      if( data_morph != 'ajaxPage') {
        $(this).attr('data-url', href);
        $(this).prop('href', 'javascript:void(0);');
        $(this).attr('data-morph', 'ajaxPage');
        $(this).click(function(){
              var divBody = $('#ajaxBodyInnerPage');
              var loadingDiv = $('<div class="loading-wait"></div>');
              loadingDiv.css('height', ( divBody.parent().height() - 43 ) );
              var loadingImg = $('<img src="/assets/images/loader4.gif"/>');
              loadingImg.css('margin-top', Math.ceil( divBody.parent().height() / 2 ));
              loadingDiv.html( loadingImg );
              divBody.prepend( loadingDiv );

              var ajax_url = $(this).attr('data-url');

              $.ajax({
                url: ajax_url,
                method: 'POST',
                data: {
                  output: 'inner_page',
                },
                success: function( data ) {
                    divBody.slideUp( function(){
                      $(this).html( data );
                      panelHeight();
                      $(this).slideDown(function(){
                        loadingDiv.remove();
                        window.history.replaceState('Object', $(document).prop('title'), ajax_url);
                        $('.autocomplete-member_change').attr('data-current_sub_uri', ajax_url);
                        init_coop();
                      });
                    });
                }
              }); // $.ajax()
            }); // $(this).click()
        }
    });
  };

 var init_coop = function() {
      bodyWrapper();
      ajaxPagination();
      setupAjaxModal();
      lending_schedule_details();
      edit_invoices_func();
      journalLine();
      confirmButton();
      confirmRemove();
      init_datepicker();
      init_payment_buttons();
      panelHeight();
      navbar_search_employee();
      addDeposits();
      //autocomplete_navbarsearch();
      //select_account_titles();
      //hoverPop();
      checkSelectedReceipts();
      init_sortable();
 }
 init_coop();
})(jQuery);

(function($){
    var widget_image_slider_action = 'add';
    var widget_image_slider_id = false;
    var append_hidden_image = function(k,v,i,c) {
      var h = $('<input type="hidden" id="'+k+c+'" name="items['+c+']['+k+']" value="'+v+'">');
      i.prepend(h);
    };

    var clear_inputs = function() {
        $('#image_id').val('');
        $('#image_url').val('');
        $('#image_title').val('');
        $('#image_desc').val('');
        $('#image_link').val('');
        $('#image_link_target').val('');
        $('#show_title').prop('checked', false);
        $('#show_desc').prop('checked', false);
        $('#image_page > option:selected').prop('selected', false);
        $('#image_page').selectpicker('refresh');
        $('#container_class').val('');
    };

    $('#widget_add_image_slider_button').click(function(){
        widget_image_slider_action = 'add';
        clear_inputs();
        $('#imageSliderModal').modal('show');
    });

    var image_slider_item_edit = function(){

        var idc = $(this).attr('data-id');

        widget_image_slider_action = 'edit';
        widget_image_slider_id = idc;

        var image_id = $('#image_id'+idc).val();
        var image_url = $('#image_url'+idc).val();
        var image_title = $('#image_title'+idc).val();
        var image_desc = $('#image_desc'+idc).val();
        var image_link = $('#image_link'+idc).val();
        var image_link_target = $('#image_link_target'+idc).val();
        var show_title = $('#show_title'+idc).val();
        var show_desc = $('#show_desc'+idc).val();
        var show_caption = $('#show_caption'+idc).val();
        var image_page = $('#image_page'+idc).val();
        var container_class = $('#container_class'+idc).val();

        $('#image_id_item_'+image_id).prop('selected', true);
        $('#image_id').selectpicker('refresh');
        $('#image_url').val(image_url);
        $('#image_title').val(image_title);
        $('#image_desc').val(image_desc);
        $('#image_link').val(image_link);
        
        $('#image_link_target').val(image_link_target);
        if( show_title == '1') {
          $('#show_title').prop('checked', true);
        }
        if( show_desc == '1') {
          $('#show_desc').prop('checked', true);
        }
        if( show_caption == '1') {
          $('#show_caption').prop('checked', true);
        }
        $('#image_page_item_'+image_page).prop('selected', true);
        $('#image_page').selectpicker('refresh');
        $('#container_class').val(container_class);
        $('#imageSliderModal').modal('show');
    };

    var image_slider_item_delete = function(){
         var idc = $(this).attr('data-id');
         if( confirm("Are you sure?") ) {
            $('#image_slider_item'+idc).remove();
         }
    }

    var image_slider_item_duplicate = function() {

        var idc = $(this).attr('data-id');

        var image_id = $('#image_id'+idc).val();
        var image_url = $('#image_url'+idc).val();
        var image_title = $('#image_title'+idc).val();
        var image_desc = $('#image_desc'+idc).val();
        var image_link = $('#image_link'+idc).val();
        var image_link_target = $('#image_link_target'+idc).val();
        var show_title = $('#show_title'+idc).val();
        var show_desc = $('#show_desc'+idc).val();
        var show_caption = $('#show_caption'+idc).val();
        var image_page = $('#image_page'+idc).val();
        var container_class = $('#container_class'+idc).val();

        var item = $('<div class="list-group-item"></div>');
        item.attr('id', 'image_slider_item'+image_slider_count);
        item.html('<span id="image_slider_item_title'+image_slider_count+'">'+image_title+'</span>');
        item.attr('id', 'image_slider_item'+image_slider_count);

        append_hidden_image('image_id', image_id, item, image_slider_count);
        append_hidden_image('image_url', image_url, item, image_slider_count);
        append_hidden_image('image_title', image_title, item, image_slider_count);
        append_hidden_image('image_desc', image_desc, item, image_slider_count);
        append_hidden_image('image_link', image_link, item, image_slider_count);
        append_hidden_image('image_link_target', image_link_target, item, image_slider_count);
        append_hidden_image('show_title', show_title, item, image_slider_count);
        append_hidden_image('show_desc', show_desc, item, image_slider_count);
        append_hidden_image('show_caption', show_caption, item, image_slider_count);
        append_hidden_image('image_page', image_page, item, image_slider_count);
        append_hidden_image('container_class', container_class, item, image_slider_count);

        var item_duplicate = $('<a href="javascript:void(0);" class="image_slider_item_duplicate btn btn-warning btn-xs pull-right" style="margin-left:5px;">Duplicate</a>');
        item_duplicate.attr('data-id', image_slider_count);
        item_duplicate.click(image_slider_item_duplicate);
        item.append( item_duplicate );

        var item_delete = $('<a href="javascript:void(0);" class="image_slider_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;">Delete</a>');
        item_delete.attr('data-id', image_slider_count);
        item_delete.click(image_slider_item_delete);
        item.append( item_delete );

        var item_edit = $('<a href="javascript:void(0);" class="image_slider_item_edit btn btn-success btn-xs pull-right">Edit</a>');
        item_edit.attr('data-id', image_slider_count);
        item_edit.click(image_slider_item_edit);
        item.append( item_edit );
        
        $('#image_slider_group').append(item);
        image_slider_count++;
    };

    $('#widget_image_slider_submit').click(function(){
        var image_id = $('#image_id > option:selected').val();
        var image_url = $('#image_url').val();
        var image_title = $('#image_title').val();
        var image_desc = $('#image_desc').val();
        var image_link = $('#image_link').val();
        var image_link_target = $('#image_link_target').val();
        var show_title = $('#show_title').is(':checked') ? 1 : 0;
        var show_desc = $('#show_desc').is(':checked') ? 1 : 0;
        var show_caption = $('#show_caption').is(':checked') ? 1 : 0;
        var image_page = $('#image_page > option:selected').val();
        var container_class = $('#container_class').val();

        var item = $('<div class="list-group-item"></div>');
        item.attr('id', 'image_slider_item'+image_slider_count);
        item.html('<span id="image_slider_item_title'+image_slider_count+'">'+image_title+'</span>');
        item.attr('id', 'image_slider_item'+image_slider_count);
        
        append_hidden_image('image_id', image_id, item, image_slider_count);
        append_hidden_image('image_url', image_url, item, image_slider_count);
        append_hidden_image('image_title', image_title, item, image_slider_count);
        append_hidden_image('image_desc', image_desc, item, image_slider_count);
        append_hidden_image('image_link', image_link, item, image_slider_count);
        append_hidden_image('image_link_target', image_link_target, item, image_slider_count);
        append_hidden_image('show_title', show_title, item, image_slider_count);
        append_hidden_image('show_desc', show_desc, item, image_slider_count);
        append_hidden_image('show_caption', show_caption, item, image_slider_count);
        append_hidden_image('image_page', image_page, item, image_slider_count);
        append_hidden_image('container_class', container_class, item, image_slider_count);

        var item_duplicate = $('<a href="javascript:void(0);" class="image_slider_item_duplicate btn btn-warning btn-xs pull-right" style="margin-left:5px;">Duplicate</a>');
        item_duplicate.attr('data-id', image_slider_count);
        item_duplicate.click(image_slider_item_duplicate);
        item.append( item_duplicate );

        var item_delete = $('<a href="javascript:void(0);" class="image_slider_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;">Delete</a>');
        item_delete.attr('data-id', image_slider_count);
        item_delete.click(image_slider_item_delete);
        item.append( item_delete );

        var item_edit = $('<a href="javascript:void(0);" class="image_slider_item_edit btn btn-success btn-xs pull-right">Edit</a>');
        item_edit.attr('data-id', image_slider_count);
        item_edit.click(image_slider_item_edit);
        item.append( item_edit );
        
        if(( image_url != '') && ( image_url != '') && ( image_url != '')) {
          if( widget_image_slider_action == 'add' ) {
            $('#image_slider_group').append(item);
            image_slider_count++;
          } else if((widget_image_slider_action == 'edit') && (widget_image_slider_id)) {
            $('#image_id'+widget_image_slider_id).val(image_id);
            $('#image_url'+widget_image_slider_id).val(image_url);
            $('#image_title'+widget_image_slider_id).val(image_title);
            $('#image_desc'+widget_image_slider_id).val(image_desc);
            $('#image_link'+widget_image_slider_id).val(image_link);
            $('#image_link_target'+widget_image_slider_id).val(image_link_target);
            $('#show_title'+widget_image_slider_id).val(show_title);
            $('#show_desc'+widget_image_slider_id).val(show_desc);
            $('#show_caption'+widget_image_slider_id).val(show_caption);
            $('#image_page'+widget_image_slider_id).val(image_page);
            $('#container_class'+widget_image_slider_id).val(container_class);

            if( $('#show_title'+widget_image_slider_id).is() == false ) {
                append_hidden_image('show_title', show_title, $('#image_slider_item'+widget_image_slider_id), widget_image_slider_id);
            }
            if( $('#show_desc'+widget_image_slider_id).is() == false ) {
                append_hidden_image('show_desc', show_desc, $('#image_slider_item'+widget_image_slider_id), widget_image_slider_id);
            }
            if( $('#show_caption'+widget_image_slider_id).is() == false ) {
                append_hidden_image('show_caption', show_caption, $('#image_slider_item'+widget_image_slider_id), widget_image_slider_id);
            }
            if( $('#image_page'+widget_image_slider_id).is() == false ) {
                append_hidden_image('image_page', image_page, $('#image_slider_item'+widget_image_slider_id), widget_image_slider_id);
            }
            $('#image_slider_item_title'+widget_image_slider_id).text(image_title);
          }
        }

        clear_inputs();

        $('#imageSliderModal').modal('hide');
    });

    $('#image_slider_group .image_slider_item_edit').click(image_slider_item_edit);
    $('#image_slider_group .image_slider_item_delete').click(image_slider_item_delete);
    $('#image_slider_group .image_slider_item_duplicate').click(image_slider_item_duplicate);

})(jQuery);

(function($){
    var widget_link_action = 'add';
    var widget_link_id = false;

    var clear_inputs = function() {
        $('#link_text').val('');
        $('#link_icon').val('');
        $('#link_desc').val('');
        $('#link_link').val('');
        $('#link_target').val('');
        $('#link_class').val('');
        $('#link_page > option:selected').prop('selected', false);
        $('#link_page').selectpicker('refresh');
    };

    $('#widget_add_link_button').click(function(){
        widget_link_action = 'add';
        clear_inputs();
        $('#linkModal').modal('show');
    });

    var list_item_edit = function(){

        var idc = $(this).attr('data-id');

        widget_link_action = 'edit';
        widget_link_id = idc;

        var link_text = $('#link_text'+idc).val();
        var link_icon = $('#link_icon'+idc).val();
        var link_desc = $('#link_desc'+idc).val();
        var link_link = $('#link_link'+idc).val();
        var link_target = $('#link_target'+idc).val();
        var link_class = $('#link_class'+idc).val();
        var link_page = $('#link_page'+idc).val();

        $('#link_text').val(link_text);
        $('#link_icon').val(link_icon);
        $('#link_desc').val(link_desc);
        $('#link_link').val(link_link);
        $('#link_target').val(link_target);
        $('#link_class').val(link_class);
        $('#link_page_item_'+link_page).prop('selected', true);
        $('#link_page').selectpicker('refresh');
        $('#linkModal').modal('show');
    };

    var list_item_delete = function(){
         var idc = $(this).attr('data-id');
         if( confirm("Are you sure?") ) {
            $('#list_item'+idc).remove();
         }
    }

    $('#widget_link_submit').click(function(){
        var link_text = $('#link_text').val();
        var link_icon = $('#link_icon').val();
        var link_desc = $('#link_desc').val();
        var link_link = $('#link_link').val();
        var link_target = $('#link_target').val();
        var link_class = $('#link_class').val();
        var link_page = $('#link_page > option:selected').val();

        var append_hidden_list = function(k,v,i,c) {
          var h = $('<input type="hidden" id="'+k+c+'" name="items['+c+']['+k+']" value="'+v+'">');
          i.append(h);
        };

        var item = $('<div class="list-group-item"></div>');
        item.attr('id', 'list_item'+link_count);
        item.html('<span id="list_item_title'+link_count+'">'+link_text+'</span>');
        item.attr('id', 'list_item'+link_count);
        
        append_hidden_list('link_text', link_text, item, link_count);
        append_hidden_list('link_icon', link_icon, item, link_count);
        append_hidden_list('link_desc', link_desc, item, link_count);
        append_hidden_list('link_link', link_link, item, link_count);
        append_hidden_list('link_target', link_target, item, link_count);
        append_hidden_list('link_class', link_class, item, link_count);
        append_hidden_list('link_page', link_page, item, link_count);

        var item_edit = $('<a href="javascript:void(0);" class="list_item_edit btn btn-warning btn-xs pull-right">Edit</a>');
        item_edit.attr('data-id', link_count);
        item_edit.click(list_item_edit);
        var item_delete = $('<a href="javascript:void(0);" class="list_item_delete btn btn-danger btn-xs pull-right" style="margin-left:5px;">Delete</a>');
        item_delete.attr('data-id', link_count);
        item_delete.click(list_item_delete);
        item.append( item_delete );
        item.append( item_edit );


        if(( link_text != '') && ( link_text != '') && ( link_text != '')) {
          if( widget_link_action == 'add' ) {
            $('#list_group').append(item);
            link_count++;
          } else if((widget_link_action == 'edit') && (widget_link_id)) {
            $('#link_text'+widget_link_id).val(link_text);
            $('#link_icon'+widget_link_id).val(link_icon);
            $('#link_desc'+widget_link_id).val(link_desc);
            $('#link_link'+widget_link_id).val(link_link);
            $('#link_target'+widget_link_id).val(link_target);
            $('#link_class'+widget_link_id).val(link_class);
            $('#link_page'+widget_link_id).val(link_page);
            $('#list_item_title'+widget_link_id).text(link_text);
          }
        }

        clear_inputs();

        $('#linkModal').modal('hide');
    });

    $('#list_group .list_item_edit').click(list_item_edit);
    $('#list_group .list_item_delete').click(list_item_delete);

})(jQuery);