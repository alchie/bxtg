<div id="<?php echo ((isset($widget_options->widget_id))&&($widget_options->widget_id!='')) ? $widget_options->widget_id : 'widget_'. $widget_id; ?>">
    <div class="banner carousel slide" id="recommended-item-carousel" data-ride="carousel">
      <div class="slides carousel-inner">

<?php 
$item_n = 0;
if( $widget_options ) foreach($widget_options->items as $item) { ?>
        <div class="item <?php echo ($item_n==0) ? 'active' : ''; ?>">
<?php if( $item->image_link != '') { ?>
<a href="<?php echo $item->image_link; ?>">
<?php } ?>

<?php $image_url = 'http://1.bp.blogspot.com/-Zc0zCn93fQo/XiUQHrin85I/AAAAAAAATQQ/mv5nbrT5fuEd03vx9vsORg6hGVu6K0HrQCK4BGAYYCw/s0/no-image.jpg';
if($item->image_id) {
  if( in_array($item->image_id, array('sample_1','sample_2','sample_3','sample_4')) ) {
    switch($item->image_id) {
      case 'sample_1':
        $image_url = 'http://4.bp.blogspot.com/-QGmHWZX0lQY/XiUQokaqtOI/AAAAAAAATQc/PIXK40OvIR4zXSC7Kyv_KDvGieP6Hm8TACK4BGAYYCw/s0/sample_1.jpg';
      break;
      case 'sample_2':
        $image_url = 'http://2.bp.blogspot.com/-3Td9dHb9sv8/XiUQxFeyBdI/AAAAAAAATQk/GWi1xbt_c5kw-g2uan9McrP9SruugsdAQCK4BGAYYCw/s0/sample_2.jpg';
      break;
      case 'sample_3':
        $image_url = 'http://2.bp.blogspot.com/-oVvrZCVfpTc/XiUQ8U2FB0I/AAAAAAAATQs/LIimgNfvj0oSVgULzKw8TmqH_5zmXBAHwCK4BGAYYCw/s0/sample_3.jpg';
      break;
      case 'sample_4':
        $image_url = 'http://4.bp.blogspot.com/-OHI3HIVWoLo/XiURCjaWblI/AAAAAAAATQ4/_k1YZvgDkooJSvUyx8QYVkD-8PICGI9ugCK4BGAYYCw/s0/sample_4.jpg';
      break;
      default:
        $image_url = 'http://1.bp.blogspot.com/-Zc0zCn93fQo/XiUQHrin85I/AAAAAAAATQQ/mv5nbrT5fuEd03vx9vsORg6hGVu6K0HrQCK4BGAYYCw/s0/no-image.jpg';
      break;
    }
  } else {
    $image_data = get_image($item->image_id);
    $image_url = $image_data->upload_url;
  }
}

?>
          <img src="<?php echo $image_url; ?>"  />

<?php if( $item->image_link != '') { ?>
</a>
<?php } ?>
<?php if($item->show_caption==1) { ?>
          <div class="banner_caption">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <div class="caption_inner animated fadeInUp">
                    <?php if($item->show_title==1) { ?><h1><?php echo $item->image_title; ?></h1><?php } ?>
                    <?php if($item->show_desc==1) { ?><p><?php echo $item->image_desc; ?></p><?php } ?>
                  </div><!--end caption_inner-->
                </div>
              </div><!--end row-->
            </div><!--end container-->
          </div><!--end banner_caption-->
<?php } ?>
        </div>
<?php 
$item_n++;
} ?>
      </div>
<?php 
if( $template_output == 'demo' ) {
  $prev = base_url('assets/templates/royal_college/images/prev.png');
  $next = base_url('assets/templates/royal_college/images/next.png');
} else {
  $prev = 'https://1.bp.blogspot.com/-ENmR_q2MCLQ/WCqZvPKL2TI/AAAAAAAALwY/B4pReByQBnI0st3xVpooIJcCkdT2mbDogCLcB/s1600/prev.png';
  $next = 'https://1.bp.blogspot.com/-_SkVT4iJzOE/WCqZuYM1TII/AAAAAAAALwI/o0G0lu4aBeYnEdCHUnbRJBVgzD2fcaQHACLcB/s1600/next.png';
}
?>
      <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">

        <img src="<?php echo $prev; ?>"  />
        </a>
      <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
        <img src="<?php echo $next; ?>"  />
      </a>    
    </div><!--end banner-->
</div>
