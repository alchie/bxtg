<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="<?php echo site_url("clients/delete/{$current_client->id}"); ?>" class="btn btn-danger btn-xs pull-right">Delete</a>
                <h3 class="panel-title">Edit Client</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

                <div class="form-group">
                    <label>Client Name</label>
                    <input name="client_name" type="text" class="form-control" value="<?php echo $current_client->name; ?>">
                </div>

                <div class="form-group">
                    <label>Address</label>
                    <input name="address" type="text" class="form-control" value="<?php echo $current_client->address; ?>">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input name="phone_number" type="text" class="form-control" value="<?php echo $current_client->phone_number; ?>">
                </div>

                <div class="form-group">
                    <label>Domain Name</label>
                    <input name="domain_name" type="text" class="form-control" value="<?php echo $current_client->domain; ?>">
                </div>
<?php if( isset($output) && ($output=='ajax') ) : ?>
    <a href="<?php echo site_url("clients/delete/{$current_client->id}"); ?>" class="btn btn-danger btn-xs confirm confirm_button btn-confirm">Delete this client</a>
<?php endif; ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("system_users"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>