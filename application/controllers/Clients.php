<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Clients');
		$this->template_data->set('current_uri', 'clients');
		$this->template_data->set('current_controller', 'clients');

		$this->_isAuth('clients', 'clients', 'view');

		//$this->check_current_client();

	}

	public function index() {
		
		$this->template_data->set('current_client', false);

		$clients = new $this->Clients_model;
		$clients->set_order('id', 'DESC');
		$this->template_data->set('clients', $clients->populate());

		$this->load->view('welcome/welcome', $this->template_data->get_data());
	}

	public function add($output='') {
		
		$this->_isAuth('clients', 'clients', 'add');

		if( $this->input->post() ) {

			$this->form_validation->set_rules('client_name', 'Client Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim');
			$this->form_validation->set_rules('domain_name', 'Domain name', 'trim');
			
			if( $this->form_validation->run() ) {
				$client = new $this->Clients_model;
				$client->setName($this->input->post('client_name'));
				$client->setAddress($this->input->post('address'));
				$client->setPhoneNumber($this->input->post('phone_number'));
				$client->setDomain($this->input->post('domain_name'));
				$client->insert();
				//redirect('clients/edit/' .  $client->getId());
				redirect('welcome/set_current_client/' .  $client->getId());
			}

			$this->postNext();
		}

		$this->template_data->set('output', $output);
		$this->load->view('clients/clients_add', $this->template_data->get_data());
	}

	public function import($output='') {

		$new_pages = array();
		$new_rows = array();
		$new_row_groups = array();
		$new_menus = array();

		if( $this->input->post('action') == 'import' ) {
			$handle = fopen($_FILES["json_file"]["tmp_name"], 'rb');
			$contents = fread($handle, filesize($_FILES["json_file"]["tmp_name"]));
			$json_data = json_decode($contents);

			$client = new $this->Clients_model;
			$client->setName($json_data->name);
			$client->setAddress($json_data->address);
			$client->setPhoneNumber($json_data->phone_number);
			$client->setDomain($json_data->domain);
			$client->insert();
			$client_id = $client->get_inserted_id();

			foreach( $json_data->layouts as $data ) {
				$layout = new $this->Layouts_model;
				$layout->setClientId($client_id);
				$layout->setLocation($data->location);
				$layout->setLayoutKey($data->layout_key);
				$layout->setLayoutValue($data->layout_value);
				$layout->insert();
			}


			foreach( $json_data->menus as $data ) {
				$menu = new $this->Menus_model;
				$menu->setClientId($client_id);
				$menu->setName($data->name);
				$menu->insert();
				$menu_id = $menu->get_inserted_id();
				$new_menus[$data->id] = $menu_id;
				
				$new_menu_links = array();
				foreach( $data->menu_links as $data2 ) {
					$link = new $this->Menus_links_model;
					$link->setMenuId($menu_id);
					$link->setText($data2->text);
					$link->setUrl($data2->url);
					$link->setIcon($data2->icon);
					$link->setParentId($data2->parent_id);
					$link->setActive($data2->active);
					$link->setInnerPage($data2->inner_page);
					$link->setOrder($data2->order);
					$link->setTarget($data2->target);
					$link->insert();
					$link_id = $link->get_inserted_id();
					$new_menu_links[$data2->id] = $link_id;
				}

				foreach( $data->menu_links as $data2 ) {
					$link = new $this->Menus_links_model;
					$link->setMenuId($menu_id,true);
					$new_link_id = (isset($new_menu_links[$data2->id])) ? $new_menu_links[$data2->id] : 0;
					$link->setParentId($new_link_id,false, true);
					$link->set_where('parent_id='.$data2->id);
					$link->update();
				}
			}

			foreach( $json_data->rows_groups as $data ) {
				
				$rows_group = new $this->Rows_groups_model;
				$rows_group->setClientId($client_id);
				$rows_group->setGroupName($data->group_name);
				$rows_group->insert();
				$new_row_groups[$data->group_id] = $rows_group->get_inserted_id();

			}

			foreach( $json_data->pages as $data ) {
				
				$page = new $this->Pages_model;
				$page->setClientId($client_id);
				$page->setPageId($data->page_id);
				$page->setPageTitle($data->page_title);
				$page->setPageSlug($data->page_slug);
				$page->setOrder($data->order);
				$page->setActive($data->active);
				$page->setParentPage($data->parent_page);
				$page->insert();
				$page_id = $page->get_inserted_id();
				$new_pages[$data->id] = $page_id;

				if( isset($data->pages_options) ) {
					foreach( $data->pages_options as $page_option ) {
						$pages_option = new $this->Pages_options_model;
						$pages_option->setClientId($client_id);
						$pages_option->setId($page_id);
						$pages_option->setOptKey($page_option->opt_key);
						$pages_option->setOptValue($page_option->opt_value);
						$pages_option->insert();
					}
				}

				if( isset($data->pages_containers) ) {
					foreach( $data->pages_containers as $page_container ) {
						$pages_container = new $this->Pages_containers_model;
						$pages_container->setClientId($client_id);
						$pages_container->setId($page_id);
						$pages_container->setOptKey($page_container->opt_key);
						$pages_container->setOptKey($page_container->opt_value);
						$pages_container->insert();
					}
				}
			}

			foreach( $json_data->rows as $data ) {

				$row = new $this->Rows_model;
				$row->setClientId($client_id);
				$row->setTitle($data->title);
				$row->setColumns($data->columns);
				$row->setRowClass($data->row_class);
				$row->setContainer($data->container);
				$row->setContainerClass($data->container_class);
				$row->setWrapperClass($data->wrapper_class);
				$row->setDisplayRows($data->display_rows);
				$row->setParentRow($data->parent_row);
				$row->setMoreSettings($data->more_settings);
				$row->setActive($data->active);
				$new_row_group_id = (isset($new_row_groups[$data->group_id])) ? $new_row_groups[$data->group_id] : 0;
				$row->setGroupId( $new_row_group_id );
				$row->insert();
				$row_id = $row->get_inserted_id();
				$new_rows[$data->id] = $row_id;

				foreach( $data->rows_columns as $data2 ) {
					$rows_column = new $this->Rows_columns_model;
					$rows_column->setRowId($row_id);
					$rows_column->setColumn($data2->column);
					$rows_column->setClass($data2->class);
					$rows_column->setContainer($data2->container);
					$rows_column->setWrapper($data2->wrapper);
					$rows_column->setMoreSettings($data2->more_settings);
					$rows_column->setColumnOrder($data2->column_order);
					$rows_column->insert();
				}

				foreach( $data->rows_columns_widgets as $data3 ) {
					$rows_columns_widget = new $this->Rows_columns_widgets_model;
					$rows_columns_widget->setRowId($row_id);
					$rows_columns_widget->setColumn($data3->column);
					$rows_columns_widget->setType($data3->type);
					$rows_columns_widget->setOptions($data3->options);
					$rows_columns_widget->setOrder($data3->order);
					$rows_columns_widget->insert();
				}

				foreach( $data->rows_settings as $data4 ) {
					$rows_setting = new $this->Rows_settings_model;
					$rows_setting->setRowId($row_id);
					$rows_setting->setSettingKey($data4->setting_key);
					$rows_setting->setSettingValue($data4->setting_value);
					$rows_setting->insert();
				}
			}

			foreach( $json_data->pages_rows as $data ) {
				$pages_rows = new $this->Pages_rows_model;
				$pages_rows->setClientId($client_id);
				$new_page_id = (isset($new_pages[$data->page_id])) ? $new_pages[$data->page_id] : 0;
				$pages_rows->setPageId($new_page_id);
				$new_row_id = (isset($new_rows[$data->row_id])) ? $new_rows[$data->row_id] : 0;
				$pages_rows->setRowId($new_row_id);
				$pages_rows->setOrder($data->order);
				$pages_rows->setActive($data->active);
				$pages_rows->setPageContainers($data->page_containers);
				$pages_rows->insert();
			}

			foreach( $json_data->layouts as $data ) {
				if( $data->location == 'common_pages' ) {
					$layout = new $this->Layouts_model;
					$layout->setClientId($client_id,true);
					$layout->setLocation($data->location,true);
					$layout->setLayoutKey($data->layout_key,true);
					$new_page_id = (isset($new_pages[$data->layout_value])) ? $new_pages[$data->layout_value] : 0;
					$layout->setLayoutValue($new_page_id,false,true);
					$layout->update();
				}
			}

			fclose($handle);
			redirect("clients");
		}
		$this->template_data->set('output', $output);
		$this->load->view('clients/clients_import', $this->template_data->get_data());
	}

	public function export($output='') {
		if( $this->input->post('action') == 'export' ) {

			$client = new $this->Clients_model('c');
			$client->setId($this->session->userdata('current_client'), true);
			$client_data = $client->get();
			
			$layouts = new $this->Layouts_model('l');
			$layouts->setClientId($this->session->userdata('current_client'),true);
			$layouts->set_limit(0);
			$client_data->layouts = $layouts->populate();

			$menus = new $this->Menus_model('m');
			$menus->setClientId($this->session->userdata('current_client'),true);
			$menus->set_limit(0);
			$menus_data = $menus->populate();
			foreach($menus_data as $i=>$menu) {
				$menu_links = new $this->Menus_links_model('ml');
				$menu_links->setMenuId($menu->id,true);
				$menu_links->set_limit(0);
				$menus_data[$i]->menu_links = $menu_links->populate();
			}
			$client_data->menus = $menus_data;

			$pages = new $this->Pages_model('p');
			$pages->setClientId($this->session->userdata('current_client'),true);
			$pages->set_limit(0);
			$pages_data = array();

			foreach($pages->populate() as $page) {

				$pages_options = new $this->Pages_options_model('p');
				$pages_options->setClientId($this->session->userdata('current_client'),true);
				$pages_options->setId($page->id,true);
				$pages_options->set_limit(0);
				$page->pages_options = $pages_options->populate();

				$pages_containers = new $this->Pages_containers_model('p');
				$pages_containers->setClientId($this->session->userdata('current_client'),true);
				$pages_containers->setId($page->id,true);
				$pages_containers->set_limit(0);
				$page->pages_containers = $pages_containers->populate();

				$pages_data[] = $page;
			}
			$client_data->pages = $pages_data;

			$rows = new $this->Rows_model('r');
			$rows->setClientId($this->session->userdata('current_client'),true);
			$rows->set_limit(0);
			$rows_data = $rows->populate();
			foreach($rows_data as $i=>$row) {
				$rows_columns = new $this->Rows_columns_model('rc');
				$rows_columns->setRowId($row->id,true);
				$rows_columns->set_limit(0);
				$rows_data[$i]->rows_columns = $rows_columns->populate();

				$rows_columns_widgets = new $this->Rows_columns_widgets_model('rcw');
				$rows_columns_widgets->setRowId($row->id,true);
				$rows_columns_widgets->set_limit(0);
				$rows_data[$i]->rows_columns_widgets = $rows_columns_widgets->populate();

				$rows_settings = new $this->Rows_settings_model('rc');
				$rows_settings->setRowId($row->id,true);
				$rows_settings->set_limit(0);
				$rows_data[$i]->rows_settings = $rows_settings->populate();
			}
			$client_data->rows = $rows_data;

			$rows_groups = new $this->Rows_groups_model('rg');
			$rows_groups->setClientId($this->session->userdata('current_client'),true);
			$rows_groups->set_limit(0);
			$client_data->rows_groups = $rows_groups->populate();

			$pages_rows = new $this->Pages_rows_model('p');
			$pages_rows->setClientId($this->session->userdata('current_client'),true);
			$pages_rows->set_limit(0);
			$client_data->pages_rows = $pages_rows->populate();

			// download
			force_download( url_title( $client_data->name ) . ".json", json_encode( $client_data ) );
			$this->postNext();
		}
		$this->template_data->set('output', $output);
		$this->load->view('clients/clients_export', $this->template_data->get_data());
	}

	public function edit($id, $output='') {

		$this->_isAuth('clients', 'clients', 'edit');

		$client = new $this->Clients_model;
		$client->setId($this->session->userdata('current_client'), true);

		if( $this->input->post() ) {

			$this->form_validation->set_rules('client_name', 'Client Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim');
			$this->form_validation->set_rules('domain_name', 'Domain name', 'trim');

			if( $this->form_validation->run() ) {
				$client->setName($this->input->post('client_name'),false,true);
				$client->setAddress($this->input->post('address'),false,true);
				$client->setPhoneNumber($this->input->post('phone_number'),false,true);
				$client->setDomain($this->input->post('domain_name'),false,true);
				$client->update();
				
			}
			$this->postNext();
		}
		
		$this->template_data->set('current_client', $client->get());
		
		$this->template_data->set('output', $output);
		$this->load->view('clients/clients_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		$this->_isAuth('clients', 'clients', 'delete');

		$client = new $this->Clients_model;
		$client->setId($id, true);
		$client->delete();
		
		$layouts = new $this->Layouts_model;
		$layouts->setClientId($id,true);
		$layouts->delete();

		$menus = new $this->Menus_model;
		$menus->setClientId($id,true);

		foreach($menus->populate() as $menu) {
			$links = new $this->Menus_links_model;
			$links->setMenuId($menu->id,true);
			$links->delete();
		}
		$menus->delete();

		$pages = new $this->Pages_model;
		$pages->setClientId($id,true);
		$pages->delete();

		$options = new $this->Pages_options_model;
		$options->setClientId($id,true);
		$options->delete();

		$page_rows = new $this->Pages_rows_model;
		$page_rows->setClientId($id,true);
		$page_rows->delete();

		$page_containers = new $this->Pages_containers_model;
		$page_containers->setClientId($id,true);
		$page_containers->delete();

		$rows = new $this->Rows_model;
		$rows->setClientId($id,true);
		foreach($rows->populate() as $row) {
			$columns = new $this->Rows_columns_model;
			$columns->setRowId($row->id,true);
			$columns->delete();

			$widgets = new $this->Rows_columns_widgets_model;
			$widgets->setRowId($row->id,true);
			$widgets->delete();

			$settings = new $this->Rows_settings_model;
			$settings->setRowId($row->id,true);
			$settings->delete();
		}
		$rows->delete();

		$row_groups = new $this->Rows_groups_model;
		$row_groups->setClientId($id,true);
		$row_groups->delete();

		redirect("welcome");
	}

}
