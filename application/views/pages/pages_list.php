<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<div class="container">
<div class="row">
	<div class="col-md-12">

	    <div class="panel panel-default">
	    	<div class="panel-heading">

	    	
<form method="get">
<div class="input-group input-group-sm col-md-4 pull-right ">
      <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Search</button>
        <a data-title="Add Page" href="<?php echo site_url("pages/add/0/ajax"); ?>" class="btn btn-success btn-xs ajax-modal"><span class="glyphicon glyphicon-plus"></span> Add a Page</a>
      </span>
      
</div>
</form>

	    		<h3 class="panel-title">All Pages (<?php echo $pages_count; ?>)</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<div class="list-group">

  <?php if( $allpages ) foreach($allpages as $page) { ?>
      <div class="list-group-item <?php echo ($page->active==1) ? '' : 'danger'; ?>">
      <input type="hidden" name="page_order[]" value="<?php echo $page->id; ?>" />
        <div class="btn-group pull-right">
        <?php if($page->subpages > 0) { ?>
           <a type="button" class="btn btn-warning btn-xs" href="<?php echo site_url("pages/subpages/{$page->id}"); ?>">Sub Pages <?php echo ($page->subpages > 0) ? "(".$page->subpages.")" : ""; ?></a>
           <?php } ?>
          <a type="button" class="btn btn-success btn-xs" href="<?php echo site_url("pages/edit/{$page->id}"); ?>">Edit Page</a>
          <a type="button" class="btn btn-primary btn-xs" href="<?php echo site_url("pages/options/{$current_client->id}/{$page->id}"); ?>">Options</a>
          <a type="button" class="btn btn-warning btn-xs" href="<?php echo site_url("pages/rows/{$current_client->id}/{$page->id}"); ?>">Page Rows</a>
          <a type="button" class="btn btn-info btn-xs" href="<?php echo site_url("pages/rows/{$current_client->id}/{$page->id}"); ?>">Widgets</a>
        </div>
        <strong><?php echo $page->page_title; ?></strong>
        <a href="<?php echo site_url("pages/subpages/{$page->parent_page}"); ?>" class="badge" style="margin-right: 10px;"><?php echo $page->parent_page_title; ?></a>
      </div>
  <?php } ?>
</div>

<?php echo $pagination; ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>

    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>