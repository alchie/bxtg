jQuery(document).ready(function($) {
    var navbar = $('.navbar-main')
      , distance = navbar.offset().top
      , $window = $(window);
    $window.scroll(function() {
        if (($window.scrollTop() >= distance) && ($(".navbar-default").hasClass("navbar-main"))) {
            navbar.removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
            $("body").css("padding-top", "70px");
        } else {
            navbar.removeClass('navbar-fixed-top');
            $("body").css("padding-top", "0px");
        }
    });
});
