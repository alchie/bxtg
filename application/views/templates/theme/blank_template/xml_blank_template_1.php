<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";

function col_class($settings, $col_var, $prefix='', $else='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var > 0)) ? $prefix . $settings->$col_var : $else;
  return ($text!='') ? $text . " " : "";
  } 
function visible_class($settings, $col_var, $prefix='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var != '')) ? $prefix . $settings->$col_var : '';
  return ($text!='') ? $text . " " : "";
  }

function blogger_page_url($page, $current_client, $mobile=false) {
    $parent_slug = $page->page_slug;
    $url = "http://{$current_client->domain}/search/label/{$parent_slug}";
    return $url;
}

function display_rows($row, $ths) { ?>
<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>


  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>

<?php if( $row->display_rows ) { ?>
    <div class="row <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
<?php } ?>

<?php if(isset($row->columns_data)) { ?>
        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php } ?>
<?php if( $row->display_column_settings ) { ?>
         <div class="<?php 
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-'); 
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/theme/welfare/v5/widget_' . $widget->type, array('widget_options'=> json_decode($widget->options))); 
                ?>
              <?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>
<?php if( $row->display_column_settings ) { ?>
          </div>
<?php } ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>>  
<?php } ?>
        <?php } ?>
<?php } ?>
<?php if( $row->display_rows ) { ?>
    </div>
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>
<?php
function display_blogger_posts_rows($row, $ths) { ?>

<b:if cond='data:blog.pageType == &quot;index&quot;'>

          <div class="col-md-4 d-flex">
            <div class="blog-entry align-self-stretch">
              <b:if cond='data:post.firstImageUrl'>
               <a expr:href="data:post.url" class="block-20 gray-hover-colored" expr:id='&quot;index-&quot; + data:post.id' expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'></a>
                <meta expr:content='data:post.firstImageUrl' itemprop='image_url'/>
              </b:if>
              <meta expr:content='data:blog.blogId' itemprop='blogId'/>
              <meta expr:content='data:post.id' itemprop='postId'/>
              <a expr:name='data:post.id'/>
<?php if($row->blogger_postlist_textbox) { ?>
              <div class="text p-4 d-block">
                <h3 class="heading mt-3"><a expr:href="data:post.url"><data:post.title/></a></h3>
                <p><data:post.snippet/></p>
              </div>
<?php } ?>
            </div>
          </div>
</b:if>

<?php } ?>
<!DOCTYPE html>
<html b:version='2' class='v2' expr:dir='data:blog.languageDirection' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>
  <head>
    <meta content='width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0' name='viewport'/>
    <b:include data='blog' name='all-head-content'/>
<title><b:if cond='data:blog.url == data:blog.homepageUrl'><data:blog.pageTitle/>
<?php foreach( $pages as $page_id=>$page_data ) { 
  $page = $page_data['page']; 

?>
<?php if($page_id!='homepage') { 
$page_url = blogger_page_url($page, $current_client);
$page_url_m = blogger_page_url($page, $current_client, true);

  ?>
<b:elseif cond='data:blog.url == &quot;<?php echo $page_url; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_m; ?>&quot;'/><?php echo htmlentities($page->page_title); ?>
<?php } ?>
<?php } ?>
<b:else/>
<b:if cond='data:blog.pageType == &quot;item&quot; || data:blog.pageType == &quot;static_page&quot;'><data:blog.pageTitle/><b:else/><?php echo ( isset($error_404_page) && ($error_404_page) && (isset($error_404_page['page'])) && ($error_404_page['page']) ) ? $error_404_page['page']->page_title : 'Error 404 : Page not found!'; ?></b:if></b:if></title>
    

  
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Overpass:300,400,400i,600,700" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.3/aos.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.4.1/css/ionicons.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" />

    <b:skin><![CDATA[
<?php $this->load->view('templates/theme/welfare/_skin5'); ?>
      ]]></b:skin>
    <b:template-skin>
      <![CDATA[
<?php $this->load->view('templates/theme/welfare/_template5'); ?>
    <?php $additional_css = trim(get_layout_value('css', 'css', $layouts));
if($additional_css!='') { echo str_replace("\n", "", $additional_css); } ?>
    ]]>
</b:template-skin>
    <b:include data='blog' name='google-analytics'/>
    <?php echo get_layout_value('head', 'head', $layouts); ?>
  </head>

   <b:defaultmarkups>
      <b:defaultmarkup type='Common'>
<?php if(isset($rows)) foreach($rows as $row) { ?>
<?php if($row->blogger_main_includable) { ?>
  <b:includable id='posts-list-<?php echo $row->id; ?>' var='post'>
    <?php echo display_blogger_posts_rows($row, $this); ?>
  </b:includable>
<?php } else { ?>
  <b:includable id='common-row-<?php echo $row->id; ?>'>
    <?php echo display_rows($row, $this); ?>
  </b:includable>
<?php } ?>
<?php } ?>
<b:includable id='404-page-error'>
    <div class="hero-wrap" style="background-image: url(https://4.bp.blogspot.com/-YlzptlqQcZ4/W6ZCKZa9njI/AAAAAAAARBU/y19iW-PDHogwUxXEI8wVO_i2WliyqB8iACLcBGAs/s1600/IMG_51393.png);" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-7 text-center" data-scrollax=" properties: { translateY: '70%' }">
             <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Home</a></span> <span>404 Error</span></p>
            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Page not found!</h1>
          </div>
        </div>
      </div>
    </div>
</b:includable>

<b:includable id='post-main' var='post'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>

          <div class="col-md-4 d-flex">
            <div class="blog-entry align-self-stretch">
              <b:if cond='data:post.firstImageUrl'>
               <a expr:href="data:post.url" class="block-20 gray-hover-colored" expr:id='&quot;index-&quot; + data:post.id' expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'></a>
                <meta expr:content='data:post.firstImageUrl' itemprop='image_url'/>
              </b:if>
              <meta expr:content='data:blog.blogId' itemprop='blogId'/>
              <meta expr:content='data:post.id' itemprop='postId'/>
              <a expr:name='data:post.id'/>

              <div class="text p-4 d-block">
                <div class="meta mb-3">
                  <div><a class='timestamp-link' expr:href='data:post.url' rel='bookmark' title='permanent link'><abbr class='published' expr:title='data:post.timestamp' itemprop='datePublished dateModified'> <data:post.timestamp/></abbr></a></div>
                  <b:if cond='data:top.showAuthor'>
                  <div><a href="#"><data:post.author/></a></div>
                  </b:if>
                  <div>
<b:if cond='data:post.allowComments'>
<a class='comment-link' expr:href='data:post.addCommentUrl' expr:onclick='data:post.addCommentOnclick'><b:if cond='data:post.numComments == 1'>1 <data:top.commentLabel/><b:else/><data:post.numComments/> <data:top.commentLabelPlural/></b:if></a>
</b:if>
                  </div>
                </div>
                <h3 class="heading mt-3"><a expr:href="data:post.url"><data:post.title/></a></h3>
                <p><data:post.snippet/></p>
              </div>
            </div>
          </div>
</b:if>
</b:includable>

<b:includable id='post-gallery' var='post'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>
          <a expr:href="data:post.firstImageUrl" class="gallery image-popup d-flex justify-content-center align-items-center img" expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'>
          </a>
</b:if>
</b:includable>

<b:includable id='post-item-body' var='post'>
<b:if cond='data:blog.pageType == &quot;item&quot;'>

<b:if cond='data:post.firstImageUrl'>
    <div class="hero-wrap" expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;' data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-7 text-center" data-scrollax=" properties: { translateY: '70%' }">
             <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Home</a></span></p>
            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><data:post.title/></h1>
          </div>
        </div>
      </div>
    </div>
<b:else/>
<div class="hero-wrap" style="background-image: url(https://4.bp.blogspot.com/-YlzptlqQcZ4/W6ZCKZa9njI/AAAAAAAARBU/y19iW-PDHogwUxXEI8wVO_i2WliyqB8iACLcBGAs/s1600/IMG_51393.png);" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-7 text-center" data-scrollax=" properties: { translateY: '70%' }">
             <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">Home</a></span></p>
            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><data:post.title/></h1>
          </div>
        </div>
      </div>
    </div>
</b:if>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
<span expr:id='&quot;p&quot; + data:post.id'><data:post.body/></span>
          </div> <!-- .col-md-8 -->
        </div>
      </div>
    </section> <!-- .section -->

</b:if>
</b:includable>

      </b:defaultmarkup>
    </b:defaultmarkups>

<body expr:class='&quot;<?php echo (get_layout_value('page', 'width', $layouts)=='boxed') ? 'bodyColor container ' : ''; ?>loading&quot; + data:blog.mobileClass'>
 
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="<?php echo get_layout_value('header', 'site_url', $layouts); ?>"><?php echo get_layout_value('header', 'site_title', $layouts); ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"><a href="<?php echo get_layout_value('header', 'site_url', $layouts); ?>" class="nav-link">Home</a></li>
<?php 
$header_nav = get_menu_links( get_layout_value('header', 'header_nav', $layouts) );
foreach( $header_nav as $hnav ) {

    if( $hnav->active != 1 ) {
    continue;
  }

?>
 <li class="nav-item"><a href="<?php echo ($hnav->inner_page) ? "http://{$current_client->domain}/search/label/{$hnav->page_slug}" : $hnav->url; ?>" <?php echo ($hnav->target!='') ? 'target="'.$hnav->target.'"' : ''; ?> class="nav-link"><?php echo htmlentities($hnav->text); ?></a></li>
<?php } ?>
        </ul>
      </div>
    </div>
  </nav>
    <!-- END nav -->


          <b:section class='main post_section' id='main' name='Main' showaddelement='no'>
            <b:widget id='Blog1' locked='true' title='Blog Posts' type='Blog' class='container'>
              <b:widget-settings>
                <b:widget-setting name='showDateHeader'>false</b:widget-setting>
                <b:widget-setting name='style.textcolor'>#666666</b:widget-setting>
                <b:widget-setting name='showShareButtons'>false</b:widget-setting>
                <b:widget-setting name='showCommentLink'>false</b:widget-setting>
                <b:widget-setting name='style.urlcolor'>#4d469c</b:widget-setting>
                <b:widget-setting name='showAuthor'>false</b:widget-setting>
                <b:widget-setting name='style.linkcolor'>#2198a6</b:widget-setting>
                <b:widget-setting name='style.unittype'>TextAndImage</b:widget-setting>
                <b:widget-setting name='style.bgcolor'>#ffffff</b:widget-setting>
                <b:widget-setting name='showAuthorProfile'>false</b:widget-setting>
                <b:widget-setting name='style.layout'>1x1</b:widget-setting>
                <b:widget-setting name='showLabels'>false</b:widget-setting>
                <b:widget-setting name='showLocation'>false</b:widget-setting>
                <b:widget-setting name='showTimestamp'>false</b:widget-setting>
                <b:widget-setting name='postsPerAd'>0</b:widget-setting>
                <b:widget-setting name='showBacklinks'>false</b:widget-setting>
                <b:widget-setting name='style.bordercolor'>#ffffff</b:widget-setting>
                <b:widget-setting name='showInlineAds'>false</b:widget-setting>
                <b:widget-setting name='showReactions'>false</b:widget-setting>
              </b:widget-settings>

<b:includable id='main' var='top'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>
 <?php foreach( $pages as $page_id=>$page_data ) { 
        $page = $page_data['page']; 
        $options = $page_data['options'];
        $page_rows = $page_data['rows'];

        $page_url = blogger_page_url($page, $current_client);
        $page_url_m = blogger_page_url($page, $current_client, true);

        ?>
      <?php if($page_id=='homepage') { ?>
      <b:if cond='data:blog.url == data:blog.homepageUrl'>
        <?php if( $page_rows ) foreach($page_rows as $page_row) { ?>
          <?php if( $page_row->blogger_main_includable ) { ?>
            <section class="ftco-section <?php echo $page_row->wrapper_class; ?>">
              <div class="container <?php echo $page_row->container_class; ?>">
                <div class="row d-flex <?php echo $page_row->row_class; ?>">
                  <b:loop values='data:posts' var='post'>
                      <b:include data='post' name='<?php echo ( $page_row->blogger_post_name != '' ) ? $page_row->blogger_post_name : 'posts-list-' . $page_row->row_id; ?>'/>
                  </b:loop>
                </div>
              </div>
            </section>
          <?php } else { ?>
            <b:include name='common-row-<?php echo $page_row->row_id; ?>'/>
          <?php } ?>
        <?php } ?>
      <?php } else { ?>
      <b:elseif cond='data:blog.url == &quot;<?php echo $page_url; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_m; ?>&quot;'/>
        <?php if( $page_rows ) foreach($page_rows as $page_row) { ?>
          <?php if( $page_row->blogger_main_includable ) { ?>
            <section class="ftco-section <?php echo $page_row->wrapper_class; ?>">
              <div class="container <?php echo $page_row->container_class; ?>">
                <div class="row d-flex <?php echo $page_row->row_class; ?>">
                  <?php if( $page_row->row_display_title ) { ?>
                    <div class="col-md-12 heading-section text-center">
                      <h2 class="mb-4"><?php echo $page_row->title; ?></h2>
                    </div>
                  <?php } ?>
                  <b:loop values='data:posts' var='post'>
                      <b:include data='post' name='<?php echo ( $page_row->blogger_post_name != '' ) ? $page_row->blogger_post_name : 'posts-list-' . $page_row->row_id; ?>'/>
                  </b:loop>
                </div>
              </div>
            </section>
          <?php } else { ?>
            <b:include name='common-row-<?php echo $page_row->row_id; ?>'/>
          <?php } ?>
        <?php } ?>
      <?php } ?>
      <?php } ?>      
      </b:if>
<b:elseif cond='data:blog.pageType == &quot;error_page&quot;'/>
  <?php if($error_404_page) { ?>
        <?php if( $error_404_page['rows'] ) foreach($error_404_page['rows'] as $page_row) { ?>
              <b:include name='common-row-<?php echo $page_row->row_id; ?>'/>
        <?php } ?>
      <?php } else { ?>
        <b:include name='404-page-error'/>
      <?php } ?>
<b:elseif cond='data:blog.pageType == &quot;item&quot; || data:blog.pageType == &quot;static_page&quot;'/>
  <b:loop values='data:posts' var='post'>
    <b:include data='post' name='post-item-body'/>
  </b:loop>
</b:if>
</b:includable>


              <b:includable id='backlinkDeleteIcon' var='backlink'></b:includable>
              <b:includable id='backlinks' var='post'></b:includable>
              <b:includable id='comment-form' var='post'></b:includable>
              <b:includable id='commentDeleteIcon' var='comment'></b:includable>
              <b:includable id='comment_count_picker' var='post'></b:includable>
              <b:includable id='comment_picker' var='post'></b:includable>
              <b:includable id='comments' var='post'></b:includable>
              <b:includable id='feedLinks'></b:includable>
              <b:includable id='feedLinksBody' var='links'></b:includable>
              <b:includable id='iframe_comments' var='post'></b:includable>
              <b:includable id='mobile-index-post' var='post'></b:includable>
              <b:includable id='mobile-main' var='top'></b:includable>
              <b:includable id='mobile-nextprev'></b:includable>
              <b:includable id='mobile-post' var='post'></b:includable>
              <b:includable id='nextprev'></b:includable>
              <b:includable id='postQuickEdit' var='post'></b:includable>
              <b:includable id='shareButtons' var='post'></b:includable>
              <b:includable id='status-message'></b:includable>
              <b:includable id='threaded-comment-form' var='post'></b:includable>
              <b:includable id='threaded_comment_js' var='post'></b:includable>
              <b:includable id='threaded_comments' var='post'></b:includable>
              <b:includable id='post' var='post'></b:includable>

            </b:widget>
          </b:section>



          <macro:include id='main-column-left-sections' name='sections'>
            <macro:param default='0' name='num' value='1'/>
            <macro:param default='sidebar' name='idPrefix'/>
            <macro:param default='sidebar' name='class'/>
            <macro:param default='false' name='includeBottom'/>
          </macro:include>

<macro:includable id='sections' var='col'>
    <b:section mexpr:class='data:col.class' mexpr:id='data:col.idPrefix + &quot;-1&quot;' preferred='yes' showaddelement='yes'/>
</macro:includable>



 
  <footer class="ftco-footer ftco-section img" style="padding: 5px; font-size: 10px;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">

           <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Designed with love by <a href="https://colorlib.com" target="_blank">Colorlib</a> and developed with love by <a href="https://www.chesteralan.com" target="_blank">Chester Alan</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </div>
        </div>
      </div>
    </footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/stellar.js/0.6.2/jquery.stellar.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.3/aos.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js"></script>
  <script><!--
 <?php $this->load->view('templates/theme/welfare/_script5'); ?>
 //-->
  </script> 
<?php echo get_layout_value('foot', 'foot', $layouts); ?>

</body>
</html>