<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo (isset($page)&&($page)) ? $page->page_title : $current_client->name; ?></title>

  <link href="<?php echo base_url('assets/templates/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
<?php 
$additional_css = trim(get_layout_value('css', 'css', $layouts));
if($additional_css!='') { ?>
<style>
  <!--
<?php echo $additional_css; ?>
  -->
</style>
<?php } ?>
<?php echo get_layout_value('head', 'head', $layouts); ?>

</head>
<body>


<?php function display_rows($rows, $ths) { ?>
<?php foreach( $rows as $row ) { ?>

<?php if($row->blogger_main_includable) { ?>
  <div style="border: 5px solid red; padding: 20px;margin: 20px; text-align: center;">
    <?php echo $row->title; ?> will be inserted here...
  </div>
<?php 
  continue;
} ?>

<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>
<?php if( $row->display_rows ) { ?>

  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>

    <div class="row <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
<?php if(isset($row->columns_data)) { ?>
        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php } ?>
          <div class="<?php 
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-'); 
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/theme/royal/royal_' . $widget->type, array(
                          'widget_id'=>$widget->widget_id,
                          'widget_options'=> json_decode($widget->options)
                       )); 
                ?>
              <?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>
          </div>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>>  
<?php } ?>
        <?php } ?>
<?php } ?>
    </div>
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>
<?php } ?>


  <!-- JQUERY SCRIPTS -->
  <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 115px;
    left: -10px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="blogger_xml" href="<?php echo site_url("templates/generate_blogger/{$current_client->id}/{$template_name}/{$xml_version}"); ?>" class="">XML</a>

</div>

<?php if((isset($page)) && ($page)) { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 190px;
    left: -25px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/" . $page->id ); ?>" class="">Edit Page</a>
<?php } else { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 325px;
    left: -44px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/0" ); ?>" class="">Edit Homepage</a>
<?php } ?>
</div>

  </body>
</html>