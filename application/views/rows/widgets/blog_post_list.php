<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

$items = false;
$i=0;
if( isset($widget) && ($widget) ) {
    $options = json_decode( $widget->options );
}
?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<script>
    <!--
        var link_count = <?php echo $i; ?>;
    -->
</script>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">List Widget
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>
                </h3>
            </div>
          
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
   <div class="col-md-12">
<div class="form-group">
<label class="pull-right"><input name="show_title" type="checkbox" value="1" <?php echo (isset($options)&&(isset($options->show_title))&&($options->show_title==1)) ? 'CHECKED' : ''; ?>> Show Title</label>
    <label>Title</label>
    <input type="text" class="form-control" name="title" placeholder="Title" value="<?php echo (isset($options)&&isset($options->title)) ? $options->title : ''; ?>">
</div>
    </div>
    <div class="col-md-3">
                <div class="form-group">
                    <label>Title Header Tag</label>
                    <input name="title_tag" type="text" class="form-control" value="<?php echo isset($options->title_tag) ? $options->title_tag : 'h3'; ?>">
                </div>
    </div>
    <div class="col-md-3">
                <div class="form-group">
                    <label>Title Class</label>
                    <input name="title_class" type="text" class="form-control" value="<?php echo isset($options->title_class) ? $options->title_class : ''; ?>">
                </div>
    </div>
    <div class="col-md-3">
                <div class="form-group">
                    <label>Container Class</label>
                    <input name="container_class" type="text" class="form-control" value="<?php echo isset($options->container_class) ? $options->container_class : ''; ?>">
                </div>
    </div>

    <div class="col-md-3">
                <div class="form-group">
                    <label>Widget Class</label>
                    <input name="widget_class" type="text" class="form-control" value="<?php echo isset($options->widget_class) ? $options->widget_class : ''; ?>">
                </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
                <div class="form-group">
                    <label>Blogger Label</label>
                    <input name="blog_list_label" type="text" class="form-control" value="<?php echo isset($options->blog_list_label) ? $options->blog_list_label : ''; ?>">
                </div>
    </div>

    <div class="col-md-4">
      
                <div class="form-group">
                    <label>Javascript Method</label>
                    <select name="javascript_method" class="form-control javascript_method">
                        <?php foreach(array(
                          'blog_list_default', 
                          'blog_list_sidebar', 
                          'blog_list_titles', 
                          'blog_list_thumbtitles', 
                        ) as $method) { ?>
                            <option value="<?php echo $method; ?>" <?php echo (isset($options->javascript_method) && ($options->javascript_method==$method)) ? 'SELECTED' : ''; ?>><?php echo $method; ?></option>
                      <?php } ?>
                    </select>
                </div>
    </div>
    <div class="col-md-4">
                <div class="form-group">
                    <label>Custom Javascript Method</label>
                    <input name="custom_method" type="text" class="form-control" value="<?php echo isset($options->custom_method) ? $options->custom_method : ''; ?>">
                </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
                <div class="form-group">
                    <label>Max Results</label>
                    <input name="blog_list_limit" type="text" class="form-control" value="<?php echo isset($options->blog_list_limit) ? $options->blog_list_limit : '10'; ?>">
                </div>
    </div>
    <div class="col-md-3">
                <div class="form-group">
                    <label>Start</label>
                    <input name="blog_list_start" type="text" class="form-control" value="<?php echo isset($options->blog_list_start) ? $options->blog_list_start : '1'; ?>">
                </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
                <div class="form-group">
                    <label>Widget ID</label>
                    <input name="widget_id" type="text" class="form-control" value="<?php echo isset($options->widget_id) ? $options->widget_id : ''; ?>">
                </div>
    </div>
</div>



<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="linkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Link</h4>
      </div>
      <div class="modal-body">
        
    <div class="form-group">
        <label for="link_text">Text</label>
        <input type="text" class="form-control" placeholder="Text" id="link_text">
    </div>

    <div class="form-group">
        <label for="link_desc">Description</label>
        <textarea class="form-control" placeholder="Description" id="link_desc"></textarea>
    </div>

<div class="form-group">
        <label for="link_link">Link</label>
        <input type="text" class="form-control" placeholder="Link" id="link_link">
    </div>

<div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label for="link_icon">Icon</label>
          <input type="text" class="form-control" placeholder="Icon" id="link_icon">
      </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="link_class">Class</label>
            <input type="text" class="form-control" placeholder="Class" id="link_class">
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label for="link_page">Inner Page</label>
          <select name="link_page" class="form-control" id="link_page">
              <option value="0">No Inner Page</option>
              <?php 
              foreach($pages as $page) { ?>
                  <option id="link_page_item_<?php echo $page->id; ?>" value="<?php echo $page->id; ?>"><?php echo $page->page_title; ?> (<?php echo $page->page_slug; ?>)</option>
              <?php } ?>
          </select>
      </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
        <label for="link_target">Link Target</label>
        <input type="text" class="form-control" placeholder="Link Target" id="link_target">
    </div>
    </div>
</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="widget_link_submit">Submit</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>