<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rows_columns_model Class
 *
 * Manipulates `rows_columns` table on database

CREATE TABLE `rows_columns` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `row_id` int(20) NOT NULL,
  `column` int(2) DEFAULT 1,
  `class` varchar(200) DEFAULT NULL,
  `container` varchar(200) DEFAULT NULL,
  `wrapper` varchar(200) DEFAULT NULL,
  `more_settings` text DEFAULT NULL,
  `column_order` int(2) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `page_id` (`row_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

ALTER TABLE  `rows_columns` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `rows_columns` ADD  `row_id` int(20) NOT NULL   ;
ALTER TABLE  `rows_columns` ADD  `column` int(2) NULL   DEFAULT '1';
ALTER TABLE  `rows_columns` ADD  `class` varchar(200) NULL   ;
ALTER TABLE  `rows_columns` ADD  `container` varchar(200) NULL   ;
ALTER TABLE  `rows_columns` ADD  `wrapper` varchar(200) NULL   ;
ALTER TABLE  `rows_columns` ADD  `more_settings` text NULL   ;
ALTER TABLE  `rows_columns` ADD  `column_order` int(2) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Rows_columns_model extends MY_Model {

	protected $id;
	protected $row_id;
	protected $column;
	protected $class;
	protected $container;
	protected $wrapper;
	protected $more_settings;
	protected $column_order;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'rows_columns';
		$this->_short_name = 'rows_columns';
		$this->_fields = array("id","row_id","column","class","container","wrapper","more_settings","column_order");
		$this->_required = array("row_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: row_id -------------------------------------- 

	/** 
	* Sets a value to `row_id` variable
	* @access public
	*/

	public function setRowId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_row_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('row_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `row_id` variable
	* @access public
	*/

	public function getRowId() {
		return $this->row_id;
	}

	public function get_row_id() {
		return $this->row_id;
	}

	
// ------------------------------ End Field: row_id --------------------------------------


// ---------------------------- Start Field: column -------------------------------------- 

	/** 
	* Sets a value to `column` variable
	* @access public
	*/

	public function setColumn($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_column($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `column` variable
	* @access public
	*/

	public function getColumn() {
		return $this->column;
	}

	public function get_column() {
		return $this->column;
	}

	
// ------------------------------ End Field: column --------------------------------------


// ---------------------------- Start Field: class -------------------------------------- 

	/** 
	* Sets a value to `class` variable
	* @access public
	*/

	public function setClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_class($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `class` variable
	* @access public
	*/

	public function getClass() {
		return $this->class;
	}

	public function get_class() {
		return $this->class;
	}

	
// ------------------------------ End Field: class --------------------------------------


// ---------------------------- Start Field: container -------------------------------------- 

	/** 
	* Sets a value to `container` variable
	* @access public
	*/

	public function setContainer($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_container($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('container', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `container` variable
	* @access public
	*/

	public function getContainer() {
		return $this->container;
	}

	public function get_container() {
		return $this->container;
	}

	
// ------------------------------ End Field: container --------------------------------------


// ---------------------------- Start Field: wrapper -------------------------------------- 

	/** 
	* Sets a value to `wrapper` variable
	* @access public
	*/

	public function setWrapper($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('wrapper', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_wrapper($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('wrapper', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `wrapper` variable
	* @access public
	*/

	public function getWrapper() {
		return $this->wrapper;
	}

	public function get_wrapper() {
		return $this->wrapper;
	}

	
// ------------------------------ End Field: wrapper --------------------------------------


// ---------------------------- Start Field: more_settings -------------------------------------- 

	/** 
	* Sets a value to `more_settings` variable
	* @access public
	*/

	public function setMoreSettings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('more_settings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_more_settings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('more_settings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `more_settings` variable
	* @access public
	*/

	public function getMoreSettings() {
		return $this->more_settings;
	}

	public function get_more_settings() {
		return $this->more_settings;
	}

	
// ------------------------------ End Field: more_settings --------------------------------------


// ---------------------------- Start Field: column_order -------------------------------------- 

	/** 
	* Sets a value to `column_order` variable
	* @access public
	*/

	public function setColumnOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column_order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_column_order($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('column_order', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `column_order` variable
	* @access public
	*/

	public function getColumnOrder() {
		return $this->column_order;
	}

	public function get_column_order() {
		return $this->column_order;
	}

	
// ------------------------------ End Field: column_order --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'row_id' => (object) array(
										'Field'=>'row_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'column' => (object) array(
										'Field'=>'column',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'class' => (object) array(
										'Field'=>'class',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'container' => (object) array(
										'Field'=>'container',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'wrapper' => (object) array(
										'Field'=>'wrapper',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'more_settings' => (object) array(
										'Field'=>'more_settings',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'column_order' => (object) array(
										'Field'=>'column_order',
										'Type'=>'int(2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `rows_columns` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'row_id' => "ALTER TABLE  `rows_columns` ADD  `row_id` int(20) NOT NULL   ;",
			'column' => "ALTER TABLE  `rows_columns` ADD  `column` int(2) NULL   DEFAULT '1';",
			'class' => "ALTER TABLE  `rows_columns` ADD  `class` varchar(200) NULL   ;",
			'container' => "ALTER TABLE  `rows_columns` ADD  `container` varchar(200) NULL   ;",
			'wrapper' => "ALTER TABLE  `rows_columns` ADD  `wrapper` varchar(200) NULL   ;",
			'more_settings' => "ALTER TABLE  `rows_columns` ADD  `more_settings` text NULL   ;",
			'column_order' => "ALTER TABLE  `rows_columns` ADD  `column_order` int(2) NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setRowId() - row_id
//setColumn() - column
//setClass() - class
//setContainer() - container
//setWrapper() - wrapper
//setMoreSettings() - more_settings
//setColumnOrder() - column_order

--------------------------------------

//set_id() - id
//set_row_id() - row_id
//set_column() - column
//set_class() - class
//set_container() - container
//set_wrapper() - wrapper
//set_more_settings() - more_settings
//set_column_order() - column_order

*/
/* End of file Rows_columns_model.php */
/* Location: ./application/models/Rows_columns_model.php */
