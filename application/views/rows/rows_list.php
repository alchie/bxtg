<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-3">

        <div class="panel panel-default">
            <div class="panel-heading">
                <a data-title="Add Group" href="<?php echo site_url("rows/add_group/ajax"); ?>" class="pull-right btn btn-primary btn-xs ajax-modal">Add Group</a>



                <h3 class="panel-title">Groups</h3>
            </div>
            
            <div class="panel-body">

<div class="list-group" style="margin-bottom: 0px;">
     
        <a class="list-group-item <?php echo ($group_id===false) ? 'active':''; ?>" href="<?php echo site_url("rows"); ?>">All Rows</a>
        <?php foreach($groups as $group) { ?>
            <a class="list-group-item <?php echo ($group_id==$group->group_id) ? 'active':''; ?>" href="<?php echo site_url("rows/group/{$group->group_id}"); ?>"><?php echo $group->group_name; ?></a>
        <?php } ?>
</div>
            </div>
        </div>

    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                

<form method="get">
<div class="input-group input-group-sm col-md-4 pull-right ">
      <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Search</button>
        <a data-title="Create New Row" href="<?php echo site_url("rows/add_row/ajax"); ?>?group=<?php echo (isset($group_id)) ? $group_id : '0'; ?>" class="btn btn-success btn-xs ajax-modal">Create New Row</a>
      </span>
      
</div>
</form>
                <h3 class="panel-title"><?php echo (isset($current_group) && ($current_group)) ? $current_group->group_name : 'Rows'; ?> (<?php echo $rows_count; ?>)

<?php if(isset($current_group) ) { ?>
<div class="btn-group" role="group">
<a href="<?php echo site_url("rows/edit_group/{$current_group->group_id}") . "?next=" . uri_string(); ?>" class="btn btn-warning btn-xs">Edit Group</a>
<a href="<?php echo site_url("rows/delete_group/{$current_group->group_id}"); ?>" class="confirm btn btn-danger btn-xs">Delete Group</a>
</div>
<?php } ?>
                </h3>


            </div>
            
            <div class="panel-body">
<?php endif; ?>

<?php function display_rows($rows, $group_id) { ?>
<ul class="list-group" style="margin-bottom: 0px;">
<?php foreach($rows as $row) { ?>
    <li class="list-group-item">

       <a href="<?php echo site_url("rows/edit_row/{$row->id}") . "?next=" . uri_string(); ?>"><?php echo $row->title; ?> 
       <?php if(($group_id==false) && ($row->group_name != NULL)) { ?>
            <span class="badge"><?php echo $row->group_name; ?></span>
       <?php } ?>
       </a>



        <input type="hidden" name="order[]" value="<?php echo $row->id; ?>">

<div class="pull-right">

<?php if( $row->display_rows == 1 ) { ?>
<div class="btn-group">
    <?php for($col=1;$col<=$row->columns;$col++) { ?>
        <a href="<?php echo site_url("rows/widgets/{$row->id}/{$col}"); ?>" class="btn btn-default btn-xs"><?php echo $col; ?></a>
    <?php } ?>
</div>
<?php } ?>

        <a href="<?php echo site_url("rows/edit_row/{$row->id}"); ?>" class="btn btn-warning btn-xs">Edit</a>
<?php if( $row->active == 1 ) { ?>
        <a href="<?php echo site_url("rows/deactivate_row/{$row->id}"); ?>" class="btn btn-danger btn-xs confirm">Deactivate</a>
<?php } else { ?>
        <a href="<?php echo site_url("rows/activate_row/{$row->id}"); ?>" class="btn btn-success btn-xs confirm">Activate</a>
<?php } ?>

</div>

        <span class="clearfix"></span>
<?php 
    if($row->children) {
        echo "<br>";
        display_rows($row->children, $group_id);
    } 
?>
    </li>
<?php } ?>
</ul>
<?php } 
display_rows($rows, $group_id);
?>
<br>
<?php echo $pagination; ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>

</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>