<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
 <form method="post">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Select a Widget Type</h3>
            </div>
           
            

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

    <div class="list-group">

<?php $widget_types = unserialize( WIDGET_TYPES ); ?>
<?php foreach($widget_types as $key=>$type) { ?>
    <a href="<?php echo site_url("rows/add_widgets/{$row->id}/{$column}/{$key}"); ?>" class="list-group-item"><?php echo $type; ?></a>
<?php } ?>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            
        </div>
    </div>
    </form>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>