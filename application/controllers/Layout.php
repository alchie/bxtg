<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template_data->set('current_page', 'Layouts');
		$this->template_data->set('current_uri', 'layouts');

		$this->_isAuth('clients', 'clients', 'view');
		$this->_isAuth('clients', 'clients', 'edit');

		$this->check_current_client();

	}

	public function index($id,$output='') {

		$clients = new $this->Clients_model;
		$clients->setId($this->session->userdata('current_client'),true);
		$this->template_data->set('current_client', $clients->get());

		if( $this->input->post('layout') ) {
			foreach( $this->input->post('layout') as $location=>$elements) {
				foreach( $elements as $key=>$value ) {
					$layouts = new $this->Layouts_model;
					$layouts->setClientId($this->session->userdata('current_client'),true);
					$layouts->setLocation($location,true);
					$layouts->setLayoutKey($key,true);
					$layouts->setLayoutValue($value);
						if( $layouts->nonEmpty() ) {
							$value = trim($value);
							if( ($value == '') || (empty($value)) ) {
								$layouts->delete();
							} else {
								$layouts->update();
							}
						} else {
							if( ($value != '') || (!empty($value)) ) {
								$layouts->set_exclude('id');
								$layouts->insert();
							}
						}
				}
			}

			if( $this->input->post('page_order') ) {
				foreach( $this->input->post('page_order') as $order=>$page_id ) {
					$page = new $this->Pages_model;
					$page->setId($page_id, true);
					$page->setOrder($order,false,true);
					$page->update();
				}
			}
		}

		$layouts = new $this->Layouts_model;
		$layouts->setClientId($this->session->userdata('current_client'),true);
		$layouts->set_limit(0);
		$this->template_data->set('layouts', $layouts->populate());


		$this->template_data->set('templates', $this->config->item('bxtg_templates'));

		$menus = new $this->Menus_model;
		$menus->setClientId($this->session->userdata('current_client'),true);
		$menus->set_select("*");
		$menus->set_select("(SELECT COUNT(*) FROM menus_links WHERE menu_id=menus.id) as total_links");
		$this->template_data->set('menus', $menus->populate());

		$pages = new $this->Pages_model('p');
		$pages->setClientId($this->session->userdata('current_client'),true);
		$pages->set_limit(0);
		$pages->set_select('p.*');
		$pages->set_order('p.page_title', 'ASC');
         $pages->set_select('(SELECT COUNT(*) FROM pages s WHERE s.parent_page=p.id) as subpages');
		$this->template_data->set('pages', $pages->recursive('p.parent_page', 0));

		$images = new $this->Images_model;
        $images->setClientId( $this->session->userdata('current_client') );
        $this->template_data->set('images', $images->populate());
        
		$this->load->view('layout/layout_list', $this->template_data->get_data());
	}



}
