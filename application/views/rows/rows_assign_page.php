<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Assign a Page</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<ul class="list-group">
<?php function display_li($page, $row_id, $pre="") { ?>
                    <li class="list-group-item"><?php echo $pre; ?>

    <a href="<?php echo site_url("rows/add_page_row/{$row_id}/{$page->id}"); ?>" class="btn btn-success btn-xs pull-right">Assign</a>

                    <a href="<?php echo site_url("pages/rows/{$page->client_id}/{$page->id}"); ?>"><?php echo $page->page_title; ?></a>
                    </li>

                   <?php if( $page->children ) foreach($page->children as $page1) { display_li($page1, $row_id, '- - - - '); } ?> 

<?php } ?>
<?php 
if( $row_pages ) {
    foreach($row_pages as $page) { display_li($page, $row_id); } 
} else {
    echo "<p class='text-center'>No Page to Assign</p>";
}
?>
</ul>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("rows"); ?>" class="btn btn-warning">Back</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>