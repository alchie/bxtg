<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo (isset($page)&&($page)) ? $page->page_title : $current_client->name; ?></title>

  <link href="<?php echo base_url('assets/templates/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/select_option1/select_option1.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/flexslider/flexslider.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/fullcalendar/fullcalendar.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/animate/animate.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/royal_college/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/templates/royal_college/default.css'); ?>" rel="stylesheet">
<?php 
$additional_css = trim(get_layout_value('css', 'css', $layouts));
$additional_css_lg = trim(get_layout_value('css', 'css_lg', $layouts));
$additional_css_md = trim(get_layout_value('css', 'css_md', $layouts));
$additional_css_sm = trim(get_layout_value('css', 'css_sm', $layouts));
$additional_css_xs = trim(get_layout_value('css', 'css_xs', $layouts));
?>
<style>
  <!--
<?php if($additional_css!='') { ?>
<?php echo $additional_css; ?>
<?php } ?>
<?php if($additional_css_xs!='') { ?>
@media (max-width: 767px) {
<?php echo $additional_css_xs; ?>
}
<?php } ?>
<?php if($additional_css_sm!='') { ?>
@media (min-width: 768px) {
<?php echo $additional_css_sm; ?>
}
<?php } ?>
<?php if($additional_css_md!='') { ?>
@media (min-width: 992px) {
<?php echo $additional_css_md; ?>
}
<?php } ?>
<?php if($additional_css_lg!='') { ?>
@media (min-width: 1200px) {
<?php echo $additional_css_lg; ?>
}
<?php } ?>
  -->
</style>

<?php echo get_layout_value('head', 'head', $layouts); ?>

</head>
<body class="<?php echo (get_layout_value('page', 'width', $layouts)=='boxed') ? 'bodyColor container' : ''; ?>">

  <div class="main_wrapper">

<?php if(get_layout_value('top_nav', 'status', $layouts)==1) { ?>
    <div class="topbar clearfix">
      <div class="container">
        <ul class="topbar-left">
          <li class="phoneNo"><i class="fa fa-phone"></i><?php echo get_layout_value('top_nav', 'phone_number', $layouts); ?></li>
          <li class="email-id hidden-xs hidden-sm"><i class="fa fa-envelope"></i>
            <a href="mailto:<?php echo get_layout_value('top_nav', 'email_address', $layouts); ?>"><?php echo get_layout_value('top_nav', 'email_address', $layouts); ?></a>
          </li>
        </ul>
        <ul class="topbar-right">
<?php 
$social_media = get_menu_links( get_layout_value('top_nav', 'social_media', $layouts) );
foreach( $social_media as $sm ) {
?>
          <li class="hidden-xs"><a href="<?php echo $sm->url; ?>"><i class="<?php echo $sm->icon; ?>"></i></a></li>
<?php } ?>          
<?php if( get_layout_value('top_nav', 'search_url', $layouts) ) { ?>
          <li class="dropdown top-search list-inline">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-search"></i>
            </a>
            <ul class="dropdown-menu">
              <form action="<?php echo get_layout_value('top_nav', 'search_url', $layouts); ?>" method="post">
                <input type="text" placeholder="Course Name" id="exampleInputEmail1" class="form-control">
                <button class="btn btn-default commonBtn" type="submit">Search</button>
              </form>
            </ul>
          </li>
<?php } ?>
        </ul>
      </div>
    </div>
<?php } ?>


    <div class="header clearfix">
      <nav class="navbar navbar-default <?php echo ( get_layout_value('header', 'nav_effects', $layouts) == 'fixed' ) ? 'navbar-main' : ''; ?>">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="header_inner">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
<?php if( get_layout_value('header', 'site_url', $layouts) ) { ?>
                  <a class="navbar-brand logo clearfix" href="<?php echo site_url("templates/view/{$current_client->id}/{$template_override}"); ?>" style="<?php echo (get_layout_value('header', 'site_logo_image', $layouts)&&get_layout_value('header', 'site_logo_width', $layouts)) ? "width:".get_layout_value('header', 'site_logo_width', $layouts) : ''; ?>">
                    <?php if( get_layout_value('header', 'site_logo_image', $layouts) ) { 
$image_id = get_layout_value('header', 'site_logo_image', $layouts);
$image_data = get_image($image_id);
                      ?>
                      <img src="<?php echo base_url("uploads/".$image_data->file_name); ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>">
                    <?php } else { ?>
                      <img src="<?php echo base_url("assets/templates/royal_college/images/logo.png"); ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>">
                    <?php } ?>
                  </a>
<?php } else { ?>
     <span class="navbar-brand logo clearfix" style="<?php echo (get_layout_value('header', 'site_logo_image', $layouts)&&get_layout_value('header', 'site_logo_width', $layouts)) ? "width:".get_layout_value('header', 'site_logo_width', $layouts) : ''; ?>">
            <?php if( get_layout_value('header', 'site_logo_image', $layouts) ) { 
$image_id = get_layout_value('header', 'site_logo_image', $layouts);
$image_data = get_image($image_id);
                      ?>
                      <img src="<?php echo base_url("uploads/".$image_data->file_name); ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>">
                    <?php } else { ?>
                      <img src="<?php echo base_url("assets/templates/royal_college/images/logo.png"); ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>">
                    <?php } ?>
      </span>
<?php }  ?>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="main-nav">
                  <ul class="nav navbar-nav navbar-right">
<?php 
$menu = get_menu( get_layout_value('header', 'header_nav', $layouts) );
$is_home_link = get_menus_meta($menu->id, "home_link");
if( $is_home_link ) {
?>
                    <li><a href="<?php echo site_url("templates/view/{$current_client->id}/{$template_override}/0/homepage"); ?>">Home</a></li>
<?php } ?>
<?php 
$header_nav = get_menu_links( get_layout_value('header', 'header_nav', $layouts) );
foreach( $header_nav as $hnav ) { 
  if( $hnav->active != 1 ) {
    continue;
  }
?>
          <?php if( $hnav->children ) { ?>
              <li class="dropdown list-inline">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $hnav->text; ?></a>
                   <ul class="dropdown-menu">
                      <?php foreach($hnav->children as $hnav_child) {  
  if( $hnav_child->active != 1 ) {
    continue;
  }
                       ?>
                          <?php if( $hnav_child->children ) { ?>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $hnav_child->text; ?></a>
                              <ul class="dropdown-menu">
                                <?php foreach($hnav_child->children as $sub_child) { 
                                  ?>
                                    <li><a href="<?php echo ($sub_child->inner_page) ? site_url("templates/view/{$current_client->id}/{$template_override}") . "/" . $sub_child->inner_page . $sub_child->url : $sub_child->url; ?>" ><?php echo $sub_child->text; ?></a></li>
                                <?php } ?>
                              </ul>
                            </li>
                          <?php } else { ?>
                            <li><a href="<?php echo ($hnav_child->inner_page) ? site_url("templates/view/{$current_client->id}/{$template_override}") . "/" . $hnav_child->inner_page . $hnav_child->url : $hnav_child->url; ?>"><?php echo $hnav_child->text; ?></a></li>
                          <?php } ?>
                      <?php } ?>
                   </ul>
              </li>
          <?php } else { ?>
              <li><a href="<?php echo ($hnav->inner_page) ? site_url("templates/view/{$current_client->id}/{$template_override}") . "/" . $hnav->inner_page . $hnav->url : $hnav->url; ?>"><?php echo $hnav->text; ?></a></li>
          <?php } ?>
<?php } ?>  
<?php if( get_layout_value('header', 'special_button_url', $layouts) ) { ?>
                  <li class="apply_now"><a href="<?php echo get_layout_value('header', 'special_button_url', $layouts); ?>" ><?php echo get_layout_value('header', 'special_button_text', $layouts); ?></a></li>
<?php } ?>
                  </ul>
                </div><!-- navbar-collapse -->
              </div>
            </div>
          </div>
        </div><!-- /.container -->
      </nav><!-- navbar -->
    </div>
<?php 
function col_class($settings, $col_var, $prefix='', $else='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var > 0)) ? $prefix . $settings->$col_var : $else;
  return ($text!='') ? $text . " " : "";
  } 
function visible_class($settings, $col_var, $prefix='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var != '')) ? $prefix . $settings->$col_var : '';
  return ($text!='') ? $text . " " : "";
  }
?>
<?php function display_rows($rows, $ths) { ?>
<?php foreach( $rows as $row ) { ?>

<?php if($row->blogger_main_includable) { ?>
  <div style="border: 5px solid red; padding: 20px;margin: 20px; text-align: center;">
    <?php echo $row->title; ?> will be inserted here...
  </div>
<?php 
  continue;
} ?>

<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>
<?php if( $row->display_rows ) { ?>

  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>

    <div class="<?php echo ($row->remove_row_class) ? '' : 'row'; ?> <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
<?php if(isset($row->columns_data)) { ?>
        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php }  ?>
          <div class="<?php 
          if( isset($more_settings->generate) ) {
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-');
          } 
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/theme/royal/royal_' . $widget->type, array(
                          'widget_id'=>$widget->widget_id,
                          'widget_options'=> json_decode($widget->options)
                       )); 
                ?>
              <?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>
          </div>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>>  
<?php } ?>
        <?php } ?>
<?php } ?>
    </div>
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>
<?php } ?>

<?php if((isset($page)) && ($page)) { ?>

<?php if( $rows ) {  
    display_rows($rows, $this); 
  } ?>

<?php } else { ?>

 <!-- ERROR CONTENT -->
          <div class='mainContent error-content clearfix'>
            <div class='container'>
            <div class='row'>
              <div class='error-content-top'>
                <h1>
                  404
                </h1>
                <h3>
                  Error
                </h3>
              </div>
              <h3>
                Oops!
              </h3>
              <p>
                The Page you&#39;re looking for could not be found!
              </p>
              <p>
                Please
                <a href='<?php echo site_url("templates/view/{$current_client->id}/{$template_override}"); ?>'>
                  contact us
                </a>
                to help us address this issue! Thank you!
              </p>
            </div>
            </div>
          </div>

<?php } ?>


 
    <div class="footer clearfix">
      <div class="container">
        <div class="row clearfix">
          <div class="col-sm-6 col-xs-12 copyRight">
            <p><?php echo get_layout_value('footer', 'copyright_name', $layouts); ?> &copy; <?php echo get_layout_value('footer', 'copyright_year', $layouts); ?></p>
          </div><!-- col-sm-6 col-xs-12 -->
          <div class="col-sm-6 col-xs-12 privacy_policy">            
<?php 
$footer_nav = get_menu_links( get_layout_value('footer', 'footer_nav', $layouts) );
foreach( $footer_nav as $fn ) {
  $fn_url = ($fn->inner_page) ? site_url("templates/view/{$current_client->id}/{$template_override}") . "/" . $fn->inner_page : $fn->url; 
?>
    <a href="<?php echo $fn_url; ?>" ><?php echo $fn->text; ?></a>
<?php } ?> 
          </div><!-- col-sm-6 col-xs-12 -->
        </div><!-- row clearfix -->
      </div><!-- container -->
    </div><!-- footer -->

  </div>

  <!-- JQUERY SCRIPTS -->
  <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/flexslider/jquery.flexslider.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/selectbox/jquery.selectbox-0.1.3.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/magnific-popup/jquery.magnific-popup.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/waypoints/waypoints.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/counterup/jquery.counterup.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/wow/wow.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/royal_college/navbar.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/moment/moment.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/fullcalendar/fullcalendar.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/templates/royal_college/custom.js'); ?>"></script>

<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 115px;
    left: -8px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="blogger_xml" href="<?php echo site_url("templates/generate_blogger/{$current_client->id}/{$template_name}/{$xml_version}"); ?>" class="">XML</a>

</div>

<?php if((isset($page)) && ($page)) { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 190px;
    left: -25px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/" . $page->id ); ?>" class="">Edit Page</a>
<?php } else { ?>
<div style="
    position: fixed;
    z-index: 9999;
    margin: 0 auto;
    border: 1px solid #000;
    padding: 10px;
    background: #CCC;
    top: 325px;
    left: -44px;
    -ms-transform: rotate(90deg); /* IE 9 */
    -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
    transform: rotate(90deg);
">
<a target="edit_page" href="<?php echo site_url("pages/rows/{$current_client->id}/0" ); ?>" class="">Edit Homepage</a>
<?php } ?>
</div>

  </body>
</html>