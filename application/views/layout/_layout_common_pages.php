
        <div class="form-group">
          <label>Homepage</label>
          <select class="form-control" name="layout[common_pages][homepage]">
              <option value="">-- No Page Selected --</option>
              <?php foreach($pages as $page) { ?>
                  <option value="<?php echo $page->id; ?>" <?php echo (get_layout_value('common_pages','homepage', $layouts)==$page->id) ? "SELECTED" : "";?>><?php echo $page->page_title; ?> (<?php echo $page->id; ?>)</option>
                  <?php if($page->children) { ?>
                    <?php foreach($page->children as $cPage) { ?>
                    <option value="<?php echo $cPage->id; ?>" <?php echo (get_layout_value('common_pages','homepage', $layouts)==$page->id) ? "SELECTED" : "";?>>- - - - <?php echo $cPage->page_title; ?> (<?php echo $cPage->id; ?>)</option>
                    <?php } ?>
                  <?php } ?>
              <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Single Page</label>
          <select class="form-control" name="layout[common_pages][single_page]">
              <option value="">-- No Page Selected --</option>
              <?php foreach($pages as $page) { ?>
                  <option value="<?php echo $page->id; ?>" <?php echo (get_layout_value('common_pages','single_page', $layouts)==$page->id) ? "SELECTED" : "";?>><?php echo $page->page_title; ?> (<?php echo $page->id; ?>)</option>
                  <?php if($page->children) { ?>
                    <?php foreach($page->children as $cPage) { ?>
                    <option value="<?php echo $cPage->id; ?>" <?php echo (get_layout_value('common_pages','single_page', $layouts)==$page->id) ? "SELECTED" : "";?>>- - - - <?php echo $cPage->page_title; ?> (<?php echo $cPage->id; ?>)</option>
                    <?php } ?>
                  <?php } ?>
              <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Blog Page</label>
          <select class="form-control" name="layout[common_pages][blog_page]">
              <option value="">-- No Page Selected --</option>
              <?php foreach($pages as $page) { ?>
                  <option value="<?php echo $page->id; ?>" <?php echo (get_layout_value('common_pages','blog_page', $layouts)==$page->id) ? "SELECTED" : "";?>><?php echo $page->page_title; ?> (<?php echo $page->id; ?>)</option>
                  <?php if($page->children) { ?>
                    <?php foreach($page->children as $cPage) { ?>
                    <option value="<?php echo $cPage->id; ?>" <?php echo (get_layout_value('common_pages','blog_page', $layouts)==$page->id) ? "SELECTED" : "";?>>- - - - <?php echo $cPage->page_title; ?> (<?php echo $cPage->id; ?>)</option>
                    <?php } ?>
                  <?php } ?>
              <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Contact Us</label>
          <select class="form-control" name="layout[common_pages][contact_us]">
              <option value="">-- No Page Selected --</option>
              <?php foreach($pages as $page) { ?>
                  <option value="<?php echo $page->id; ?>" <?php echo (get_layout_value('common_pages','contact_us', $layouts)==$page->id) ? "SELECTED" : "";?>><?php echo $page->page_title; ?> (<?php echo $page->id; ?>)</option>
                  <?php if($page->children) { ?>
                    <?php foreach($page->children as $cPage) { ?>
                    <option value="<?php echo $cPage->id; ?>" <?php echo (get_layout_value('common_pages','contact_us', $layouts)==$page->id) ? "SELECTED" : "";?>>- - - - <?php echo $cPage->page_title; ?> (<?php echo $cPage->id; ?>)</option>
                    <?php } ?>
                  <?php } ?>
              <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Error Page</label>
          <select class="form-control" name="layout[common_pages][error_404]">
              <option value="">-- No Page Selected --</option>
              <?php foreach($pages as $page) { ?>
                  <option value="<?php echo $page->id; ?>" <?php echo (get_layout_value('common_pages','error_404', $layouts)==$page->id) ? "SELECTED" : "";?>><?php echo $page->page_title; ?> (<?php echo $page->id; ?>)</option>
                  <?php if($page->children) { ?>
                    <?php foreach($page->children as $cPage) { ?>
                    <option value="<?php echo $cPage->id; ?>" <?php echo (get_layout_value('common_pages','error_404', $layouts)==$page->id) ? "SELECTED" : "";?>>- - - - <?php echo $cPage->page_title; ?> (<?php echo $cPage->id; ?>)</option>
                    <?php } ?>
                  <?php } ?>
              <?php } ?>
          </select>
        </div>
