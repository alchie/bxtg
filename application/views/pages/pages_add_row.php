<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Rows</h3>
            </div>
            <form method="post">
            <div class="panel-body">
<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
<?php endif; ?>


<?php if( $rows ) { ?>
<div class="list-group">
    <?php foreach($rows as $row) { ?>

        <label class="list-group-item">
            <input type="checkbox" value="<?php echo $row->id; ?>" name="row[]">
        <?php echo $row->title; ?>
        </label>

    <?php } ?>
</div>    
<?php } else { ?>
    <p class="text-center">
        No Available Row<br />
        <a href="<?php echo site_url("rows/add_row/{$output}"); ?>" data-title="Create New Row" class="ajax-modal-inner btn btn-xs btn-success">Create New Row</a>
    </p>
<?php }  ?>


<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>