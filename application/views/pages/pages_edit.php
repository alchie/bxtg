<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('pages/pages_header_nav'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
<?php if($page->active==0) { ?>
    <a href="<?php echo site_url("pages/delete/{$page->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
                <h3 class="panel-title">Edit Page</h3>
            </div>
            <form method="post">
            <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
                <div class="form-group">
                    <label>Page ID</label>
                    <input name="page_id" type="text" class="form-control" value="<?php echo $page->page_id; ?>">
                </div>

                <div class="form-group">
                    <label>Page Title</label>
                    <input name="page_title" type="text" class="form-control" value="<?php echo $page->page_title; ?>">
                </div>

                <div class="form-group">
                    <label>Page Slug</label>
                    <input name="page_slug" type="text" class="form-control" value="<?php echo $page->page_slug; ?>">
                </div>

                <div class="form-group">
                    <label>Parent Page</label>
                    <select name="parent_page" type="text" class="form-control">
                        <option value="0">-- Parent Page --</option>
                        <?php foreach($parent_pages as $ppage) { ?>
                            <option value="<?php echo $ppage->id; ?>" <?php echo ($ppage->id==$page->parent_page) ? "SELECTED" : ""; ?>><?php echo $ppage->page_title; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label><input name="active" type="checkbox" value="1" <?php echo ($page->active==1) ? "CHECKED" : ""; ?>> Active</label>
                </div>

<?php if( isset($output) && ($output=='ajax') ) : ?>
    <a href="" class="btn btn-danger btn-xs">Delete this page</a>
<?php endif; ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>

            </div>
            </form>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>

<?php endif; ?>