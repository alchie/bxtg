<?php if (isset($widget_options)&&(isset($widget_options->active))&&($widget_options->active=='1')) { ?>

<?php if (isset($widget_options)&&(isset($widget_options->generate_container))&&($widget_options->generate_container!='')) { ?>
<div id="<?php echo ((isset($widget_options->widget_id))&&($widget_options->widget_id!='')) ? $widget_options->widget_id : 'widget_'. $widget_id; ?>" class="<?php echo ((isset($widget_options->container_class))&&($widget_options->container_class!='')) ? $widget_options->container_class : ''; ?>">
<?php } ?>
<nav class="navbar navbar-default <?php echo ((isset($widget_options->nav_class))&&($widget_options->nav_class!='')) ? $widget_options->nav_class : ''; ?>" id="<?php echo ((isset($widget_options->nav_id))&&($widget_options->nav_id!='')) ? $widget_options->nav_id : ''; ?>">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?php echo ((isset($widget_options->title))&&($widget_options->title!='')) ? $widget_options->title : ''; ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<?php if (isset($widget_options)&&(isset($widget_options->menu_id))&&($widget_options->menu_id!='')) { ?>
      <ul class="nav navbar-nav navbar-right">
       <?php 
$menu_links = get_menu_links($widget_options->menu_id);
foreach($menu_links as $link) { //print_r($link);
	$link_url = $link->url;
	if( $link->inner_page ) {
		//$page_data = get_page($link->inner_page);
		$link_url = site_url('templates/view/' . $this->session->userdata('current_client') . '/0/' . $link->inner_page);
	}
	if( $link->active != 1) { continue; }
?>
        <li <?php echo ($link->children) ? 'class="dropdown"' : ''; ?>>
        	<a href="<?php echo $link_url; ?>" <?php echo ($link->children) ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : ''; ?>><?php echo $link->text; ?></a>
        	<?php if($link->children) { ?>
        		<ul class="dropdown-menu">
        		<?php foreach($link->children as $child) { 
					$child_url = $child->url;
					if( $child->inner_page ) {
						//$page_data = get_page($link->inner_page);
						$link_url = site_url('templates/view/' . $this->session->userdata('current_client') . '/0/' . $child->inner_page);
					}
        			?>
	            	<li>
	            		<a href="<?php echo $child_url; ?>"><?php echo $child->text; ?></a>
	            	</li>
	            <?php } ?>
	          </ul>
        	<?php } ?>
        </li>
<?php } ?>
      </ul>
<?php } ?>
<?php if (isset($widget_options)&&(isset($widget_options->search_box))&&($widget_options->search_box=='1')) { ?>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
<?php } ?>
<?php if (isset($widget_options)&&(isset($widget_options->menu_id_right))&&($widget_options->menu_id_right!='')) { ?>
      <ul class="nav navbar-nav navbar-right">
       <?php 
$menu_links = get_menu_links($widget_options->menu_id_right);
foreach($menu_links as $link) { //print_r($link);
	$link_url = $link->url;
	if( $link->inner_page ) {
		//$page_data = get_page($link->inner_page);
		$link_url = site_url('templates/view/' . $this->session->userdata('current_client') . '/0/' . $link->inner_page);
	}
	if( $link->active != 1) { continue; }
?>
        <li <?php echo ($link->children) ? 'class="dropdown"' : ''; ?>>
        	<a href="<?php echo $link_url; ?>" <?php echo ($link->children) ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : ''; ?>><?php echo $link->text; ?></a>
        	<?php if($link->children) { ?>
        		<ul class="dropdown-menu">
        		<?php foreach($link->children as $child) { 
					$child_url = $child->url;
					if( $child->inner_page ) {
						//$page_data = get_page($link->inner_page);
						$link_url = site_url('templates/view/' . $this->session->userdata('current_client') . '/0/' . $child->inner_page);
					}
        			?>
	            	<li>
	            		<a href="<?php echo $child_url; ?>"><?php echo $child->text; ?></a>
	            	</li>
	            <?php } ?>
	          </ul>
        	<?php } ?>
        </li>
<?php } ?>
      </ul>
<?php } ?>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php if (isset($widget_options)&&(isset($widget_options->generate_container))&&($widget_options->generate_container!='')) { ?>
</div>
<?php } ?>

<?php } ?>