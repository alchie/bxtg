<div id="<?php echo ((isset($widget_options->widget_id))&&($widget_options->widget_id!='')) ? $widget_options->widget_id : 'widget_'. $widget_id; ?>">

<?php if((isset($widget_options->add_container)) && ($widget_options->add_container==1)) { ?>
<div class="container">
<?php } ?>

<?php if((isset($widget_options->add_cols)) && ($widget_options->add_cols==1)) { ?>
<div class="row">
<?php } ?>

<?php 
$item_n = 0; 
if( $widget_options ) foreach($widget_options->items as $item) { ?>

<?php 
	$page = false;
	$link_url = $item->image_link; 
	if( (isset($item->image_page)) && ($item->image_page != '') && ($item->image_page > 0) ) {
		$page = get_page($item->image_page);
		$link_url = site_url("templates/view/{$current_client->id}/royal/" . $item->image_page);
		if( $template_output == 'blogger_xml') {
			$link_url = "http://" . $current_client->domain . "/" . $page->page_slug . ".html";
		}
	}
?>


<?php if((isset($widget_options->add_cols)) && ($widget_options->add_cols==1)) { ?>
<div class="col-lg-<?php echo $widget_options->col_width_lg; ?> col-md-<?php echo $widget_options->col_width_md; ?> col-sm-<?php echo $widget_options->col_width_sm; ?> col-xs-<?php echo $widget_options->col_width_xs; ?>">
<?php } ?>

  <div class="aboutImage <?php echo (isset($item->container_class)) ? $item->container_class : ""; ?>">
<?php if( ($item->image_link != '') || ($page) ) { ?>
    <a href="<?php echo $link_url; ?>" > 
<?php } ?>
    <img src="<?php echo $item->image_url; ?>"  />
<?php if((isset($item->show_caption)) && ($item->show_caption==1)) { ?>
	<?php if((isset($item->show_desc)) && ($item->show_desc==1)) { ?>
      <div class="overlay">
        <p><?php echo $item->image_desc; ?></p>
      </div>
    <?php } ?>
	<?php if((isset($item->show_title)) && ($item->show_title==1)) { ?>
      <span class="captionLink"><?php echo $item->image_title; ?><span></span></span>
	<?php } ?>

<?php } ?>
<?php if( ($item->image_link != '') || ($page) ) { ?>
    </a>
<?php } ?>
  </div>

<?php if((isset($widget_options->add_cols)) && ($widget_options->add_cols==1)) { ?>
</div>
<?php } ?>

<?php 
$item_n++;
} ?>

<?php if((isset($widget_options->add_cols)) && ($widget_options->add_cols==1)) { ?>
</div>
<?php } ?>
<?php if((isset($widget_options->add_container)) && ($widget_options->add_container==1)) { ?>
</div>
<?php } ?>

</div>