<?php 
$item_n = 0; 
if( $widget_options ) foreach($widget_options->items as $item) { ?>

<?php 
	$page = false;
	$link_url = $item->image_link; 
	if( (isset($item->image_page)) && ($item->image_page != '') && ($item->image_page > 0) ) {
		$page = get_page($item->image_page);
		$link_url = site_url("templates/view/{$current_client->id}/royal/" . $item->image_page);
		if( $template_output == 'blogger_xml') {
			$link_url = "http://" . $current_client->domain . "/search/label/" . $page->page_slug;
		}
	}
?>

  <div class="aboutImage <?php echo (isset($item->container_class)) ? $item->container_class : ""; ?>">
<?php if( ($item->image_link != '') || ($page) ) { ?>
    <a href="<?php echo $link_url; ?>" > 
<?php } ?>
    <img src="<?php echo $item->image_url; ?>"  />
<?php if((isset($item->show_caption)) && ($item->show_caption==1)) { ?>
	<?php if((isset($item->show_desc)) && ($item->show_desc==1)) { ?>
      <div class="overlay">
        <p><?php echo $item->image_desc; ?></p>
      </div>
    <?php } ?>
	<?php if((isset($item->show_title)) && ($item->show_title==1)) { ?>
      <span class="captionLink"><?php echo $item->image_title; ?><span></span></span>
	<?php } ?>

<?php } ?>
<?php if( ($item->image_link != '') || ($page) ) { ?>
    </a>
<?php } ?>
  </div>
<?php 
$item_n++;
} ?>