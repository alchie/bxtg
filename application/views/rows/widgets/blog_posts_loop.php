<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('rows/rows_header_nav'); ?>

<div class="container">
<div class="row">
  <form method="post">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

    <button type="submit" class="btn btn-success btn-xs pull-right" style="margin-right:5px">Save</button>

                <h3 class="panel-title">HTML / Text
<?php if(( isset($widget) && ($widget) ) && ((isset($columns)) && (count($columns) > 1)) ) { ?>
<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Move to Column <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php foreach($columns as $col) { 
    if( $col->column==$widget->column) {
      continue;
    }
    ?>
    <li><a href="<?php echo site_url("rows/move_widget/{$widget->widget_id}/{$col->column}"); ?>"><?php echo $col->column; ?></a></li>
  <?php } ?>
  </ul>
</div>
<?php } ?>

                </h3>
            </div>
          
            <div class="panel-body">

<?php endif; ?>

<?php 

if( isset($widget) && ($widget) ) {
  $widget_options = json_decode( $widget->options );  
}
?>

<div class="form-group">
<label class="pull-right"><input name="show_wrapper" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_wrapper))&&($widget_options->show_wrapper==1)) ? 'CHECKED' : ''; ?>> Show Wrapper</label>
    <label>Wrapper Class</label>
    <input type="text" class="form-control" name="wrapper_class" placeholder="Wrapper Class" value="<?php echo (isset($widget_options)&&isset($widget_options->wrapper_class)) ? $widget_options->wrapper_class : ''; ?>">
</div>

<div class="form-group">
<label class="pull-right"><input name="show_container" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_container))&&($widget_options->show_container==1)) ? 'CHECKED' : ''; ?>> Show Container</label>
    <label>Container Class</label>
    <input type="text" class="form-control" name="container_class" placeholder="Container Class" value="<?php echo (isset($widget_options)&&isset($widget_options->container_class)) ? $widget_options->container_class : ''; ?>">
</div>

<div class="form-group">
<label class="pull-right"><input name="show_row" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_row))&&($widget_options->show_row==1)) ? 'CHECKED' : ''; ?>> Show Row</label>
    <label>Row Class</label>
    <input type="text" class="form-control" name="row_class" placeholder="Row Class" value="<?php echo (isset($widget_options)&&isset($widget_options->row_class)) ? $widget_options->row_class : ''; ?>">
</div>

<div class="form-group">
<label class="pull-right"><input name="show_column" type="checkbox" value="1" <?php echo (isset($widget_options)&&(isset($widget_options->show_column))&&($widget_options->show_column==1)) ? 'CHECKED' : ''; ?>> Show Column</label>
    <label>Column Class</label>
    <input type="text" class="form-control" name="column_class" placeholder="Column Class" value="<?php echo (isset($widget_options)&&isset($widget_options->column_class)) ? $widget_options->column_class : ''; ?>">
</div>

<div class="form-group">
    <label>Loop Template</label>
    <input type="text" class="form-control" name="loop_template" placeholder="Loop Template" value="<?php echo (isset($widget_options)&&isset($widget_options->loop_template)) ? $widget_options->loop_template : ''; ?>">
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

            </div>
            
        </div>
    </div>
    </form>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>