<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";

function col_class($settings, $col_var, $prefix='', $else='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var > 0)) ? $prefix . $settings->$col_var : $else;
  return ($text!='') ? $text . " " : "";
  } 
function visible_class($settings, $col_var, $prefix='') {
  $text = (isset($settings->$col_var) && ($settings->$col_var != '')) ? $prefix . $settings->$col_var : '';
  return ($text!='') ? $text . " " : "";
  }

function blogger_page_url($page, $current_client, $mobile=false, $secure=false) {
    $parent_slug = $page->page_slug;
    $protocol = 'http://';
    if($secure) {
      $protocol = 'https://';
    }
    $url = "{$protocol}{$current_client->domain}/search/label/{$parent_slug}";
    return $url;
}
function display_rows($row, $ths) { 

?>
<?php if($row->wrapper_class!='') { ?>
  <div class="<?php echo $row->wrapper_class; ?>">  
<?php } ?>
<?php if($row->container) { ?>
  <div class="container <?php echo $row->container_class; ?>">  
<?php } ?>
<?php if( $row->display_rows ) { ?>

  <?php if( $row->row_display_title ) { ?>
  <div class="col-md-12">
    <h1 class="text-center"><?php echo $row->title; ?></h1>
    <hr/>
  </div>
  <?php } ?>

    <div class="<?php echo ($row->remove_row_class) ? '' : 'row'; ?> <?php echo $row->row_class; ?>" id="body-row-<?php echo $row->id; ?>">
<?php if(isset($row->columns_data)) { ?>
        <?php foreach($row->columns_data as $col) { 
              $settings = $col['settings']; 
              $more_settings = (isset($settings->more_settings)) ? json_decode( $settings->more_settings ) : false;
          ?>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  <div class="<?php echo $settings->wrapper; ?>">  
<?php } ?>
         <div class="<?php 
        if( isset($more_settings->generate) ) {
          echo col_class($more_settings, 'col_lg', 'col-lg-', 'hidden-lg'); 
          echo col_class($more_settings, 'col_md', 'col-md-', 'hidden-md');
          echo col_class($more_settings, 'col_sm', 'col-sm-', 'hidden-sm');
          echo col_class($more_settings, 'col_xs', 'col-xs-', 'hidden-xs');
          echo col_class($more_settings, 'col_lg_offset', 'col-lg-offset-'); 
          echo col_class($more_settings, 'col_md_offset', 'col-md-offset-'); 
          echo col_class($more_settings, 'col_sm_offset', 'col-sm-offset-'); 
          echo col_class($more_settings, 'col_xs_offset', 'col-xs-offset-'); 
          echo visible_class($more_settings, 'visible_lg', 'visible-lg-'); 
          echo visible_class($more_settings, 'visible_md', 'visible-md-'); 
          echo visible_class($more_settings, 'visible_sm', 'visible-sm-'); 
          echo visible_class($more_settings, 'visible_xs', 'visible-xs-'); 
        }
          echo (isset($settings->class)) ? $settings->class : '';
          ?>">
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  <div class="<?php echo $settings->container; ?>">  
<?php } ?>
              <?php foreach($col['widgets'] as $widget) { ?>
                <?php 
                         $ths->load->view('templates/theme/royal/v5/royal_' . $widget->type, array(
                          'widget_id'=>$widget->widget_id,
                          'widget_options'=> json_decode($widget->options))); 
                ?>
              <?php } ?>
<?php if(isset($settings->container) && ($settings->container != '') ) { ?>
  </div>  
<?php } ?>
          </div>
<?php if(isset($settings->wrapper) && ($settings->wrapper != '') ) { ?>
  </div>>  
<?php } ?>
        <?php } ?>
<?php } ?>
    </div>
<?php } ?>

<?php if( $row->children ) { ?>
    <?php  display_rows($row->children, $ths);  ?>
<?php } ?>

<?php if($row->container) { ?>
  </div>
<?php } ?>
<?php if($row->wrapper_class!='') { ?>
  </div>
<?php } ?>
<?php } ?>

<?php
function display_blogger_posts_rows($row, $ths) { ?>
<b:if cond='data:blog.pageType == &quot;index&quot;'>

          <div class="col-md-4 d-flex">
            <div class="blog-entry align-self-stretch">
              <b:if cond='data:post.firstImageUrl'>
               <a expr:href="data:post.url" class="block-20 gray-hover-colored" expr:id='&quot;index-&quot; + data:post.id' expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'></a>
                <meta expr:content='data:post.firstImageUrl' itemprop='image_url'/>
              </b:if>
              <meta expr:content='data:blog.blogId' itemprop='blogId'/>
              <meta expr:content='data:post.id' itemprop='postId'/>
              <a expr:name='data:post.id'/>
<?php if($row->blogger_postlist_textbox) { ?>
              <div class="text p-4 d-block">
                <h3 class="heading mt-3"><a expr:href="data:post.url"><data:post.title/></a></h3>
                <p><data:post.snippet/></p>
              </div>
<?php } ?>
            </div>
          </div>
</b:if>
<?php } ?><!DOCTYPE html>
<html b:version='2' class='v2' expr:dir='data:blog.languageDirection' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>
  <head>
    <meta content='width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0' name='viewport'/>
    <b:include data='blog' name='all-head-content'/>
<title><b:if cond='data:blog.url == data:blog.homepageUrl'><data:blog.pageTitle/>
<?php foreach( $pages as $page_id=>$page_data ) { 
  $page = $page_data['page']; 

?>
<?php if($page_id!='homepage') { 
$page_url = blogger_page_url($page, $current_client);
$page_url_s = blogger_page_url($page, $current_client, false, true);
$page_url_m = blogger_page_url($page, $current_client, true);
$page_url_ms = blogger_page_url($page, $current_client, true, true);
  ?>
<b:elseif cond='data:blog.url == &quot;<?php echo $page_url; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_s; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_m; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_ms; ?>&quot;'/><?php echo htmlentities($page->page_title); ?>
<?php } ?>
<?php } ?>
<b:else/>
<b:if cond='data:blog.pageType == &quot;item&quot;'><data:blog.pageTitle/><b:else/><?php echo ( isset($error_404_page) && ($error_404_page) && (isset($error_404_page['page'])) && ($error_404_page['page']) ) ? $error_404_page['page']->page_title : 'Error 404 : Page not found!'; ?></b:if></b:if></title>
    

  <link crossorigin='anonymous' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' rel='stylesheet'/>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'/>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/flexslider.min.css' rel='stylesheet'/>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.css' rel='stylesheet'/>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,600italic,400italic,700' rel='stylesheet' type='text/css'/>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'/>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' rel='stylesheet'/>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' rel='stylesheet'/>

    <b:skin><![CDATA[
<?php $this->load->view('templates/theme/royal/_xml_royal_5_skin'); ?>
      ]]></b:skin>
    <b:template-skin>
      <![CDATA[
<?php $this->load->view('templates/theme/royal/_xml_royal_5_template'); ?>
<?php $additional_css = trim(get_layout_value('css', 'css', $layouts)); 
if($additional_css!='') { echo str_replace("\n", "", $additional_css); } ?>
<?php $additional_css_xs = trim(get_layout_value('css', 'css_xs', $layouts)); 
if($additional_css_xs!='') { 
   echo '@media (max-width: 767px) {';
  echo str_replace("\n", "", $additional_css_xs); 
  echo '}';
} ?>
<?php $additional_css_sm = trim(get_layout_value('css', 'css_sm', $layouts)); 
if($additional_css_sm!='') { 
   echo '@media (min-width: 768px) {';
  echo str_replace("\n", "", $additional_css_sm); 
  echo '}';
} ?>
<?php $additional_css_md = trim(get_layout_value('css', 'css_md', $layouts)); 
if($additional_css_md!='') { 
   echo '@media (min-width: 992px) {';
  echo str_replace("\n", "", $additional_css_md); 
  echo '}';
} ?>
<?php $additional_css_lg = trim(get_layout_value('css', 'css_lg', $layouts)); 
if($additional_css_lg!='') { 
  echo '@media (min-width: 1200px) {';
  echo str_replace("\n", "", $additional_css_lg); 
  echo '}';
} ?>
    ]]></b:template-skin>
<b:if cond='data:blog.url == data:blog.homepageUrl'>
<style>
.hide_on_homepage { display:none; }
</style>
</b:if>

   <b:defaultmarkups>
      <b:defaultmarkup type='Common'>
<?php if($rows) foreach($rows as $row) { ?>
<?php if($row->blogger_main_includable) { ?>
  <b:includable id='posts-list-<?php echo $row->id; ?>' var='post'>
    <?php echo display_blogger_posts_rows($row, $this); ?>
  </b:includable>
<?php } else { ?>
  <b:includable id='common-row-<?php echo $row->id; ?>' var='posts'>
    <?php echo display_rows($row, $this); ?>
  </b:includable>
<?php } ?>
<?php } ?>

<b:includable id='post-main' var='post'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>

          <div class="col-md-4 d-flex">
            <div class="blog-entry align-self-stretch">
              <b:if cond='data:post.firstImageUrl'>
               <a expr:href="data:post.url" class="block-20 gray-hover-colored" expr:id='&quot;index-&quot; + data:post.id' expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'></a>
                <meta expr:content='data:post.firstImageUrl' itemprop='image_url'/>
              </b:if>
              <meta expr:content='data:blog.blogId' itemprop='blogId'/>
              <meta expr:content='data:post.id' itemprop='postId'/>
              <a expr:name='data:post.id'/>

              <div class="text p-4 d-block">
                <div class="meta mb-3">
                  <div><a class='timestamp-link' expr:href='data:post.url' rel='bookmark' title='permanent link'><abbr class='published' expr:title='data:post.timestamp' itemprop='datePublished dateModified'> <data:post.timestamp/></abbr></a></div>
                  <b:if cond='data:top.showAuthor'>
                  <div><a href="#"><data:post.author/></a></div>
                  </b:if>
                  <div>
<b:if cond='data:post.allowComments'>
<a class='comment-link' expr:href='data:post.addCommentUrl' expr:onclick='data:post.addCommentOnclick'><b:if cond='data:post.numComments == 1'>1 <data:top.commentLabel/><b:else/><data:post.numComments/> <data:top.commentLabelPlural/></b:if></a>
</b:if>
                  </div>
                </div>
                <h3 class="heading mt-3"><a expr:href="data:post.url"><data:post.title/></a></h3>
                <p><data:post.snippet/></p>
              </div>
            </div>
          </div>
</b:if>
</b:includable>

<b:includable id='post-gallery' var='post'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>
          <a expr:href="data:post.firstImageUrl" class="gallery image-popup d-flex justify-content-center align-items-center img" expr:style='&quot;background-image: url(&quot; +data:post.firstImageUrl+ &quot;);&quot;'>
          </a>
</b:if>
</b:includable>

<b:includable id='post-aboutImage' var='post'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>
<div class="aboutImage">
      <a expr:href="data:post.url">
        <img expr:src="data:post.firstImageUrl" alt="" class="img-responsive"/>
        <div class="overlay">
          <p><data:post.snippet/></p>
        </div>
        <span class="captionLink"><data:post.title/><span></span></span>
      </a>
</div>
</b:if>
</b:includable>

<b:includable id='post-item-body' var='post'>
<div class="content_top clearfix">
<div class="container">
<div class="row">
<div class="col-md-12">
  <div class='post hentry uncustomized-post-template' itemprop='blogPost' itemscope='itemscope' itemtype='http://schema.org/BlogPosting'>
    <b:if cond='data:post.firstImageUrl'>
      <meta expr:content='data:post.firstImageUrl' itemprop='image_url'/>
    </b:if>
    <meta expr:content='data:blog.blogId' itemprop='blogId'/>
    <meta expr:content='data:post.id' itemprop='postId'/>

    <a expr:name='data:post.id'/>
    <b:if cond='data:post.title'>
      <h1 class='post-title entry-title' itemprop='name'>
      <b:if cond='data:post.link or (data:post.url and data:blog.url != data:post.url)'>
        <a expr:href='data:post.link ? data:post.link : data:post.url'><data:post.title/></a>
      <b:else/>
        <data:post.title/>
      </b:if>
      </h1>
    </b:if>

    <div class='post-body entry-content' expr:id='&quot;post-body-&quot; + data:post.id' expr:itemprop='(data:blog.metaDescription ? &quot;&quot; : &quot;description &quot;) + &quot;articleBody&quot;'>
      <data:post.body/>
      <div style='clear: both;'/>
    </div>

  </div>
  </div>
  </div>
  </div>
  </div>

</b:includable>

<b:includable id='404-page-error'>
<div class="custom_content custom error-content">
<div class="container ">
<div class="row " id="body-row-26">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <div class='error-content-top'>
   <h1>404</h1>
   <h3>Error</h3>
 </div>
 <h3>Oops!</h3><?php $contact_us = get_page(get_layout_value('common_pages','contact_us', $layouts)); ?>
 <p>The Page you&#39;re looking for could not be found!</p><p>Please <a href='http://<?php echo $current_client->domain; ?>/search/label/<?php echo ($contact_us) ? $contact_us->page_slug : 'contact-us'; ?>'>contact us</a> to help us address this issue! Thank you!</p>
</div>
</div>
</div>
</div>
</b:includable>
      </b:defaultmarkup>
    </b:defaultmarkups>

    <b:include data='blog' name='google-analytics'/>
    <?php echo get_layout_value('head', 'head', $layouts); ?>

<script>
// <![CDATA[
<?php $this->load->view('templates/theme/royal/_xml_royal_5_scripts'); ?>

// ]]></script>

</head>

<body expr:class='&quot;<?php echo (get_layout_value('page', 'width', $layouts)=='boxed') ? 'bodyColor container ' : ''; ?>loading&quot; + data:blog.mobileClass'>
 
  <div class="main_wrapper">

<?php if(get_layout_value('top_nav', 'status', $layouts)==1) { ?>
    <div class="topbar clearfix">
      <div class="container">
        <ul class="topbar-left">
          <li class="phoneNo"><i class="fa fa-phone"></i><?php echo get_layout_value('top_nav', 'phone_number', $layouts); ?></li>
          <li class="email-id hidden-xs hidden-sm"><i class="fa fa-envelope"></i>
            <a href="mailto:<?php echo get_layout_value('top_nav', 'email_address', $layouts); ?>"><?php echo get_layout_value('top_nav', 'email_address', $layouts); ?></a>
          </li>
        </ul>
        <ul class="topbar-right">
<?php 
$social_media = get_menu_links( get_layout_value('top_nav', 'social_media', $layouts) );
foreach( $social_media as $sm ) {
?>
          <li class="hidden-xs"><a href="<?php echo $sm->url; ?>"><i class="<?php echo $sm->icon; ?>"></i></a></li>
<?php } ?>          
<?php if( get_layout_value('top_nav', 'search_url', $layouts) ) { ?>
          <li class="dropdown top-search list-inline">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-search"></i>
            </a>
            <ul class="dropdown-menu">
              <form action="<?php echo get_layout_value('top_nav', 'search_url', $layouts); ?>" method="post">
                <input type="text" placeholder="Course Name" id="exampleInputEmail1" class="form-control">
                <button class="btn btn-default commonBtn" type="submit">Search</button>
              </form>
            </ul>
          </li>
<?php } ?>
        </ul>
      </div>
    </div>
<?php } ?>


    <div class="header clearfix">
      <nav class="navbar navbar-default <?php echo ( get_layout_value('header', 'nav_effects', $layouts) == 'fixed' ) ? 'navbar-main' : ''; ?>">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="header_inner">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
<?php if( get_layout_value('header', 'site_url', $layouts) ) { ?>
                  <a class="navbar-brand logo clearfix" href="<?php echo get_layout_value('header', 'site_url', $layouts); ?>" style="<?php echo (get_layout_value('header', 'site_logo_image', $layouts)&&get_layout_value('header', 'site_logo_width', $layouts)) ? "width:".get_layout_value('header', 'site_logo_width', $layouts) : ''; ?>">
 <?php if( get_layout_value('header', 'site_logo_image', $layouts) ) { 
$image_id = get_layout_value('header', 'site_logo_image', $layouts);
$image_data = get_image($image_id);
                      ?>
                      <img src="<?php echo $image_data->upload_url ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>" />
                    <?php } else { ?>
                      <?php echo get_layout_value('header', 'site_title', $layouts); ?>
                    <?php } ?>
                  </a>
<?php } else { ?>
     <span class="navbar-brand logo clearfix" style="<?php echo (get_layout_value('header', 'site_logo_image', $layouts)&&get_layout_value('header', 'site_logo_width', $layouts)) ? "width:".get_layout_value('header', 'site_logo_width', $layouts) : ''; ?>">
            <?php if( get_layout_value('header', 'site_logo_image', $layouts) ) { 
$image_id = get_layout_value('header', 'site_logo_image', $layouts);
$image_data = get_image($image_id);
                      ?>
                      <img src="<?php echo $image_data->upload_url ?>" title="<?php echo get_layout_value('header', 'site_title', $layouts); ?>" />
              <?php } else { ?>
                <?php echo get_layout_value('header', 'site_title', $layouts); ?>
              <?php } ?>
      </span>
<?php }  ?>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="main-nav">
                  <ul class="nav navbar-nav navbar-right">
<?php 
$menu = get_menu( get_layout_value('header', 'header_nav', $layouts) );
$is_home_link = get_menus_meta($menu->id, "home_link");
if( $is_home_link ) {
?>
                    <li><a href="<?php echo get_layout_value('header', 'site_url', $layouts); ?>">Home</a></li>
<?php } ?>
<?php 
$header_nav = get_menu_links( get_layout_value('header', 'header_nav', $layouts) );
foreach( $header_nav as $hnav ) {

    if( $hnav->active != 1 ) {
    continue;
  }

?>
          <?php if( $hnav->children ) { ?>
              <li class="dropdown list-inline">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo htmlentities($hnav->text); ?></a>
                   <ul class="dropdown-menu">
                      <?php foreach($hnav->children as $hnav_child) { 
  if( $hnav_child->active != 1 ) {
    continue;
  }
                       ?>
                          <?php if( $hnav_child->children ) { ?>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo htmlentities($hnav_child->text); ?></a>
                              <ul class="dropdown-menu">
                                <?php foreach($hnav_child->children as $sub_child) { ?>
                                    <li><a href="<?php echo ($sub_child->inner_page) ? "//{$current_client->domain}/search/label/{$sub_child->page_slug}{$sub_child->url}" : $sub_child->url; ?>" <?php echo ($sub_child->target!='') ? 'target="'.$sub_child->target.'"' : ''; ?>><?php echo htmlentities($sub_child->text); ?></a></li>
                                <?php } ?>
                              </ul>
                            </li>
                          <?php } else { ?>
                            <li><a href="<?php echo ($hnav_child->inner_page) ? "//{$current_client->domain}/search/label/{$hnav_child->page_slug}{$hnav_child->url}" : $hnav_child->url; ?>" <?php echo ($hnav_child->target!='') ? 'target="'.$hnav_child->target.'"' : ''; ?>><?php echo htmlentities($hnav_child->text); ?></a></li>
                          <?php } ?>
                      <?php } ?>
                   </ul>
              </li>
          <?php } else { ?>
              <li><a href="<?php echo ($hnav->inner_page) ? "//{$current_client->domain}/search/label/{$hnav->page_slug}{$hnav->url}" : $hnav->url; ?>" <?php echo ($hnav->target!='') ? 'target="'.$hnav->target.'"' : ''; ?>><?php echo htmlentities($hnav->text); ?></a></li>
          <?php } ?>
<?php } ?>  
<?php if( get_layout_value('header', 'special_button_url', $layouts) ) { ?>
                  <li class="apply_now"><a href="<?php echo get_layout_value('header', 'special_button_url', $layouts); ?>" ><?php echo htmlentities(get_layout_value('header', 'special_button_text', $layouts)); ?></a></li>
<?php } ?>
                  </ul>
                </div><!-- navbar-collapse -->
              </div>
            </div>
          </div>
        </div><!-- /.container -->
      </nav><!-- navbar -->
    </div>



          <b:section class='main' id='main' name='Main' showaddelement='no'>
            <b:widget id='Blog1' locked='true' title='Blog Posts' type='Blog' class='container'>
              <b:widget-settings>
                <b:widget-setting name='showDateHeader'>false</b:widget-setting>
                <b:widget-setting name='style.textcolor'>#666666</b:widget-setting>
                <b:widget-setting name='showShareButtons'>false</b:widget-setting>
                <b:widget-setting name='showCommentLink'>false</b:widget-setting>
                <b:widget-setting name='style.urlcolor'>#4d469c</b:widget-setting>
                <b:widget-setting name='showAuthor'>false</b:widget-setting>
                <b:widget-setting name='style.linkcolor'>#2198a6</b:widget-setting>
                <b:widget-setting name='style.unittype'>TextAndImage</b:widget-setting>
                <b:widget-setting name='style.bgcolor'>#ffffff</b:widget-setting>
                <b:widget-setting name='showAuthorProfile'>false</b:widget-setting>
                <b:widget-setting name='style.layout'>1x1</b:widget-setting>
                <b:widget-setting name='showLabels'>false</b:widget-setting>
                <b:widget-setting name='showLocation'>false</b:widget-setting>
                <b:widget-setting name='showTimestamp'>false</b:widget-setting>
                <b:widget-setting name='postsPerAd'>0</b:widget-setting>
                <b:widget-setting name='showBacklinks'>false</b:widget-setting>
                <b:widget-setting name='style.bordercolor'>#ffffff</b:widget-setting>
                <b:widget-setting name='showInlineAds'>false</b:widget-setting>
                <b:widget-setting name='showReactions'>false</b:widget-setting>
              </b:widget-settings>

<b:includable id='main' var='top'>
<b:if cond='data:blog.pageType == &quot;index&quot;'>
 <?php foreach( $pages as $page_id=>$page_data ) { 
        $page = $page_data['page']; 
        $options = $page_data['options'];
        $page_rows = $page_data['rows'];

$page_url = blogger_page_url($page, $current_client);
$page_url_s = blogger_page_url($page, $current_client, false, true);
$page_url_m = blogger_page_url($page, $current_client, true);
$page_url_ms = blogger_page_url($page, $current_client, true, true);

        ?>
      <?php if($page_id=='homepage') { ?>
      <b:if cond='data:blog.url == data:blog.homepageUrl'>
      <?php if( $page_rows ) foreach($page_rows as $page_row) { ?>
        <?php if( $page_row->blogger_main_includable ) { ?>
            <section class="<?php echo $page_row->wrapper_class; ?>">
              <div class="container <?php echo $page_row->container_class; ?>">
                <div class="row <?php echo $page_row->row_class; ?>">
                  <?php if( $page_row->row_display_title ) { ?>
                    <div class="col-md-12 text-center">
                      <h2 class="mb-4"><?php echo $page_row->title; ?></h2>
                    </div>
                  <?php } ?>
                  <b:loop values='data:posts' var='post'>
                  <div class="<?php echo ( $page_row->blogger_post_row_class != '' ) ? $page_row->blogger_post_row_class : ''; ?>">
                      <b:include data='post' name='<?php echo ( $page_row->blogger_post_name != '' ) ? $page_row->blogger_post_name : 'posts-list-' . $page_row->row_id; ?>'/>
                  </div>
                  </b:loop>
                </div>
              </div>
            </section>
          <?php } else { ?>
            <b:include  data='posts' name='common-row-<?php echo $page_row->row_id; ?>'/>
          <?php } ?>
      <?php } ?>
      <?php } else { ?>

     <b:elseif cond='data:blog.url == &quot;<?php echo $page_url; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_s; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_m; ?>&quot; || data:blog.url == &quot;<?php echo $page_url_ms; ?>&quot;'/>

      <?php if( $page_rows ) foreach($page_rows as $page_row) { ?>
        
        <?php if( $page_row->blogger_main_includable ) { ?>
            <section class="<?php echo $page_row->wrapper_class; ?>">
              <div class="container <?php echo $page_row->container_class; ?>">
                <div class="row <?php echo $page_row->row_class; ?>">
                  <?php if( $page_row->row_display_title ) { ?>
                    <div class="col-md-12 text-center">
                      <h2 class="mb-4"><?php echo $page_row->title; ?></h2>
                    </div>
                  <?php } ?>
                  <b:loop values='data:posts' var='post'>
                  <div class="<?php echo ( $page_row->blogger_post_row_class != '' ) ? $page_row->blogger_post_row_class : ''; ?>">
                      <b:include data='post' name='<?php echo ( $page_row->blogger_post_name != '' ) ? $page_row->blogger_post_name : 'posts-list-' . $page_row->row_id; ?>'/>
                    </div>
                  </b:loop>
                </div>
              </div>
            </section>
          <?php } else { ?>
            <b:include data='posts' name='common-row-<?php echo $page_row->row_id; ?>'/>
          <?php } ?>


      <?php } ?>

      <?php } ?>
      <?php } ?>      
      </b:if>
<b:elseif cond='data:blog.pageType == &quot;error_page&quot;'/>
  <?php if($error_404_page) { ?>
        <?php if( $error_404_page['rows'] ) foreach($error_404_page['rows'] as $page_row) { ?>
              <b:include name='common-row-<?php echo $page_row->row_id; ?>'/>
        <?php } ?>
      <?php } else { ?>
        <b:include name='404-page-error'/>
      <?php } ?>
<b:elseif cond='data:blog.pageType == &quot;item&quot; || data:blog.pageType == &quot;static_page&quot;'/>

  <?php if($single_page) { ?>
        <?php if( $single_page['rows'] ) foreach($single_page['rows'] as $page_row) { 
if($page_row->blogger_main_includable) {
          ?>
  <b:loop values='data:posts' var='post'>
    <b:include data='post' name='post-item-body'/>
  </b:loop>
<?php } else { ?>
    <b:include name='common-row-<?php echo $page_row->row_id; ?>'/>
<?php } ?>
        <?php } ?>
      <?php } else { ?>
  <b:loop values='data:posts' var='post'>
    <b:include data='post' name='post-item-body'/>
  </b:loop>
      <?php } ?>


</b:if>

</b:includable>


              <b:includable id='backlinkDeleteIcon' var='backlink'></b:includable>
              <b:includable id='backlinks' var='post'></b:includable>
              <b:includable id='comment-form' var='post'></b:includable>
              <b:includable id='commentDeleteIcon' var='comment'></b:includable>
              <b:includable id='comment_count_picker' var='post'></b:includable>
              <b:includable id='comment_picker' var='post'></b:includable>
              <b:includable id='comments' var='post'></b:includable>
              <b:includable id='feedLinks'></b:includable>
              <b:includable id='feedLinksBody' var='links'></b:includable>
              <b:includable id='iframe_comments' var='post'></b:includable>
              <b:includable id='mobile-index-post' var='post'></b:includable>
              <b:includable id='mobile-main' var='top'></b:includable>
              <b:includable id='mobile-nextprev'></b:includable>
              <b:includable id='mobile-post' var='post'></b:includable>
              <b:includable id='nextprev'></b:includable>
              <b:includable id='post' var='post'></b:includable>
              <b:includable id='postQuickEdit' var='post'></b:includable>
              <b:includable id='shareButtons' var='post'></b:includable>
              <b:includable id='status-message'></b:includable>
              <b:includable id='threaded-comment-form' var='post'></b:includable>
              <b:includable id='threaded_comment_js' var='post'></b:includable>
              <b:includable id='threaded_comments' var='post'></b:includable>

            </b:widget>
          </b:section>



          <macro:include id='main-column-left-sections' name='sections'>
            <macro:param default='0' name='num' value='1'/>
            <macro:param default='sidebar' name='idPrefix'/>
            <macro:param default='sidebar' name='class'/>
            <macro:param default='false' name='includeBottom'/>
          </macro:include>

<macro:includable id='sections' var='col'>
    <b:section mexpr:class='data:col.class' mexpr:id='data:col.idPrefix + &quot;-1&quot;' preferred='yes' showaddelement='yes'/>
</macro:includable>



 
    <div class="footer clearfix">
      <div class="container">
        <div class="row clearfix">
          <div class="col-sm-6 col-xs-12 copyRight">
            <p><?php echo get_layout_value('footer', 'copyright_name', $layouts); ?> &amp;copy; <?php echo get_layout_value('footer', 'copyright_year', $layouts); ?></p>
          </div><!-- col-sm-6 col-xs-12 -->
          <div class="col-sm-6 col-xs-12 privacy_policy">            
<?php 
$footer_nav = get_menu_links( get_layout_value('footer', 'footer_nav', $layouts) );
foreach( $footer_nav as $fn ) {
  $fn_url = ($fn->inner_page) ? "http://{$current_client->domain}/search/label/{$fn->page_slug}.html" : $fn->url; 
?>
    <a href="<?php echo $fn_url; ?>" <?php echo ($fn->target!='') ? 'target="'.$fn->target.'"' : ''; ?>><?php echo htmlentities($fn->text); ?></a>
<?php } ?> 
          </div><!-- col-sm-6 col-xs-12 -->
        </div><!-- row clearfix -->
      </div><!-- container -->
    </div><!-- footer -->

  </div>

<!-- JQUERY SCRIPTS -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'/>
  <script crossorigin='anonymous' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'/>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider.min.js'/>
  <script>
// <![CDATA[
(function($,undefined){var PROP_NAME="selectbox",FALSE=false,TRUE=true;function Selectbox(){this._state=[];this._defaults={classHolder:"sbHolder",classHolderDisabled:"sbHolderDisabled",classSelector:"sbSelector",classOptions:"sbOptions",classGroup:"sbGroup",classSub:"sbSub",classDisabled:"sbDisabled",classToggleOpen:"sbToggleOpen",classToggle:"sbToggle",speed:200,effect:"slide",onChange:null,onOpen:null,onClose:null}}$.extend(Selectbox.prototype,{_isOpenSelectbox:function(target){if(!target){return FALSE}var inst=this._getInst(target);return inst.isOpen},_isDisabledSelectbox:function(target){if(!target){return FALSE}var inst=this._getInst(target);return inst.isDisabled},_attachSelectbox:function(target,settings){if(this._getInst(target)){return FALSE}var $target=$(target),self=this,inst=self._newInst($target),sbHolder,sbSelector,sbToggle,sbOptions,s=FALSE,optGroup=$target.find("optgroup"),opts=$target.find("option"),olen=opts.length;$target.attr("sb",inst.uid);$.extend(inst.settings,self._defaults,settings);self._state[inst.uid]=FALSE;$target.hide();function closeOthers(){var key,uid=this.attr("id").split("_")[1];for(key in self._state){if(key!==uid){if(self._state.hasOwnProperty(key)){if($(":input[sb='"+key+"']")[0]){self._closeSelectbox($(":input[sb='"+key+"']")[0])}}}}}sbHolder=$("<div>",{id:"sbHolder_"+inst.uid,"class":inst.settings.classHolder});sbSelector=$("<a>",{id:"sbSelector_"+inst.uid,href:"#","class":inst.settings.classSelector,click:function(e){e.preventDefault();closeOthers.apply($(this),[]);var uid=$(this).attr("id").split("_")[1];if(self._state[uid]){self._closeSelectbox(target)}else{self._openSelectbox(target)}}});sbToggle=$("<a>",{id:"sbToggle_"+inst.uid,href:"#","class":inst.settings.classToggle,click:function(e){e.preventDefault();closeOthers.apply($(this),[]);var uid=$(this).attr("id").split("_")[1];if(self._state[uid]){self._closeSelectbox(target)}else{self._openSelectbox(target)}}});sbToggle.appendTo(sbHolder);sbOptions=$("<ul>",{id:"sbOptions_"+inst.uid,"class":inst.settings.classOptions,css:{display:"none"}});$target.children().each(function(i){var that=$(this),li,config={};if(that.is("option")){getOptions(that)}else{if(that.is("optgroup")){li=$("<li>");$("<span>",{text:that.attr("label")}).addClass(inst.settings.classGroup).appendTo(li);li.appendTo(sbOptions);if(that.is(":disabled")){config.disabled=true}config.sub=true;getOptions(that.find("option"),config)}}});function getOptions(){var sub=arguments[1]&&arguments[1].sub?true:false,disabled=arguments[1]&&arguments[1].disabled?true:false;arguments[0].each(function(i){var that=$(this),li=$("<li>"),child;if(that.is(":selected")){sbSelector.text(that.text());s=TRUE}if(i===olen-1){li.addClass("last")}if(!that.is(":disabled")&&!disabled){child=$("<a>",{href:"#"+that.val(),rel:that.val(),text:that.text(),click:function(e){e.preventDefault();var t=sbToggle,uid=t.attr("id").split("_")[1];self._changeSelectbox(target,$(this).attr("rel"),$(this).text());self._closeSelectbox(target)}});if(sub){child.addClass(inst.settings.classSub)}child.appendTo(li)}else{child=$("<span>",{text:that.text()}).addClass(inst.settings.classDisabled);if(sub){child.addClass(inst.settings.classSub)}child.appendTo(li)}li.appendTo(sbOptions)})}if(!s){sbSelector.text(opts.first().text())}$.data(target,PROP_NAME,inst);sbSelector.appendTo(sbHolder);sbOptions.appendTo(sbHolder);sbHolder.insertAfter($target)},_detachSelectbox:function(target){var inst=this._getInst(target);if(!inst){return FALSE}$("#sbHolder_"+inst.uid).remove();$.data(target,PROP_NAME,null);$(target).show()},_changeSelectbox:function(target,value,text){var inst=this._getInst(target),onChange=this._get(inst,"onChange");$("#sbSelector_"+inst.uid).text(text);$(target).find("option[value='"+value+"']").attr("selected",TRUE);if(onChange){onChange.apply((inst.input?inst.input[0]:null),[value,inst])}else{if(inst.input){inst.input.trigger("change")}}},_enableSelectbox:function(target){var inst=this._getInst(target);if(!inst||!inst.isDisabled){return FALSE}$("#sbHolder_"+inst.uid).removeClass(inst.settings.classHolderDisabled);inst.isDisabled=FALSE;$.data(target,PROP_NAME,inst)},_disableSelectbox:function(target){var inst=this._getInst(target);if(!inst||inst.isDisabled){return FALSE}$("#sbHolder_"+inst.uid).addClass(inst.settings.classHolderDisabled);inst.isDisabled=TRUE;$.data(target,PROP_NAME,inst)},_optionSelectbox:function(target,name,value){var inst=this._getInst(target);if(!inst){return FALSE}inst[name]=value;$.data(target,PROP_NAME,inst)},_openSelectbox:function(target){var inst=this._getInst(target);if(!inst||inst.isOpen||inst.isDisabled){return }var el=$("#sbOptions_"+inst.uid),viewportHeight=parseInt($(window).height(),10),offset=$("#sbHolder_"+inst.uid).offset(),scrollTop=$(window).scrollTop(),height=el.prev().height(),diff=viewportHeight-(offset.top-scrollTop)-height/2,onOpen=this._get(inst,"onOpen");el.css({top:height+"px",maxHeight:(diff-height)+"px"});inst.settings.effect==="fade"?el.fadeIn(inst.settings.speed):el.slideDown(inst.settings.speed);$("#sbToggle_"+inst.uid).addClass(inst.settings.classToggleOpen);this._state[inst.uid]=TRUE;inst.isOpen=TRUE;if(onOpen){onOpen.apply((inst.input?inst.input[0]:null),[inst])}$.data(target,PROP_NAME,inst)},_closeSelectbox:function(target){var inst=this._getInst(target);if(!inst||!inst.isOpen){return }var onClose=this._get(inst,"onClose");inst.settings.effect==="fade"?$("#sbOptions_"+inst.uid).fadeOut(inst.settings.speed):$("#sbOptions_"+inst.uid).slideUp(inst.settings.speed);$("#sbToggle_"+inst.uid).removeClass(inst.settings.classToggleOpen);this._state[inst.uid]=FALSE;inst.isOpen=FALSE;if(onClose){onClose.apply((inst.input?inst.input[0]:null),[inst])}$.data(target,PROP_NAME,inst)},_newInst:function(target){var id=target[0].id.replace(/([^A-Za-z0-9_-])/g,"\\\\$1");return{id:id,input:target,uid:Math.floor(Math.random()*99999999),isOpen:FALSE,isDisabled:FALSE,settings:{}}},_getInst:function(target){try{return $.data(target,PROP_NAME)}catch(err){throw"Missing instance data for this selectbox"}},_get:function(inst,name){return inst.settings[name]!==undefined?inst.settings[name]:this._defaults[name]}});$.fn.selectbox=function(options){var otherArgs=Array.prototype.slice.call(arguments,1);if(typeof options=="string"&&options=="isDisabled"){return $.selectbox["_"+options+"Selectbox"].apply($.selectbox,[this[0]].concat(otherArgs))}if(options=="option"&&arguments.length==2&&typeof arguments[1]=="string"){return $.selectbox["_"+options+"Selectbox"].apply($.selectbox,[this[0]].concat(otherArgs))}return this.each(function(){typeof options=="string"?$.selectbox["_"+options+"Selectbox"].apply($.selectbox,[this].concat(otherArgs)):$.selectbox._attachSelectbox(this,options)})};$.selectbox=new Selectbox();$.selectbox.version="0.1.3"})(jQuery);
// ]]>
  </script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'/>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/noframework.waypoints.min.js'/>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js'/>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js'/>
  <script>
  //  <![CDATA[
$(document).ready(function(){$('.navbar a.dropdown-toggle').on('click',function(e){var elmnt=$(this).parent().parent();if(!elmnt.hasClass('nav')){var li=$(this).parent();var heightParent=parseInt(elmnt.css('height').replace('px',''))/ 2;
var widthParent=parseInt(elmnt.css('width').replace('px',''))- 10;if(!li.hasClass('open'))li.addClass('open')
else li.removeClass('open');$(this).next().css('top',heightParent+'px');$(this).next().css('left',widthParent+'px');return false;}});});
 //   ]]>
  </script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js'/>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js'/>
   <script>
// <![CDATA[
jQuery(document).ready(function($){
  var navbar=$('.navbar-main'),distance=navbar.offset().top,$window=$(window);
  $window.scroll(function(){
    if(($window.scrollTop()>=distance)&&($(".navbar-default").hasClass("navbar-main"))) {
      navbar.removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
      $("body").css("padding-top","70px");
    } else {
      navbar.removeClass('navbar-fixed-top');
      $("body").css("padding-top","0px");
    }
  });
});
    // ]]>
  </script>

</body>
</html>