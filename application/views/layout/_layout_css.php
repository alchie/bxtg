  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingCSS">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCSS" aria-expanded="true" aria-controls="collapseCSS">
          Custom CSS
        </a>
      </h4>
    </div>
    <div id="collapseCSS" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCSS">
      <div class="panel-body">

        <div class="form-group">
          <label>General</label>
          <textarea class="form-control" name="layout[css][css]" rows="10"><?php echo get_layout_value('css','css', $layouts); ?></textarea>
        </div>

<div class="row">
  <div class="col-md-6">
            <div class="form-group">
              <label>Large [.col-lg-*]</label>
          <textarea class="form-control" name="layout[css][css_lg]" rows="5"><?php echo get_layout_value('css','css_lg', $layouts); ?></textarea>
        </div>
</div>
  <div class="col-md-6">
            <div class="form-group">
              <label>Medium [.col-md-*]</label>
          <textarea class="form-control" name="layout[css][css_md]" rows="5"><?php echo get_layout_value('css','css_md', $layouts); ?></textarea>
        </div>
</div>
</div>

<div class="row">
  <div class="col-md-6">
            <div class="form-group">
              <label>Small [.col-sm-*]</label>
          <textarea class="form-control" name="layout[css][css_sm]" rows="5"><?php echo get_layout_value('css','css_sm', $layouts); ?></textarea>
        </div>
</div>
  <div class="col-md-6">
            <div class="form-group">
              <label>Extra Small [.col-xs-*]</label>
          <textarea class="form-control" name="layout[css][css_xs]" rows="5"><?php echo get_layout_value('css','css_xs', $layouts); ?></textarea>
        </div>
</div>
</div>

      </div>
    </div>
  </div>