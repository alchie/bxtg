<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-success navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNavBarCollapse" aria-expanded="false" aria-controls="navbar" id="mainNavBarToggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand" href="<?php echo site_url(); ?>">
<?php if(isset($current_client)&&($current_client)) { ?>
    <?php echo $current_client->name; ?>
<?php } else { ?>
    <?php echo APP_NAME; ?>
<?php }  ?>
          </span>
        </div>
        <div id="mainNavBarCollapse" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
<?php if(isset($current_client)&&($current_client)) { ?>
          <li class="<?php echo ($current_uri=='layouts') ? 'active' : ''; ?>">
              <a href="<?php echo site_url("layout/index/{$current_client->id}"); ?>">
                Layout
              </a>
          </li>

          <li class="<?php echo ($current_uri=='pages') ? 'active' : ''; ?>">
              <a href="<?php echo site_url("pages/index/{$current_client->id}"); ?>">
                Pages
              </a>
          </li>

          <li class="<?php echo ($current_uri=='rows') ? 'active' : ''; ?>">
              <a href="<?php echo site_url("rows"); ?>">
                Rows
              </a>
          </li>
          <li class="<?php echo ($current_uri=='menus') ? 'active' : ''; ?>">
              <a href="<?php echo site_url("menus/index/{$current_client->id}"); ?>">
                Menus
              </a>
          </li>
          <li class="<?php echo ($current_uri=='images') ? 'active' : ''; ?>">
              <a href="<?php echo site_url("images"); ?>">
                Images
              </a>
          </li>
          <!--<li class="<?php echo ($current_uri=='templates') ? 'active' : ''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Templates <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url("templates/index/{$current_client->id}"); ?>">All Templates</a></li>
                  <?php if( $templates ) foreach($templates as $tkey=>$template) { ?>
                    <li><a href="<?php echo site_url("templates/view/{$current_client->id}/{$tkey}"); ?>" target="_demo"><?php echo $template['name']; ?></a></li>
                  <?php } ?>
              </ul>
              
          </li>-->
<?php }  ?>
          </ul>

          <ul class="nav navbar-nav navbar-right">
<?php if( (isset($current_client)) && ($current_client) && (hasAccess('clients', 'clients', 'edit') ) ) { ?>
<li>
              <a target="_demo" href="<?php echo site_url("templates/view/{$current_client->id}"); ?>">
                 <span class="glyphicon glyphicon-play" style="color:yellow"></span> PREVIEW
              </a>
          </li>
          <li>
              <a class="ajax-modal" href="<?php echo site_url("clients/edit/{$current_client->id}/ajax") . "?next=" . uri_string(); ?>" data-title="Edit Client">
                 <span class="glyphicon glyphicon-pencil"></span>
              </a>
          </li>
          <li>
              <a class="ajax-modal" href="<?php echo site_url("clients/export/ajax") . "?next=" . uri_string(); ?>" data-title="Export Client" data-hide_footer="1">
                 <span class="glyphicon glyphicon-export"></span>
              </a>
          </li>
<?php } ?>
<?php if( isset($current_controller) && (($current_controller == 'welcome')||($current_controller == 'clients') ) ) { ?>
<?php if( hasAccess('clients', 'clients', 'add') ) { ?>
          <li>
              <a class="ajax-modal" href="<?php echo site_url("clients/add/ajax") . "?next=" . uri_string(); ?>" data-title="Add Client">
                 <span class="glyphicon glyphicon-plus"></span>
              </a>
          </li>
          <li>
              <a href="<?php echo site_url("clients/import") . "?next=" . uri_string(); ?>">
                 <span class="glyphicon glyphicon-import"></span>
              </a>
          </li>
<?php } ?>
<?php } ?>
<!--
          <li>
              <a href="<?php echo site_url("welcome"); ?>">
                <span class="glyphicon glyphicon-th"></span>
              </a>
          </li>
-->

<?php 
$main_menu = array();

$main_menu['system'] = array(
      'title' => 'System',
      'uri' => 'system',
      'permission' => 'system',
      'sub_menus' => array(
          'welcome' => array(
            'title' => 'Clients',
            'uri' => 'welcome',
            'permission' => 'users',
          ),
          'system_users' => array(
            'title' => 'User Accounts',
            'uri' => 'system_users',
            'permission' => 'users',
          ),
          'system_backup' => array(
            'title' => 'Database Backup',
            'uri' => 'system_backup',
            'permission' => 'backup',
          ),
        )
);

foreach($main_menu as $main=>$menu): 
  if( ! isset( $menu['permission'] ) ) {
    continue;
  }
  if( ! isset( $this->session->menu_module[$menu['permission']] ) ) {
    continue;
  }
?>
          <li class="dropdown">
              <a href="#<?php echo $menu['uri']; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-cog"></span>
              </a>
                  <ul class="dropdown-menu">
                  <?php foreach($menu['sub_menus'] as $sub=>$sub_menu): 
                    if( ! isset( $sub_menu['permission'] ) ) {
                      continue;
                    }
                    if( ($sub_menu['permission']) && (! in_array($sub_menu['permission'], $this->session->menu_module[$menu['permission']] ) ) ) {
                      continue;
                    }
                  ?>
                    <?php if( isset($sub_menu['header']) && ($sub_menu['header']) ) { ?>
                        <?php if( isset($sub_menu['separator']) && ($sub_menu['separator']) ) { ?>
                          <li role="separator" class="divider"></li>
                        <?php } ?>
                        <?php if( isset($sub_menu['title']) && ($sub_menu['title'] != '') ) { ?>
                          <span class="dropdown-header"><?php echo $sub_menu['title']; ?></span>
                        <?php } ?>
                    <?php } else { ?>
                    <li><a href="<?php echo site_url($sub_menu['uri']); ?>"><?php echo $sub_menu['title']; ?></a></li>
                    <?php } ?>
                  <?php endforeach; ?>
                  </ul>
          </li>
<?php endforeach; ?>

            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="visible-xs"><?php echo $this->session->name; ?>
            <span class="caret hidden-xs"></span>
          </span>
          <span class="glyphicon glyphicon-user hidden-xs"></span></a>
          <ul class="dropdown-menu">
            <span class="dropdown-header"><?php echo $this->session->name; ?></span>
            <li><a href="<?php echo site_url("account/settings/ajax") . "?next=" . uri_string(); ?>" class="ajax-modal" data-title="Account Settings">Account Settings</a></li>
            <li><a data-title="Change Password" class="ajax-modal" href="<?php echo site_url("account/change_password/ajax") . "?next=" . uri_string(); ?>">Change Password</a></li>
            <li><a href="<?php echo site_url('account/logout') . "?next=" . urlencode( uri_string() ); ?>">Logout</a></li>
          </ul>
        </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>