<?php if (isset($widget_options)&&(isset($widget_options->container_class))&&($widget_options->container_class!='')) { ?>
	<div class="<?php echo $widget_options->container_class; ?>">
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->show_title))&&($widget_options->show_title==1)) { ?>
	<h5><?php echo $widget_options->title; ?></h5>
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->show_list))&&($widget_options->show_list==1)) { ?>
	<ul class="<?php echo $widget_options->list_class; ?>">
<?php } ?>
<?php 

	$links = get_menu_links( $widget_options->menu_id );

	if( $links ) foreach($links as $link) { 
		$link_url = $link->url;

		if( $link->inner_page ) {
			$link_url = site_url("templates/view/{$client_id}/{$template_name}/{$link->inner_page}");
		}


		$list_item_class = (isset($widget_options->list_item_class)) ? $widget_options->list_item_class : '';

		if (isset($widget_options)&&(isset($widget_options->show_list))&&($widget_options->show_list==1)) {
			echo "<li class='{$list_item_class}'>";
		}
		echo "<a href=\"{$link_url}\">";

		if (isset($widget_options)&&(isset($widget_options->show_icon))&&($widget_options->show_icon==1)) {
			echo "<span class=\"{$link->icon}\"></span> ";
		}

		if (isset($widget_options)&&(isset($widget_options->show_text))&&($widget_options->show_text==1)) {
			echo htmlentities($link->text);
		}

		echo "</a>";
		if (isset($widget_options)&&(isset($widget_options->show_list))&&($widget_options->show_list==1)) {
			echo "<li>";
		}
	}

?>

<?php if (isset($widget_options)&&(isset($widget_options->show_list))&&($widget_options->show_list==1)) { ?>
	</ul>
<?php } ?>

<?php if (isset($widget_options)&&(isset($widget_options->container_class))&&($widget_options->container_class!='')) { ?>
	</div>
<?php } ?>