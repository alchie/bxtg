<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['bxtg_templates'] = array(
	'blank_template' => array('name'=>'Blank Template (Empty)','version'=>'1'),
	'bootstrap3_blank' => array('name'=>'Bootstrap 3.3.7 (Blank)','version'=>'1'),
	'royal' => array('name'=>'Royal College','version'=>'5'),
	'welfare' => array('name'=>'Welfare by ColorLib','version'=>'5'),
);

